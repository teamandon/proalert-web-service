﻿using System;
using ProAlert.Andon.Service.Models;

namespace ProAlert.Andon.Service.ViewModels
{
    public class PlannedInfoVM
    {
        public PlannedDtLog pdl { get; set; }

        public string PlannedDtName
        {
            get
            {
                if (pdl.PlannedDt == null) return "";
                return pdl.PlannedDt.Name;
                //using (var db = new ProAndonContext())
                //{
                //    return db.PlannedDts.FirstOrDefault(x => x.PlannedDtID == pdl.PlannedDtID).Name;
                //}
            }
        }

        public DateTime Start => pdl.Start;
        public DateTime? Stop => pdl.Stop;
        public string Notes => pdl.Notes;

        public string WorkCenterName
        {
            get
            {
                if (pdl.WorkCenter == null) return "";
                return pdl.WorkCenter.Name;
                //using (var db = new ProAndonContext())
                //{
                //    return db.WorkCenters.FirstOrDefault(x => x.WorkCenterID == pdl.WorkCenterID).Name;
                //}
            }
        }
    }
}