﻿
namespace ProAlert.Andon.Service.ViewModels
{
    public class WcPrintersVM
    {
        public int WorkCenterId { get; set; }
        public string WorkCenterName { get; set; }
        public string Label { get; set; }
        public string Printer { get; set; }
        public bool IsDirty { get; set; }
    }
}
