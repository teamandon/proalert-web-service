﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vistech.Andon.Service.Models;

namespace Vistech.Andon.Service.ViewModels
{
    public class StatsVM
    {
        public StatsVM()
        {
            ptl = new ProductTimeLine();
        }

        #region Members

        public ProductTimeLine ptl;
        private int totalOpSec { get; set; }
        private int _mfg;
        private int _matl;
        private int _other;
        private int _rework;
        private string _lastDTReason;
        private int _totDT;
        private int _pieceCnt;
        private int _totCycles;
        private decimal _cycleTarget;
        private int _operationTime;
        private string _cycles;
        private int _pid;
        private int _plannedProductionTime;
        private ProductWorkCenterStat _pwcs;
        private decimal _avgCyclesPerMin;

        public bool Current
        {
            get { return OEEStartTime <= DateTime.UtcNow && OEEEndTime >= DateTime.UtcNow.AddSeconds(-40);  }
        }

        public int OperationTime
        {
            get { return _operationTime; }
            set { _operationTime = value; }
        }

        public String OperationTimeInMin
        {
            get { return (OperationTime/60000).ToString("N1"); }
        }

        public int? WorkCenterID { get; set; }

        public DateTime OEEStartTime { get; set; }

        public DateTime OEEEndTime { get; set; }

        public int TotalDT
        {
            get { return _totDT; }
            set { _totDT = value; }
        }

        public String TotalDTInMin
        {
            get { return (_totDT/60000).ToString("N1"); }
        }

        public String LastDTReason
        {
            get { return _lastDTReason; } set { _lastDTReason = value; }
        }

        public String Scrap { get { return _mfg.ToString() + "(" + _matl.ToString() + ")(" + _other.ToString() + ")"; } }

        public int PlannedProduction { get { return _plannedProductionTime; } set { _plannedProductionTime = value; } }

        public int MFG { get { return _mfg; } set { _mfg = value; } }
        public int MATL { get { return _matl; } set { _matl = value; }}
        public int OTHER { get { return _other; } set { _other = value; } }
        public int Rework { get { return _rework; } set { _rework = value; }}

        public String PieceCnt
        {
            get
            {
                //return TotalPieces.ToString() + "@" + (TotalPieces - (_mfg + _matl + _other + _rework)).ToString();
                return (CycleTarget * (_pwcs == null ? 1 :_pwcs.UnitsPerCycle)).ToString("N0") + "@" + TotalPieces.ToString("N0");
            }
        }

        public int TotalPieces
        {
            get { return _pieceCnt; } set { _pieceCnt = value; }
        }

        public decimal RunRate { get; set; }

        public int TotalCycles { get { return _totCycles; } set { _totCycles = value; }}

        public decimal TargetPieces {get { return CycleTarget* (_pwcs == null ? 0 : _pwcs.UnitsPerCycle) ; }}

        public decimal CycleTarget {get { return _cycleTarget; } set { _cycleTarget = value; }}

        public String Cycles
        {
            get { return _cycles; }
            set { _cycles = value; }
        }

        public decimal CyclesPerMin { get { return _avgCyclesPerMin; } set { _avgCyclesPerMin = value; }}

        public decimal Availability
        {
            get { return PlannedProduction == 0 || OperationTime == 0 ? 0 : (decimal) OperationTime/PlannedProduction; }
        }

        public decimal Performance
        {
            get
            {
                if (OperationTime == 0 || RunRate == 0 || TotalPieces == 0)
                    return 0;
                var p = TotalPieces/((decimal) OperationTime/60000)/RunRate;
                return p > 1 ? 1 : p;
            }
        }

        public decimal Quality
        {
            //get { return TotalPieces == 0 ? 0 : (decimal)(TotalPieces - (MFG + MATL + OTHER + Rework)) / TotalPieces; }
            get { return TotalPieces == 0 ? 0 : (decimal)(TotalPieces - (MFG + OTHER + Rework)) / TotalPieces; }
        }

        public decimal OEE
        {
            get { return Availability*Performance*Quality; }
        }

        public bool Down { get; set; }

        public bool WCPlanned { get; set; }

        public bool TimeSegPlanned { get { return PlannedProduction <= ptl.GetTotalPlannedDT(); } }

        public String WCProduct
        {
            get;set;
        }

        public int ProductID { get { return _pid; } set { _pid = value; }}

        public String ScrapOneNumber
        {
            get { return (MFG + MATL + OTHER + Rework).ToString(); }
        }

        public decimal TargetPerformance
        {
            get { return _pwcs == null ? 1 : _pwcs.Performance; }
        }

        public decimal TargetAvailablity
        {
            get { return _pwcs == null ? 1 : _pwcs.Available; }
        }

        public decimal TargetQuality
        {
            get { return _pwcs == null ? 1 : _pwcs.Quality; }
        }

        public decimal TargetOEE
        {
            get { return _pwcs == null ? 1 : _pwcs.OEE; }
        }

        #endregion

        #region Methods

        public void SetValues()
        {
            _pid = SetProductID();
            _pwcs = getProductWorkCenterStat();
            SetMisc();
            FillCalls();
            FillPlannedDT();
            FillCycles();
            SetPlannedProduction();
            MFG = setMfg();
            MATL = setMatl();
            OTHER = setOther();
            Rework = setRework();
            OperationTime = setOperationTime();
            LastDTReason = setLastDtReason();
            TotalDT = setTotalDt();
            TotalCycles = GetTotalCycles();
            TotalPieces = setPieceCnt();
            CycleTarget = setCycleTarget();
            CyclesPerMin = setCyclesPerMin();
            Cycles = setCycles();
        }

        #endregion

        #region Functions

        private void SetMisc()
        {
            using (var db = new AndonVistechContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var wc = db.WorkCenters.FirstOrDefault(x => x.WorkCenterID == WorkCenterID);
                var name = "";
                if (wc != null)
                {
                    name = wc.Name;
                    Down = wc.Down ?? false;
                    WCPlanned = wc.Planned ?? false;
                }
                var product = _pid == 0 ? "" : db.Products.First(x => x.ProductID == _pid).Name;
                WCProduct = name + "/" + product;
            }
        }
        private int SetProductID()
        {
            using (var db = new AndonVistechContext())
            {
                db.Configuration.ProxyCreationEnabled = false;

                if (ProductID == 0)
                {
                    var n = new AppError
                    {
                        ErrorMsg = "no product has been selected",
                        ErrorDT = DateTime.UtcNow,
                        Source = "Stats - SetProductID"
                    };
                    db.AppErrors.Add(n);
                    db.SaveChanges();
                }
                return ProductID;

                ////var cycles = from c in db.CycleHistories
                ////             where c.WorkCenterID == WorkCenterID && c.UpdateDT >= OEEStartTime && c.UpdateDT <= OEEEndTime
                ////             orderby c.UpdateDT
                ////             select c;
                ////if (cycles.Select(x => x.ProductID).Distinct().Count() > 1)
                ////{
                ////    var n = new AppError
                ////    {
                ////        ErrorMsg = "multiple products in time segment",
                ////        ErrorDT = DateTime.UtcNow,
                ////        Source = "Stats - SetProductID"
                ////    };
                ////    db.AppErrors.Add(n);
                ////    db.SaveChanges();
                ////}
                ////if (cycles.Any())
                ////    if (cycles.First().ProductID != null)
                ////        return cycles.ToList()[cycles.Count()-1].ProductID ?? 0;

                //// if I can't find a cycle in within the parameters of the time frame, look outside the time frame to determine what was setup to run.

                ////var tsStart = OEEStartTime;
                //var first = (from pd in db.PlannedDtLogs
                //                 where pd.WorkCenterID == WorkCenterID && pd.PlannedDt.Name == "Product Change Over" && pd.Stop >= OEEStartTime && pd.Stop < OEEEndTime
                //                 select pd).Take()
                //var firstChoice = db.PlannedDtLogs.OrderByDescending(x => x.Start)
                //    .FirstOrDefault(x => x.WorkCenterID == WorkCenterID && x.PlannedDt.Name == "Product Change Over" && x.Stop >= OEEStartTime && x.Stop < OEEEndTime);
                //if (firstChoice != null)
                //    if (firstChoice.ProductID != null)
                //        return firstChoice.ProductID ?? 0;

                //var secondChoice = db.PlannedDtLogs.OrderBy(x => x.Start)
                //    .FirstOrDefault(x => x.WorkCenterID == WorkCenterID && x.PlannedDt.Name == "Product Change Over" && x.);

                //if (startDate != null)
                //{
                //    if (startDate.ProductID != null) return startDate.ProductID ?? 0;
                //    //// look in cyclehistory for any cycles since the change over up to the time segment in question.
                //    //var cycleBefore =
                //    //    db.CycleHistories.FirstOrDefault(x => x.WorkCenterID == WorkCenterID && x.UpdateDT > startDate.Stop && x.UpdateDT < startDate.Start);
                //    //if (cycleBefore != null)
                //    //{
                //    //    var stats =
                //    //        db.ProductWorkCenterStats.FirstOrDefault(
                //    //            x => x.WorkCenterID == WorkCenterID && x.ProductID == cycleBefore.ProductID);
                //    //    if (stats != null)
                //    //    {
                //    //        return stats.ProductID;
                //    //    }
                //    //}
                //}
                //if (stopDate == null)
                //{
                //    var pid = db.WorkCenters.FirstOrDefault(x => x.WorkCenterID == WorkCenterID);
                //    if (pid == null) return 0;
                //    return pid.ProductID ?? 0;
                //}

                //var cycleAfter =
                //    db.CycleHistories.FirstOrDefault(
                //        x =>
                //            x.WorkCenterID == WorkCenterID && x.UpdateDT < stopDate.Start &&
                //            x.UpdateDT > tsStart);

                //if (cycleAfter == null)
                //{
                //    var wcPID = db.WorkCenters.FirstOrDefault(x => x.WorkCenterID == WorkCenterID).ProductID;
                //    if (wcPID == null) return 0;
                //    return wcPID ?? 0;
                //}

                //var afterStats = db.ProductWorkCenterStats.FirstOrDefault(x => x.WorkCenterID == WorkCenterID && x.ProductID == cycleAfter.ProductID);
                //if (afterStats == null) return 0;
                //return afterStats.ProductID;

            }

        }

        private ProductWorkCenterStat getProductWorkCenterStat()
        {
            using (var db = new AndonVistechContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var found = (from p in db.ProductWorkCenterStats
                             where p.WorkCenterID == WorkCenterID && p.ProductID == _pid
                             select p).Any();
                if (!found) return null;

                var dr = (from p in db.ProductWorkCenterStats
                          where p.WorkCenterID == WorkCenterID && p.ProductID == _pid
                          select p).Single();
                return dr;
            }
        }

        private String setCycles()
        {
            return CycleTarget.ToString("N1") + "@" + TotalCycles.ToString("N0");
        }

        private string setLastDtReason()
        {
            using (var db = new AndonVistechContext())
            {
                var reason = from cl in db.CallLogs
                             join c in db.Calls on cl.CallID equals c.CallID
                             where cl.WorkCenterId == WorkCenterID
                             && c.DT
                             && cl.InitiateDt >= OEEStartTime
                             && cl.InitiateDt <= OEEEndTime
                             select new { initiatedt = cl.InitiateDt, Name = c.Name };
                if (!reason.Any())
                    return "N/A";
                var lastcall = reason.OrderByDescending(a => a.initiatedt).Select(x => x.Name);
                return lastcall.ToList()[0];
            }
        }

        private void FillCycles()
        {
            ptl.FillInBlanks();
            // first get list of WC's & ProductID's with start/stops
            using (var db = new AndonVistechContext())
            {
                foreach (var item in ptl.GetList().Where(x => !x.plannedDT && !x.DT))
                {
                    db.Configuration.ProxyCreationEnabled = false;
                    var query = (from ch in db.CycleHistories
                                 where ch.WorkCenterID == WorkCenterID && ch.UpdateDT >= item.Start && ch.UpdateDT < item.End
                                 orderby ch.UpdateDT descending
                                 select new {ch.WorkCenterID, ch.UpdateDT} ).FirstOrDefault();

                    if (query == null) return;

                    var sumCnt = (from ch in db.CycleHistories
                                  where ch.WorkCenterID == WorkCenterID && ch.UpdateDT >= item.Start && ch.UpdateDT < item.End
                                  select ch.Count).Sum();

                    var nwcs = new CycleVM
                    {
                        WorkCenterID = query.WorkCenterID,
                        UpdateDt = query.UpdateDT,
                        Count = sumCnt,
                        pwcs = _pwcs
                    };

                    ptl.AddCycle(nwcs);
                }

                //var query = from ch in db.CycleHistories
                //            join p in db.ProductWorkCenterStats on new { wc = ch.WorkCenterID, pid = (int)ch.ProductID } equals
                //                new { wc = p.WorkCenterID, pid = p.ProductID }
                //            where ch.WorkCenterID == WorkCenterID && ch.UpdateDT >= OEEStartTime && ch.UpdateDT <= OEEEndTime
                //            orderby ch.WorkCenterID, ch.UpdateDT
                //            select new { ch, p };

                //foreach (var dr in query)
                //{
                //    if (dr.ch.ProductID != null)
                //    {
                //        var nwcs = new CycleVM()
                //        {
                //            WorkCenterID = dr.ch.WorkCenterID,
                //            ProductID = (int)dr.ch.ProductID,
                //            UpdateDt = dr.ch.UpdateDT,
                //            Count = dr.ch.Count,
                //            pwcs = dr.p
                //        };
                //        ptl.AddCycle(nwcs);
                //    }
                //} 
            }
        }

        private void FillCalls()
        {
            using (var db = new AndonVistechContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var allDT = from cl in db.CallLogs
                    join c in db.Calls on cl.CallID equals c.CallID
                    where cl.WorkCenterId == WorkCenterID
                          && c.DT
                          && ((cl.InitiateDt < OEEStartTime && cl.ResolveDt > OEEStartTime)
                              || (cl.InitiateDt >= OEEStartTime && cl.InitiateDt <= OEEEndTime))
                    select new {cl, c.DT};

                ptl.oeeStart = OEEStartTime;
                ptl.oeeStop = OEEEndTime;

                foreach (var dr in allDT.ToList().OrderBy(x => x.cl.InitiateDt).ThenByDescending(x => x.cl.ResolveDt))
                {
                    var ts = new Timeseg();
                    ts.Start = dr.cl.InitiateDt < OEEStartTime ? OEEStartTime : dr.cl.InitiateDt;
                    ts.End = dr.cl.ResolveDt ?? OEEEndTime;
                    ts.DT = dr.DT;
                    ts.plannedDT = false;
                    ts.addCall(new CallInfoVM
                    {
                        cl = dr.cl
                    });
                    ptl.Add(ts);
                }
            }

        }

        private void FillPlannedDT()
        {
            using (var db = new AndonVistechContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var allPDT = from p in db.PlannedDtLogs
                             join pd in db.PlannedDts on p.PlannedDtID equals pd.PlannedDtID
                    where p.WorkCenterID == WorkCenterID
                          && ((p.Start < OEEStartTime && p.Stop > OEEStartTime)
                              || (p.Start >= OEEStartTime && p.Start < OEEEndTime))
                    select new {p, pd.Name};

                foreach (var dr in allPDT.OrderBy(x => x.p.Start).ThenByDescending(x => x.p.Stop))
                {
                    var ts = new Timeseg();
                    ts.Start = dr.p.Start < OEEStartTime ? OEEStartTime : dr.p.Start;
                    ts.End = dr.p.Stop ?? OEEEndTime;
                    ts.DT = false;
                    ts.plannedDT = true;
                    ts.ProductChangeOver = dr.Name == "Product Change Over";
                    ts.addPlanned(new PlannedInfoVM()
                    {
                        pdl = dr.p
                    });
                    ptl.Add(ts);
                }
            }
        }

        private void SetPlannedProduction()
        {
            var totTime = (int)OEEEndTime.Subtract(OEEStartTime).TotalMilliseconds;
            PlannedProduction = totTime - ptl.GetTotalPlannedDT();
            PlannedProduction = PlannedProduction == 0 ? 1 : PlannedProduction;
        }

        private int setTotalDt()
        {
            return ptl.GetTotalDT();
        }

        private int setOther()
        {
            using (var db = new AndonVistechContext())
            {
                var other = (from s in db.Scraps
                             join st in db.ScrapTypes on s.ScrapTypeID equals st.ScrapTypeID
                             where s.WorkCenterID == WorkCenterID
                                   && st.Name == "Other"
                                   && s.ScrapDT >= OEEStartTime
                                   && s.ScrapDT <= OEEEndTime
                             select s.ScrapCount).DefaultIfEmpty(0).Sum();
                return other;
            }
        }

        private int setMatl()
        {
            using (var db = new AndonVistechContext())
            {
                var matl = (from s in db.Scraps
                            join st in db.ScrapTypes on s.ScrapTypeID equals st.ScrapTypeID
                            where s.WorkCenterID == WorkCenterID
                                  && st.Name == "Material"
                                  && s.ScrapDT >= OEEStartTime
                                  && s.ScrapDT <= OEEEndTime
                            select s.ScrapCount).DefaultIfEmpty(0).Sum();
                return matl;
            }
        }

        private int setMfg()
        {
            using (var db = new AndonVistechContext())
            {
                var mfg = (from s in db.Scraps
                           join st in db.ScrapTypes on s.ScrapTypeID equals st.ScrapTypeID
                           where s.WorkCenterID == WorkCenterID
                                 && st.Name == "Manufacturing"
                                 && s.ScrapDT >= OEEStartTime
                                 && s.ScrapDT <= OEEEndTime
                           select s.ScrapCount).DefaultIfEmpty(0).Sum();
                return mfg;
            }
        }

        private int setRework()
        {
            using (var db = new AndonVistechContext())
            {
                var rework = (from s in db.Scraps
                              join st in db.ScrapTypes on s.ScrapTypeID equals st.ScrapTypeID
                              where
                                  s.WorkCenterID == WorkCenterID
                                  && st.Name == "Rework"
                                  && s.ScrapDT >= OEEStartTime
                                  && s.ScrapDT <= OEEEndTime
                              select s.ScrapCount).DefaultIfEmpty(0).Sum();
                return rework;  
            }
        }

        private int setPieceCnt()
        {
            if (_pwcs == null) return 0;
            return TotalCycles * _pwcs.UnitsPerCycle; 
        }

        private int GetTotalCycles()
        {
            var temp = ptl.GetTotalCyclesinDT();  // testing purposes
            return ptl.GetTotalCycles();
        }

        private decimal setCycleTarget()
        {
            if (_pwcs == null) return 0;
            return (decimal)_pwcs.CyclesPerHour / 3600000 * OperationTime;
            //return target;
        }

        private decimal setCyclesPerMin()
        {
            if (_pwcs == null)
            {
                RunRate = (decimal) 0.001;
                return (decimal).001;
            }
            RunRate = _pwcs.CyclesPerHour/60*_pwcs.UnitsPerCycle;
            return _pwcs.CyclesPerHour/60;
            //var avg = new List<decimal>();
            //var rr = new List<decimal>();

            //foreach (var dr in ptl.GetList().Where(x => !x.plannedDT && !x.DT))
            //{
            //    if (dr.Cycles.Count == 0) continue;
            //    decimal thisAvg = 0;
            //    decimal thisRR = 0;
            //    foreach (var c in dr.Cycles)
            //    {
            //        thisAvg += c.pwcs.CyclesPerHour;
            //        thisRR += c.pwcs.CyclesPerHour*c.pwcs.UnitsPerCycle;
            //    }
            //    avg.Add(thisAvg / dr.Cycles.Count);
            //    rr.Add(thisRR / dr.Cycles.Count);
            //}
            //// convert hours to minutes
            //AvgRunRate = rr.Count > 0 ? rr.Average()/60 : 0;
            //return avg.Count > 0 ? avg.Average()/60 : 0;
        }

        private int setOperationTime()
        {
            // Now - OEE Start Time - planned downtime
            var time = PlannedProduction - ptl.GetTotalDT();
            return time > 1 ? time : 1;
        }
        #endregion
    }

    public class Timeseg
    {
        public Timeseg()
        {
            callList = new List<CallInfoVM>();
            plannedList = new List<PlannedInfoVM>();
            Cycles = new List<CycleVM>();
        }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public bool DT { get; set; }
        public bool plannedDT { get; set; }
        public bool ProductChangeOver { get; set; }
        public List<CallInfoVM> callList;
        public List<PlannedInfoVM> plannedList;
        public List<CycleVM> Cycles;

        public void addCall(CallInfoVM callInfoVm)
        {
            callList.Add(callInfoVm);
        }

        public void addPlanned(PlannedInfoVM plannedInfoVm)
        {
            plannedList.Add(plannedInfoVm);
        }
    }
    
    public class ProductTimeLine
    {
        public List<Timeseg> _list;
        public DateTime oeeStart { get; set; }
        public DateTime oeeStop { get; set; }
        public String ProductName { get; set; }

        public ProductTimeLine()
        {
            _list = new List<Timeseg>();
        }

        public void Add(Timeseg ts)
        {
            ts.Start = ts.Start < oeeStart ? oeeStart : ts.Start;
            ts.End = ts.End > oeeStop ? oeeStop : ts.End;

            _list.Add(ts);
        }

        public void FillInBlanks()
        {
            var comp = Compress(true, false);

            comp.AddRange(Compress(false, true));

            if (comp.Count == 0)
            {
                _list.Add(new Timeseg
                {
                    Start = oeeStart,
                    End = oeeStop,
                    plannedDT = false,
                    DT = false
                });
                return;
            }

            var tmp = new List<TmpSeg>();

            foreach (var dr in comp.OrderBy(x=>x.Start))
            {
                if (dr.Start < oeeStart) continue;

                if (dr.Start > oeeStart && tmp.Count == 0)
                {
                    tmp.Add(new TmpSeg
                    {
                        Start = oeeStart,
                        End = dr.Start
                    });
                    tmp.Add(new TmpSeg
                    {
                        Start = dr.End
                    });
                    continue;
                }
                if (dr.End > oeeStop) continue;
                var openSeg = tmp.FirstOrDefault(x => x.End == null);
                if (openSeg != null)
                {
                    openSeg.End = dr.Start;
                    tmp.Add(new TmpSeg
                    {
                        Start = dr.End
                    });
                }
                else
                {
                    var seg = new TmpSeg
                    {
                        Start = dr.End
                    };
                    tmp.Add(seg);
                }
            }

            var lastTime = comp.Last().End;

            if (lastTime < oeeStop)
            {
                var found = tmp.FirstOrDefault(x => x.End == null);
                if (found != null)
                    found.End = oeeStop;
                else
                {
                    var seg = new TmpSeg
                    {
                        Start = lastTime,
                        End = oeeStop
                    };
                    tmp.Add(seg);
                }

            }

            foreach (var dr in tmp)
            {
                if (dr.Start != dr.End)
                    _list.Add(new Timeseg
                    {
                        Start = dr.Start ?? DateTime.MinValue,
                        End = dr.End ?? DateTime.MinValue,
                        plannedDT = false,
                        DT = false
                    });
            }
        }

        public void AddCycle(CycleVM c)
        {
            var seg = _list.FirstOrDefault(x=>x.Start <= c.UpdateDt && x.End >= c.UpdateDt);

            if (seg == null)
            {
                var ts = new Timeseg
                {
                    Start = oeeStart,
                    End = oeeStop,
                    plannedDT = false,
                    ProductChangeOver = false,
                    DT = false
                };
                ts.Cycles.Add(c);
                _list.Add(ts);
            }
            else
            {
                seg.Cycles.Add(c);
            }

        }

        private Timeseg TestStart(Timeseg ts, IEnumerable<Timeseg> l )
        {
            return l.FirstOrDefault(x => x.Start <= ts.Start && x.End > ts.Start);
        }

        //private Timeseg TestEnd(Timeseg ts, IEnumerable<Timeseg> l )
        //{
        //    return l.FirstOrDefault(x => x.Start <= ts.End && x.End >= ts.End);
        //}

        //public String GetList()
        //{
        //    return _list.Aggregate(String.Empty, (current, dr) => current + (dr.Start.ToString("G") + " - " + dr.End.ToString("G") + "<br/>"));
        //}

        public List<Timeseg> GetList()
        {
            return _list;
        }

        public int GetTotalDT()
        {
            var dt = 0;
            var shortList = Compress(true, false).ToList();
            foreach (var dr in shortList)
            {
                var tf = dr.End.Subtract(dr.Start);
                dt += (int)tf.TotalMilliseconds;
            }

            return dt;
        }

        public int GetTotalPlannedDT()
        {
            var dt = 0;
            var shortlist = Compress(false, true).ToList();
            foreach (var dr in shortlist)
            {
                var tf = dr.End.Subtract(dr.Start);
                dt += (int)tf.TotalMilliseconds;
            }

            return dt;
        }

        public int GetTotalCyclesinDT()
        {
            var cnt = 0;
            foreach (var dr in _list.Where(x => x.DT))
            {
                if (dr.Cycles == null) continue;
                foreach (var c in dr.Cycles)
                {
                    cnt += c.Count;
                }
            }
            return cnt;
        }

        public int GetTotalCycles()
        {
            var cnt = 0;
            foreach (var dr in _list.Where(x => !x.DT && !x.plannedDT))
            {
                if (dr.Cycles == null) continue;
                foreach (var c in dr.Cycles)
                {
                    cnt += c.Count;
                }
            }
            return cnt;
        }

        public List<Timeseg> Compress(bool dt, bool planned)
        {
            var nList = new List<Timeseg>();

            foreach (var dr in _list.Where(x=>x.DT == dt && x.plannedDT == planned).ToList().OrderBy(x => x.Start).ThenByDescending(x => x.End))
            {
                var ts = TestStart(dr, nList);
                if (ts == null)
                    nList.Add(dr);
                else
                {
                    if (ts.End < dr.End)
                    {
                        ts.End = dr.End;
                        foreach (var c in dr.callList.ToList())
                        {
                            ts.addCall(c);
                        }
                        foreach (var p in dr.plannedList.ToList())
                        {
                            ts.addPlanned(p);
                        }
                    }
                }
            }
            return nList;
        }
    }

    public class TmpSeg
    {
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
    }

    public class PlannedSeg : TmpSeg
    {
        public int ProductID { get; set; }
    }
}