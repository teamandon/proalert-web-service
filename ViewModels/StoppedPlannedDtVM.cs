﻿using System;
using ProAlert.Andon.Service.Models;

namespace ProAlert.Andon.Service.ViewModels
{
    public class StoppedPlannedDtVM
    {
        public PlannedDtLog PlannedDtLog { get; set; }
        public string Name { get; set; }

    }
}
