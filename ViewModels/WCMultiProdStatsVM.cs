﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vistech.Andon.Model.Models;

namespace Vistech.Andon.Service.ViewModels
{
    public class WCMultiProdStatsVM
    {
        // intended to hold one WC's contiguous time line that includes one or more WCProductTimelines.
        #region Private
        private String _CacheID;
        private DateTime _start;
        private DateTime _stop;
        private Double _plannedOperationTime;
        private Double _operationTime;
        private Double _totalDt;
        private Double _totalPlannedDt;
        private Decimal _availability;
        private Decimal _performance;
        private Decimal _quality;
        private Decimal _targetCycles;
        private Decimal _targetPieces;
        private Int32 _actualCycles;
        private Int32 _actualPieces;
        private Decimal _Scrap;
        private Decimal _mfg;
        private Decimal _matl;
        private Decimal _other;
        private Decimal _rework;
        private List<ProductTimelineStatsVM> _list;

        #endregion

        #region Members
        public int WorkCenterID { get; set; }
        public DateTime Start { get; set; }
        public DateTime Stop { get; set; }

        public Double OperationTime { get { return _operationTime; } }
        public Double PlannedOperationTime { get { return _plannedOperationTime; } }
        public Double TotalDT { get { return _totalDt; } }
        public Double TotalPlannedDT { get { return _totalPlannedDt; } }
        public Decimal Availablity { get { return _availability; } }
        public Decimal Performance { get { return _performance; } }
        public Decimal Quality { get { return _quality; } }
        public Decimal TargetCycles { get { return _targetCycles; } }
        public Decimal TargetPieces { get { return _targetPieces; } }
        public Int32 ActualCycles { get { return _actualCycles; } }
        public Int32 ActualPieces { get { return _actualPieces; } }
        #endregion

        #region Methods

        public void Setup(DateTime start, DateTime stop)
        {
            _start = start;
            _stop = stop;
            Start = _start;
            Stop = _stop;
            // take time frame and gather all time lines
            LoadTimelines();

        }
        #endregion

        #region Functions

        private void LoadTimelines()
        {
            using (var db = new AndonVistechContext())
            {
                var query = from p in db.WCProductTimelines
                    where p.WorkCenterID == WorkCenterID
                          && ((p.Start <= Start && p.Stop > Start) || (p.Start > Start && p.Start < Stop))
                    select p;
                foreach (var tl in query)
                {
                    //_list.Add(tl);
                }
            }
        }
        #endregion
    }
}