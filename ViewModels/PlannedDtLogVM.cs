﻿
using System.Collections.Generic;
using ProAlert.Andon.Service.Models;

namespace ProAlert.Andon.Service.ViewModels
{
    public class PlannedDtLogVM
    {
        public PlannedDtLog PlannedDtLog { get; set; }
        public IEnumerable<PlannedDt> PlannedDts { get; set; }
    }
}
