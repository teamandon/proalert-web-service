﻿using System.Collections.Generic;
using System.Linq;
using ProAlert.Andon.Service.Models;

namespace ProAlert.Andon.Service.ViewModels
{
    public class FacilityTimelineStatsVM
    {
        public List<ProductTimelineStatsVM> List { get; set; }

        public FacilityTimelineStatsVM()
        {
            List = new List<ProductTimelineStatsVM>();
        }

        public List<WCProductTimeline> GetTimeLineList()
        {
            var tlList = new List<WCProductTimeline>();
            foreach (var item in List.ToList())
            {
                //tlList.AddRange(item.TL.Timesegments.ToList());
            }
            return tlList;
        }

        public void Add(ProductTimelineStatsVM productTimelineStatsVm)
        {
            List.Add(productTimelineStatsVm);
        }
    }
}