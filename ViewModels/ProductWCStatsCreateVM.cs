﻿using System.Web.Mvc;
using ProAlert.Andon.Service.Models;

namespace ProAlert.Andon.Service.ViewModels
{
    public class ProductWCStatsCreateVM
    {
        public ProductWorkCenterStat ProductWorkCenterStat { get; set; }
        public SelectList Products { get; set; }
        public SelectList WorkCenters { get; set; }

    }
}
