﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vistech.Andon.Model.Models;

namespace Vistech.Andon.Service.ViewModels
{
    public class OEEVM
    {
        private static int? _pid;
        private static DateTime _start;
        private static DateTime _stop;
        private static int? _wcid;
        private TimeLine tl;

        /// <summary>
        /// Instantiate
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="wcid"></param>
        public OEEVM()
        {

        }

        public decimal Availablity { get { return tl == null ? 1 : tl.Availability; } }

        public string Cycles
        {
            get { return tl.CycleTarget.ToString("N1") + "@" + tl.TotalCycles.ToString("N0"); }
        }

        public TimeLine GetTimeLine
        {
            get { return tl; }
        }

        public string LastDTReason { get { return tl == null ? "" : tl.LastDTReason(); } }

        public decimal OEE
        {
            get
            {
                var oee = Availablity * Performance * Quality;
                return oee;
            }
        }

        public DateTime OEEStartTime { get { return _start; } set { _start = value; } }

        public DateTime OEEStopTime { get { return _stop; } set { _stop = value; } }

        public decimal OperationTimeInMin
        {
            get { return tl.TotOperationTime / 60000; }
        }

        public decimal Performance { get { return tl == null ? 1 : tl.Performance > 1 ? 1 : tl.Performance; } }

        public string PieceCnt
        {
            get { return tl.TotPiecesTarget.ToString("N0")  + "@" + tl.TotalPieces.ToString("N0"); }
        }

        public int? ProductID { get { return _pid; } set { _pid = value; } }

        public decimal Quality { get { return tl == null ? 1 : tl.Quality; } }

        public int Rework { get { return tl == null ? 1 : tl.rework; } }

        public string Scrap { get { return tl == null ? "" : tl.mfg.ToString() + "(" + tl.matl.ToString() + ")(" + tl.other.ToString() + ")"; } }

        public decimal Target
        {
            get
            {
                using (var db = new AndonVistechContext())
                {
                    db.Configuration.ProxyCreationEnabled = false;
                    var ta = db.ProductWorkCenterStats.FirstOrDefault(x => x.WorkCenterID == WorkCenterID);
                    if (ta == null)
                        return (decimal)0;

                    return ta.OEE;
                }
            }
        }

        public List<StatsVM> TimeSegmentList { get { return tl == null ? null : tl.TimeSegList; } }

        public string TotalDTInMin { get { return tl == null ? "" : tl.TotalDTInMin().ToString("N1"); } }

        public bool Weighted { get; set; }

        public int? WorkCenterID { get { return _wcid; } set { _wcid = value; } }

        public void SetValues() { CreateTimeLines(); }

        private void CreateTimeLines()
        {
            tl = new TimeLine();
            tl.Weighted = Weighted;
            var list = new List<PlannedSeg>();
            // find out how many different products were run in time frame.
            using (var db = new AndonVistechContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var query = from pdt in db.PlannedDtLogs
                            where pdt.WorkCenterID == WorkCenterID
                                  && pdt.PlannedDt.Name == "Product Change Over"
                                  &&
                                  ((pdt.Start <= OEEStartTime && pdt.Stop > OEEStartTime) ||
                                   (pdt.Start > OEEStartTime && pdt.Start < OEEStopTime))
                            orderby pdt.Start
                            select new { pdt.Start, pdt.ProductID };

                foreach (var dr in query.ToList())
                {
                    if (list.Count == 0)
                    {
                        list.Add(new PlannedSeg
                        {
                            Start = OEEStartTime
                        });
                    }
                    var item = list.FirstOrDefault(x => x.End == null);
                    if (item == null) continue;
                    item.End = dr.Start;
                    list.Add(new PlannedSeg
                    {
                        Start = dr.Start,
                        ProductID = dr.ProductID ?? 0
                    });
                }
            }

            var last = list.FirstOrDefault(x => x.End == null);
            if (last != null)
                last.End = OEEStopTime;

            // this section covers the possiblity that the time frame selected does not contain any Product Change Over entries.
            // So we have to manually create a time segment
            if (!list.Any())
            {
                // find the first planneddtlog before this time segment and use it's productid
                using (var db = new AndonVistechContext())
                {
                    var pid = 0;
                    list.Add(new PlannedSeg
                    {
                        Start = OEEStartTime,
                        End = OEEStopTime,
                        ProductID = pid
                    });
                }
            }

            // Search for Product Change Overs from before the time segment to populate the ProductID
            using (var db = new AndonVistechContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                foreach (var ts in list.Where(x => x.ProductID == 0).ToList())
                {
                    // search for the first Product Change Over before this time segment and use it's productID
                    var start = ts.Start;
                    var query = (from p in db.PlannedDtLogs
                        where p.WorkCenterID == WorkCenterID
                              && p.PlannedDt.Name == "Product Change Over"
                                                    && p.Stop <= start
                        orderby p.Stop descending
                        select p).FirstOrDefault();
                    if (query != null)
                        ts.ProductID = query.ProductID ?? 0;
                }
            }

            foreach (var dr in list.OrderBy(x=>x.Start))
            {
                // 
                var stats = new StatsVM
                {
                    WorkCenterID = _wcid,
                    OEEStartTime = dr.Start ?? DateTime.MinValue,
                    OEEEndTime = dr.End ?? DateTime.MaxValue,
                    ProductID = dr.ProductID 
                };
                stats.SetValues();
                tl.Add(stats);
            }
            tl.CalcTotOperationTime();
        }
    }

    public class TimeLine
    {
        public List<StatsVM> _list;

        public TimeLine()
        {
            _list = new List<StatsVM>();
            Count = 0;
        }

        public int matl { get { return _list.Sum(x => x.MATL); } }

        public int mfg { get { return _list.Sum(x => x.MFG); } }

        public int other { get { return _list.Sum(x => x.OTHER); } }

        public int rework { get { return _list.Sum(x => x.Rework); } }

        public decimal Availability
        {
            get
            {
                var list = _list.Where(x => !x.TimeSegPlanned).Select(dr => new Availability
                {
                    Available = (decimal)dr.OperationTime / dr.PlannedProduction,
                    OperationTime = dr.OperationTime
                }).ToList();

                foreach (var item in list)
                {
                    item.Value = Weighted ? item.Available * ((decimal)item.OperationTime / TotOperationTime) : item.Available;
                }

                if (list.Count == 0) return 0;

                decimal p = 0;
                if (Weighted)
                    p = list.Sum(x => x.Value);
                else
                    p = list.Average(x => x.Value);
                return p > 1 ? 1 : p;
            }
        }

        public decimal Performance
        {
            get
            {
                var list = _list.Where(x => !x.TimeSegPlanned).Select(dr => new Performance
                {
                    Pieces = dr.TotalPieces,
                    OperationTime = dr.OperationTime,
                    RunRate = dr.RunRate
                }).ToList();

                foreach (var dr in list)
                {
                    var performance = dr.Pieces / (dr.OperationTime / 60000) / dr.RunRate;
                    if (Weighted)
                        dr.Value = performance * (dr.OperationTime / TotOperationTime);
                    else
                    {
                        dr.Value = performance;
                    }
                }
                if (list.Count == 0) return 0;
                decimal p = 0;
                if (Weighted)
                    p = list.Sum(x => x.Value);
                else
                    p = list.Average(x => x.Value);
                return p > 1 ? 1 : p;
            }
        }

        public decimal Quality
        {
            get
            {
                var list = _list.Where(x => !x.TimeSegPlanned).Select(dr => new Quality
                {
                    OperationTime = dr.OperationTime,
                    Qual = dr.TotalPieces == 0 ? 0 : (decimal)(dr.TotalPieces - (dr.MATL + dr.MFG + dr.OTHER + dr.Rework)) / dr.TotalPieces
                }).ToList();

                foreach (var dr in list)
                {
                    dr.Value = Weighted ? dr.Qual * (dr.OperationTime / TotOperationTime) : dr.Qual;
                }

                if (list.Count == 0) return 0;

                decimal p = 0;
                if (Weighted)
                    p = list.Sum(x => x.Value);
                else
                    p = list.Average(x => x.Value);
                return p > 1 ? 1 : p;
            }
        }

        public decimal OEE { get { return Availability * Performance * Quality; } }

        public DateTime Start { get; set; }

        public DateTime Stop { get; set; }

        public List<StatsVM> TimeSegList { get { return _list; } }

        public decimal TotalCycles { get { return _list.Sum(x => x.TotalCycles); } }

        public int TotalPieces { get { return _list.Sum(x => x.TotalPieces); } }

        public decimal TotPiecesTarget { get { return _list.Sum(item => item.TargetPieces); } }

        public decimal TotOperationTime { get; set; }

        public bool Weighted { get; set; }

        public void Add(StatsVM stats)
        {
            _list.Add(stats);
            Count += 1;
        }

        public void CalcTotOperationTime()
        {
            foreach (var dr in _list)
            {
                TotOperationTime += dr.OperationTime;
            }
        }

        public void Clear()
        {
            _list.Clear();
        }

        public string LastDTReason()
        {
            return _list.Last().LastDTReason;
        }

        public int TotalDTInMin()
        {
            return _list.Sum(dr => dr.TotalDT)/60000;
        }

        public int Count { get; set; }

        public decimal CycleTarget { get { return _list.Sum(x => x.CycleTarget); } }
    }

    class Availability
    {
        public decimal Available { get; set; }
        public decimal OperationTime { get; set; }
        public decimal Value { get; set; }
    }

    class Performance
    {
        public decimal OperationTime { get; set; }
        public decimal Pieces { get; set; }
        public decimal RunRate { get; set; }
        public decimal Value { get; set; }
    }

    class Quality
    {
        public decimal OperationTime { get; set; }
        public decimal Qual { get; set; }
        public decimal Value { get; set; }
    }
    
}