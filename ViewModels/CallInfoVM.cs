﻿using System;
using ProAlert.Andon.Service.Models;


namespace ProAlert.Andon.Service.ViewModels
{
    public class CallInfoVM //: IDisposable
    {
        //private bool isDisposed = false;

        public CallLog cl { get; set; }

        public string WcName => cl.WorkCenter.Name;

        public string ProductName => cl.Product.Name;

        public string CallName => cl.Call.Name;

        public DateTime InitiateDt => cl.InitiateDt;
        public DateTime? ResponseDt => cl.ResponseDt;
        public DateTime? ResolveDt => cl.ResolveDt;

        public string IssueName
        {
            get
            {
                if (cl.Issue == null) return "";
                return cl.Issue.Name;
                //using (var db = new ProAndonContext())
                //{
                //    return db.Issues.FirstOrDefault(x => x.IssueID == cl.IssueID).Name;
                //}
            }
        }

        public string SubCatName
        {
            get
            {
                if (cl.SubCategory == null) return "";
                return cl.SubCategory.Name;
                //using (var db = new ProAndonContext())
                //{
                //    return db.SubCategories.FirstOrDefault(x => x.SubCategoryID == cl.SubCategoryID).Name;
                //}
            }
        }

        public string OperatorNotes => cl.OperatorNotes;
        public string Resolution => cl.Resolution;
        //public void Dispose()
        //{
        //    Dispose(true);
        //    GC.SuppressFinalize(this);
        //}

        //protected virtual void Dispose(bool disposing)
        //{
        //    if (!isDisposed)
        //    {
        //        if (disposing)
        //        {
        //            // cleanup managed objects by calling thier Dispose() methods.
        //        }
        //        // cleanup unmanaged objects
        //    }
        //    isDisposed = true;
        //}
    }
}