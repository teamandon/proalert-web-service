﻿using System.Collections.Generic;
using ProAlert.Andon.Service.Models;

namespace ProAlert.Andon.Service.ViewModels
{
    public class ActiveCallLogVM
    {
        public IEnumerable<CallLog> CallLogs { get; set; }
        public IEnumerable<Call> Calls  { get; set; }
    }
}