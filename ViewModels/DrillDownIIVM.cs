﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using ProAlert.Andon.Service.Common;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;

namespace ProAlert.Andon.Service.ViewModels
{
    public class DrillDownIIVM
    {
        #region Private
        private DateTime _start;
        private DateTime _stop;
        private double _plannedOperationTime;
        private double _operationTime;
        private double _totalDt;
        private double _totalPlannedDt;
        private decimal _availability;
        private decimal _performance;
        private decimal _quality;
        private decimal _targetCycles;
        private decimal _targetPieces;
        //private decimal _targetOEE;
        private int _actualCycles;
        private decimal _actualPieces;
        //private decimal _Scrap;
        private decimal _mfg;
        private decimal _matl;
        private decimal _other;
        private decimal _rework;
        //private int _lastCycleRead;
        //private int _lastCallRead;
        //private int _lastPlanRead;
        private string _lastDtReason;
        //private int _lastScrapRead;
        private DateTime _lastUpdate;
        private bool _isDirty;
        private bool _updating;
        private int _workCenterId;
        private string _productName;
        private string _pieceCnt;
        private string _cacheId;
        private decimal _cyclesPerMin;
        private IReadOnlyRepository _repo;

        #endregion

        public DrillDownIIVM()
        {
            
        }
        [JsonConstructor]
        public DrillDownIIVM(IReadOnlyRepository repo)
        {
            Repo = repo;
        }
        #region Members

        public string CacheID
        {
            get { return _cacheId; }
            set { _cacheId = value; }
        }

        public WCProductTimeline TL { get; set; }
        [JsonIgnore]
        public IReadOnlyRepository Repo
        {
            get { return _repo; }
            set { _repo = value; }
        }


        public DateTime LastUpdate
        {
            get { return _lastUpdate; }
            set { _lastUpdate = value; }
        }

        public bool IsDirty
        {
            get { return _isDirty; }
            set { _isDirty = value; }
        }

        public bool Updating
        {
            get { return _updating; }
            set { _updating = value; }
        }

        public int WorkCenterID
        {
            get { return _workCenterId; }
            set { _workCenterId = value; }
        }
        public DateTime Start { get; set; }
        public DateTime Stop { get; set; }

        public double OperationTime => _operationTime;
        public double OperationTimeInMin => _operationTime / 60000;
        public double PlannedOperationTime => _plannedOperationTime;
        public double TotalDT => _totalDt;
        public double TotalPlannedDT => _totalPlannedDt;
        public decimal Availablity => _availability;
        public decimal Performance => _performance;
        public decimal Quality => _quality;
        public decimal OEE => _availability * _performance * _quality;
        public decimal Target => TL == null ? 0 : TL.ProductWcStatsHistory == null ? 0 : TL.ProductWcStatsHistory.OEE;
        public decimal TargetCycles => _targetCycles;
        public decimal TargetPieces => _targetPieces;
        public int ActualCycles => _actualCycles;
        public decimal ActualPieces => _actualPieces;
        public string LastDTReason => _lastDtReason;
        public string TotalDTInMin => (_totalDt / 60000).ToString("N1");
        public string Scrap => _mfg.ToString() + "(" + _matl.ToString() + ")(" + _other.ToString() + ")";
        public decimal Rework => _rework;

        public decimal CyclesPerMin
        {
            get { return _cyclesPerMin; }
            set { _cyclesPerMin = value; }
        }

        public string PieceCnt
        {
            get { return _pieceCnt; }
            set { _pieceCnt = value; }
        }
        public string Cycles => TL == null ? "0@0" : CyclesPerMin.ToString("N2") + "@" + (OperationTimeInMin == 0 ? 0 : _actualCycles / OperationTimeInMin).ToString("N2");
        //public string Cycles { get { return _targetCycles.ToString("N1") + "@" + _actualCycles.ToString("N0"); } }
        public decimal Mfg => _mfg;
        public decimal Matl => _matl;
        public decimal Other => _other;
        public string WCProduct { get; set; }
        public bool WCPlanned { get; set; }

        public string ProductName
        {
            get { return _productName; }
            set { _productName = value; }
        }

        public bool Down { get; set; }

        public OEESummaryDetail OeeSummaryDetail => new OEESummaryDetail
        {
            CacheId = TL.Id.ToString(),
            TimeFrame =
                "OEE timeframe:&#013; " + timeconversions.UTCtoClient(null, Start).ToString("M/dd/yyyy HH:mm:ss") +
                " &#013; - " + timeconversions.UTCtoClient(null, Stop).ToString("M/dd/yyyy HH:mm:ss"),
            WcProductCyclesPerMin = TL.WCProduct + " (" + CyclesPerMin.ToString("N0") + ")",
            PlannedVsOperational =
                (TL.PlannedProduction / 60000).ToString("N0") + "@" + (TL.OperationTime / 60000).ToString("N0"),
            Down = TL.TotalDTInMin.ToString("N1"),
            LastDown = TL.LastDTReason,
            ScrapTitle =
                (TL.Mfg > 0 ? "MFG: " + TL.Mfg + " " : string.Empty) +
                (TL.Matl > 0 ? "Matl: " + TL.Matl + " " : string.Empty) +
                (TL.Other > 0 ? "Other: " + TL.Other + " " : string.Empty) +
                (TL.Rework > 0 ? "Rework: " + TL.Rework + " " : string.Empty),
            Scrap = TL.ScrapOneNumber.ToString("N0"),
            TargetVsActual = PieceCnt,
            Availability = TL.Availability.ToString("P1"),
            Performance = TL.Performance.ToString("P1"),
            Quality = TL.Quality.ToString("P1"),
            Oee = TL.OEE.ToString("P1"),
            AvailColor = TL.Availability < TL.ProductWcStatsHistory.Available ? "redCell" : "greenCell",
            PerformColor = TL.Performance < TL.ProductWcStatsHistory.Performance ? "redCell" : "greenCell",
            QualColor = TL.Quality < TL.ProductWcStatsHistory.Quality ? "redCell" : "greenCell",
            OeeColor = TL.OEE < TL.ProductWcStatsHistory.OEE ? "redCell" : "greenCell",
            WcStatusColor = TL.PlannedDt ? "lightblueCell" : TL.UnplannedDt ? "redCell" : !TL.Current ? "greenCell" : "lightgreenCell",
            Ocolor = "",
            TotalOeeAvg = "",
            TargetOeeAvg = "",
            Current = TL.Current.ToString()
        };
        #endregion

        #region Methods

        public void SetValues(DateTime start, DateTime stop)
        {
            _start = start;
            _stop = stop;
            Start = start;
            Stop = stop;
            _totalDt = SetDT();
            _plannedOperationTime = SetPlannedOperation();
            _operationTime = SetOperationTime();
            _availability = SetAvailability();
            _targetCycles = SetTargetCycles();
            _targetPieces = SetTargetPieces();
            _actualCycles = SetActualCycles();
            _actualPieces = SetActualPieces();
            _performance = SetPerformance();
            _mfg = SetMfg();
            _matl = SetMatl();
            _rework = SetRework();
            _other = SetOther();
            _quality = SetQuality();
            _lastDtReason = setLastDtReason();
            _pieceCnt = _targetPieces.ToString("N0") + "@" + (_actualPieces - (_mfg + _matl + _rework + _other)).ToString("N0");
            _cyclesPerMin = TL.ProductWcStatsHistory.CyclesPerHour/60;
            SetWCProduct();
            UpdateTl();
        }
        private void UpdateTl()
        {
            TL.ActualPieces = ActualPieces;
            TL.TotalDT = (decimal)TotalDT;
            TL.Availability = Availablity;
            TL.Performance = Performance;
            TL.Quality = Quality;
            TL.OEE = OEE;
            TL.OperationTime = OperationTime < 0 ? 0 : (decimal)OperationTime;
            TL.PlannedProduction = PlannedOperationTime < 0 ? 0 : (decimal)PlannedOperationTime;
            TL.PlannedDt = WCPlanned;
            TL.LastDTReason = LastDTReason;
            TL.ActualPieces = ActualPieces;
            TL.ActualCycles = ActualCycles;
            TL.UnplannedDt = Down;
            TL.Mfg = (decimal)_mfg;
            TL.Matl = _matl;
            TL.Rework = _rework;
            TL.Other = _other;
            TL.TotalPlannedDT = (decimal)TotalPlannedDT;
        }
        public void UpdateValues(DateTime start, DateTime stop)
        {
            if (_start != start)
            {
                SetValues(start, stop);
            }
            else
            {
                _stop = stop;
            }
        }
        #endregion

        #region Functions

        private void SetWCProduct()
        {
            if (TL.WorkCenter == null) return;
            ProductName = _repo.GetById<Product>(TL.ProductID).Name;
            WCProduct = TL.WorkCenter.Name + " : " + ProductName;
            Down = TL.Current ? TL.WorkCenter.UnplannedDt ?? false : TL.UnplannedDt;
            WCPlanned = TL.Current ? TL.WorkCenter.PlannedDt ?? false : TL.PlannedDt;
        }

        private TmpSeg TestStart(TmpSeg ts, IEnumerable<TmpSeg> l)
        {
            return l.FirstOrDefault(x => x.Start <= ts.Start && (x.Stop ?? _stop) > ts.Start);
        }

        //private double SetPlannedDT()
        //{
        //    return TL.Timesegments == null ? 0 : TL.Timesegments.Where(x => x.plannedDT == true).Sum(ts => (ts.Stop ?? DateTime.UtcNow).Subtract(ts.Start).TotalMilliseconds);
        //}

        private double SetPlannedOperation()
        {
            return _stop.Subtract(_start).TotalMilliseconds - _totalPlannedDt;
        }

        private double SetDT()
        {
            var planList = new List<TmpSeg>();

            double dTime = 0;
            double pTime = 0;

            if (TL == null)
            {
                _totalPlannedDt = 0;
                return 0;
            }

            //var cTl = Db.TlRepository.Get(x => x.WCProductTimelineID == TL.WCProductTimelineID, includeProperties: "Timesegments").Single();
            if (TL.Timesegments == null)
            {
                _totalPlannedDt = 0;
                return 0;
            }
            // only putting in time segments that belong to time frame of time line.  Don't have to weed out.  BUT, need to find partials
            foreach (var ts in TL.Timesegments.Where(x => (x.UnplannedDt == true || x.PlannedDt == true)))
            {
                var tsid = ts.Id;
                DateTime ts_stop = ts.Stop ?? Stop;

                var tempdt = ts_stop.Subtract(ts.Start < Start ? Start : ts.Start).TotalMilliseconds;
                double tempP = 0;
                //var pLogs = Db.PlannedDtLogRepository.Get(x => x.TimesegmentID == tsid && ((p)),
                //    includeProperties: "PlannedDt");
                if (ts.PlannedDtLogs != null)
                {
                    //if (pLogs.Any())
                    //{
                    foreach (var p in ts.PlannedDtLogs.OrderBy(s => s.Start).ThenByDescending(s => s.Stop ?? TL.Stop))
                    {
                        if (p.Stop <= TL.Start) continue;  // skip planned dt's that occurred before the start of selected time line.
                        var nts = new TmpSeg
                        {
                            Start = p.Start < TL.Start ? TL.Start : p.Start,
                            Stop = p.Stop ?? TL.Stop
                        };
                        var dr = TestStart(nts, planList);
                        if (dr == null)
                            planList.Add(nts);
                        else
                        {
                            if (dr.Stop < nts.Stop)
                                dr.Stop = nts.Stop;
                        }
                    }
                    foreach (var t in planList)
                    {
                        var d = (DateTime)t.Stop;
                        var s = (DateTime)t.Start;
                        tempP += d.Subtract(s).TotalMilliseconds;
                    }
                    planList.Clear();
                    //}
                }
                pTime += tempP;
                dTime += tempdt - tempP < 0 ? 0 : tempdt - tempP;

            }
            _totalPlannedDt = pTime;
            return dTime;

            //var planList = new List<ProductTimelineStatsVM.TmpSeg>();

            //double dTime = 0;
            //double pTime = 0;

            //if (TL == null)
            //{
            //    _totalPlannedDt = 0;
            //    return 0;
            //}

            ////var cTl = Db.TlRepository.Get(x => x.WCProductTimelineID == TL.WCProductTimelineID, includeProperties: "Timesegments").Single();
            //if (TL.Timesegments == null)
            //{
            //    _totalPlannedDt = 0;
            //    return 0;
            //}
            //// only putting in time segments that belong to time frame of time line.  Don't have to weed out.  BUT, need to find partials
            //foreach (var ts in TL.Timesegments.Where(x => (x.UnplannedDt == true || x.PlannedDt == true) && x.ProductChangeOver != true))
            //{
            //    var tsid = ts.Id;
            //    if (ts.Stop < Start) continue;
            //    DateTime ts_stop = ts.Stop ?? Stop;

            //    var tempdt = ts_stop.Subtract(ts.Start < Start ? Start : ts.Start).TotalMilliseconds;
            //    double tempP = 0;
            //    //var pLogs = Db.PlannedDtLogRepository.Get(x => x.TimesegmentID == tsid && ((p)),
            //    //    includeProperties: "PlannedDt");
            //    if (ts.PlannedDtLogs != null)
            //    {
            //        //if (pLogs.Any())
            //        //{
            //        foreach (var p in ts.PlannedDtLogs.OrderBy(s => s.Start).ThenByDescending(s => s.Stop ?? _stop))
            //        {
            //            if (p.Stop <= _start) continue;  // skip planned dt's that occurred before the start of selected time line.
            //            var nts = new ProductTimelineStatsVM.TmpSeg
            //            {
            //                Start = p.Start < _start ? _start : p.Start,
            //                Stop = p.Stop ?? _stop
            //            };
            //            var dr = TestStart(nts, planList);
            //            if (dr == null)
            //                planList.Add(nts);
            //            else
            //            {
            //                if (dr.Stop < nts.Stop)
            //                    dr.Stop = nts.Stop;
            //            }
            //        }
            //        foreach (var t in planList)
            //        {
            //            var d = (DateTime)t.Stop;
            //            var s = (DateTime)t.Start;
            //            tempP += d.Subtract(s).TotalMilliseconds;
            //        }
            //        planList.Clear();
            //        //}
            //    }
            //    pTime += tempP;
            //    dTime += tempdt - tempP < 0 ? 0 : tempdt - tempP;

            //}
            //_totalPlannedDt = pTime;
            //return dTime;
        }

        private double SetOperationTime()
        {
            return _plannedOperationTime - _totalDt;
        }

        private decimal SetTargetCycles()
        {
            return TL.ProductWcStatsHistory == null ? 0 : (decimal)_operationTime * (TL.ProductWcStatsHistory.CyclesPerHour / 3600000);
        }

        private decimal SetTargetPieces()
        {
            return TL.ProductWcStatsHistory == null ? 0 : _targetCycles * TL.ProductWcStatsHistory.UnitsPerCycle;
        }

        private int SetActualCycles()
        {
            var totCycles = 0;
            if (TL.Timesegments == null) return 0;
            foreach (var ts in TL.Timesegments.Where(x => (x.UnplannedDt != true) && (x.ProductChangeOver != true) && (x.PlannedDt != true)))
            {
                totCycles += ts.CycleSummary == null ? 0 : ts.CycleSummary.Count;
                //    if (ts.Stop <= Start) continue;
                //    var tsStart = ts.Start < Start ? Start : ts.Start;
                //    var tsStop = ts.Stop > Stop ? Stop : (ts.Stop ?? Stop);

                //    if (ts.Start >= Start && ts.Stop <= Stop && ts.CycleSummary != null)
                //        totCycles += _repo.GetById<CycleSummary>(ts.CycleSummaryID).Count;
                //    else
                //    {
                //        totCycles +=
                //            _repo.Get<CycleHistory>(
                //                x =>
                //                    x.WorkCenterID == TL.WorkCenterID &&
                //                    (x.UpdateDT >= tsStart && x.UpdateDT <= (tsStop))).Sum(x => x.Count);
                //    }
            }

            return totCycles;
            //return TL.Timesegments == null
            //    ? 0
            //    : TL.Timesegments.Where(x => (x.DT != true) && (x.ProductChangeOver != true) && (x.plannedDT != true))
            //    .Sum(ts => ts.CycleSummary == null ? 0 : ts.CycleSummary.Count);
        }

        private decimal SetActualPieces()
        {
            return TL.ProductWcStatsHistory == null ? 0 : _actualCycles * TL.ProductWcStatsHistory.UnitsPerCycle;
        }

        private decimal SetAvailability()
        {
            const double TOLERANCE = 0;
            var caps = _repo.GetById<ProductWCStatsHistory>(TL.ProductWcStatsHistoryID);
            var cap = (decimal)1;

            if (caps != null)
                cap = caps.AvailableCap == 0 ? 1 : caps.AvailableCap;

            var value = Math.Abs(_plannedOperationTime) <= TOLERANCE ? 0 : (decimal)_operationTime / (decimal)_plannedOperationTime;
            if (value < 0)
                value = 0;

            return value > cap ? cap : value;
        }

        private decimal SetPerformance()
        {
            var caps = _repo.GetById<ProductWCStatsHistory>(TL.ProductWcStatsHistoryID);
            var cap = (decimal)1;
            if (caps != null)
                cap = caps.PerformanceCap == 0 ? 1 : caps.PerformanceCap;

            var value = _targetCycles == 0 ? 0 : _actualCycles / _targetCycles;
            if (value < 0)
                value = 0;

            return value > cap ? cap : value;
        }

        private decimal SetQuality()
        {
            var caps = _repo.GetById<ProductWCStatsHistory>(TL.ProductWcStatsHistoryID);
            var cap = (decimal)1;
            if (caps != null)
                cap = caps.QualityCap == 0 ? 1 : caps.QualityCap;

            if (_actualPieces == 0) return 0;
            var value = (_actualPieces - (_mfg + _other + _rework)) / _actualPieces;

            if (value < 0)
                value = 0;

            return value > cap ? cap : value;
        }

        private int SetMfg()
        {
            if (TL.Timesegments == null) return 0;
            return TL.Timesegments.Where(ts => ts.Scraps != null).Sum(ts => ts.Scraps.Where(x => x.ScrapType.Name.Equals("Manufacturing")).Where(s => s.ScrapDT >= Start).Sum(s => s.ScrapCount));

            //var cnt = 0;
            //foreach (var ts in TL.Timesegments)
            //{
            //    if (ts.Scraps == null) continue;
            //    foreach (var s in ts.Scraps.Where(x => x.ScrapType.Name.Equals("Manufacturing")))
            //    {
            //        if (s.ScrapDT >= Start)
            //            cnt += s.ScrapCount;
            //    }
            //}
            //return cnt;
            //return (from ts in TL.Timesegments where ts.Scraps != null where ts.Scraps.Count != 0 select (from s in ts.Scraps where s.ScrapType != null where s.ScrapType.Name.Equals("Manufacturing") select s.ScrapCount).Sum()).Sum();
        }

        private int SetMatl()
        {
            if (TL.Timesegments == null) return 0;
            return TL.Timesegments.Where(ts => ts.Scraps != null).Sum(ts => ts.Scraps.Where(x => x.ScrapType.Name.Equals("Material")).Where(s => s.ScrapDT >= Start).Sum(s => s.ScrapCount));

            //return (from ts in TL.Timesegments where ts.Scraps != null where ts.Scraps.Count != 0 select (from s in ts.Scraps where s.ScrapType != null where s.ScrapType.Name.Equals("Material") select s.ScrapCount).Sum()).Sum();
        }

        private int SetRework()
        {
            if (TL.Timesegments == null) return 0;
            return TL.Timesegments.Where(ts => ts.Scraps != null).Sum(ts => ts.Scraps.Where(x => x.ScrapType.Name.Equals("Rework")).Where(s => s.ScrapDT >= Start).Sum(s => s.ScrapCount));

            //return (from ts in TL.Timesegments where ts.Scraps != null where ts.Scraps.Count != 0 select (from s in ts.Scraps where s.ScrapType != null where s.ScrapType.Name.Equals("Rework") select s.ScrapCount).Sum()).Sum();
        }

        private int SetOther()
        {
            if (TL.Timesegments == null) return 0;
            return TL.Timesegments.Where(ts => ts.Scraps != null).Sum(ts => ts.Scraps.Where(x => x.ScrapType.Name.Equals("Other")).Where(s => s.ScrapDT >= Start).Sum(s => s.ScrapCount));

            //return (from ts in TL.Timesegments where ts.Scraps != null where ts.Scraps.Count != 0 select (from s in ts.Scraps where s.ScrapType != null where s.ScrapType.Name.Equals("Other") select s.ScrapCount).Sum()).Sum();
        }

        private string setLastDtReason()
        {
            var dtReason = (from c in _repo.Get<CallLog>(x => x.WorkCenterId == TL.WorkCenterID && x.Call.DT && x.InitiateDt <= TL.Stop,
                o => o.OrderByDescending(r => r.InitiateDt), "Call")
                            select new { c.InitiateDt, c.Call.Name }).FirstOrDefault();
            var pdtReason = (from pdt in
                _repo.Get<PlannedDtLog>(x => x.WorkCenterID == TL.WorkCenterID && x.Start <= TL.Stop, o => o.OrderByDescending(r => r.Start), "PlannedDt")
                             select new { pdt.Start, pdt.PlannedDt.Name }).FirstOrDefault();

            if (dtReason == null && pdtReason == null)
                return "N/A";
            if (dtReason == null && pdtReason.Name != null)
                return pdtReason.Name;
            if (dtReason != null && pdtReason == null)
                return dtReason.Name;
            if (dtReason != null && pdtReason.Name != null)
                return dtReason.InitiateDt > pdtReason.Start ? dtReason.Name : pdtReason.Name;
            return "not sure";  // shouldn't ever happen.
        }
        #endregion
        public class TmpSeg
        {
            public DateTime? Start { get; set; }
            public DateTime? Stop { get; set; }
        }
    }
}