﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ProAlert.Andon.Service.Models;

namespace ProAlert.Andon.Service.ViewModels
{
    public class CallLogCloseVM
    {
        [Display(Name = "Call")]
        public string CallName { get; set; }
        public CallLog CallLog { get; set; }
        public IEnumerable<Issue> Issues { get; set; }
        public IEnumerable<SubCategory> SubCategories { get; set; }
    }
}
