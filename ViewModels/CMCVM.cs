﻿using System.Collections.Generic;
using ProAlert.Andon.Service.Models;

namespace ProAlert.Andon.Service.ViewModels
{
    public class CMCVM
    {
        public IEnumerable<Call> Calls { get; set; }
        public IEnumerable<CallLog> CallLogs { get; set; }
        public IEnumerable<Issue> Issues { get; set; }
        public IEnumerable<SubCategory> SubCategories { get; set; }
        public WorkCenter  WorkCenter { get; set; }

        public Employee Employee { get; set; } 
    }
}