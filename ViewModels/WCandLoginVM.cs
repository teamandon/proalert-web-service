﻿using System.Collections.Generic;
using ProAlert.Andon.Service.Models;

namespace ProAlert.Andon.Service.ViewModels
{
    public class WCandLoginVM
    {
        public virtual LoginViewModel LoginModel { get; set; }
        public virtual IEnumerable<WorkCenter>  WorkCenters { get; set; } 
    }
}