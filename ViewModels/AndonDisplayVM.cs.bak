﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ProAlert.Andon.Service.Common;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;
using AutoMessage = ProAlert.Andon.Service.Models.AutoMessage;
using Enumerations = ProAlert.Andon.Service.Common.Enumerations;

namespace ProAlert.Andon.Service.ViewModels
{
    public class AndonDisplayVM
    {
        private static bool _isForWc;
        private static IRepository _repo;

        #region Properties

        public bool IsForWc
        {
            get { return _isForWc; }
            set { _isForWc = value; }
        }

        public IRepository Repo
        {
            get { return _repo; }
            set { _repo = value; }
        }

        //public bool IsForWc { get; set; }

        [Display(Name="Maintenance")]
        public dispMsg Maintenance {
            get
            {
                const Enumerations.DisplayLine line = Enumerations.DisplayLine.Maintenance;
                var msg = Msg(line);

                var eType = Enumerations.CallType.Maintenance;
                var query = Query(eType);
                var myString = string.Empty;
                foreach (var dr in query)
                {
                    if (dr.DT)
                    {
                        if (dr.Responded)
                            myString += "<label style='padding-left:1em;'  class='lblyDT'>" + dr.workcenter + "-" + dr.call + "(" + dr.minpassed + ")</label>";
                        else
                            myString += "<label style='padding-left:1em;'  class='lblDT'>" + dr.workcenter + "-" + dr.call + "(" + dr.minpassed + ")</label>";
                    }
                    else
                    {
                        myString += "<label style='padding-left:1em;'  class='lblNonDT'>" + dr.workcenter + "-" + dr.call + "(" + dr.minpassed + ")</label>";
                    }
                }
                if (myString.Length == 0)
                    myString = "<label style='padding-left:1em;'  class='lblgOK'><b>OK</b></label>";
                msg.message += myString;
                return msg;
            }
        }

        [Display(Name="QC Tech")]
        public dispMsg QC
        {
            get
            {
                const Enumerations.DisplayLine line = Enumerations.DisplayLine.QC;
                var msg = Msg(line);

                var eType = Enumerations.CallType.QC;
                var query = Query(eType);
                var myString = string.Empty;
                foreach (var dr in query)
                {
                    if (dr.DT)
                    {
                        if (dr.Responded)
                            myString += "<label style='padding-left:1em;'  class='lblyDT'>" + dr.workcenter + "-" + dr.call + "(" + dr.minpassed + ")</label>";
                        else
                            myString += "<label style='padding-left:1em;'  class='lblDT'>" + dr.workcenter + "-" + dr.call + "(" + dr.minpassed + ")</label>";
                    }
                    else
                    {
                        myString += "<label style='padding-left:1em;'  class='lblNonDT'>" + dr.workcenter + "-" + dr.call + "(" + dr.minpassed + ")</label>";
                    }
                }
                if (myString.Length == 0)
                    myString = "<label style='padding-left:1em;'  class='lblgOK'><b>OK</b></label>";
                msg.message += myString;
                return msg;
            }
            
        }

        [Display(Name="Materials")]
        public dispMsg Materials {
            get
            {
                const Enumerations.DisplayLine line = Enumerations.DisplayLine.Materials;
                var msg = Msg(line);
                var eType = Enumerations.CallType.Materials;
                var query = Query(eType);
                var myString = string.Empty;
                foreach (var dr in query)
                {
                    if (dr.DT)
                    {
                        if (dr.Responded)
                            myString += "<label style='padding-left:1em;' class='lblyDT'>" + dr.workcenter + "-" + dr.call + "(" + dr.minpassed + ")</label>";
                        else
                            myString += "<label style='padding-left:1em;'  class='lblDT'>" + dr.workcenter + "-" + dr.call + "(" + dr.minpassed + ")</label>";
                    }
                    else
                    {
                        myString += "<label style='padding-left:1em;'  class='lblNonDT'>" + dr.workcenter + "-" + dr.call + "(" + dr.minpassed + ")</label>";
                    }
                }
                if (myString.Length == 0)
                    myString = "<label style='padding-left:1em;'  class='lblgOK'><b>OK</b></label>";
                msg.message += myString;
                return msg;
            }
        }

        public dispMsg Advisory
        {
            get
            {
                const Enumerations.DisplayLine line = Enumerations.DisplayLine.Advisory;

                return Msg(line);
            }
        }

        private static dispMsg Msg(Enumerations.DisplayLine line)
        {
            var dm = new dispMsg {message = string.Empty};
            var tzi = _repo.GetFirst<Employee>(x => x.LogIn == "tzi");

            var now = DateTime.Now;
            foreach (var dr in _repo.Get<AutoMessage>(x => x.DisplayLine == line))
            {
                if (dr.Image != null && dm.Image == null && _isForWc != true)
                    dm.Image = dr.Image;
                var start = dr.Start;
                if (tzi != null)
                    start = timeconversions.UTCtoClient(tzi.GetTimeZoneInstance(), dr.Start);
                var color = string.Empty;
                switch (dr.DisplayColors)
                {
                    case Enumerations.DisplayColors.Green:
                        color = "<label style='padding-left:1em;'  class='lblgOK'>";
                        break;
                    case Enumerations.DisplayColors.Yellow:
                        color = "<label style='padding-left:1em;'  class='lblNonDT'>";
                        break;
                    case Enumerations.DisplayColors.Red:
                        color = "<label style='padding-left:1em;'  class='lblDT'>";
                        break;
                }
                if (dr.Reoccurring != true)
                {
                    if (start <= now && start.AddMinutes(dr.Duration) >= now)
                        dm.message = SetMsg(dr, now, dm.message, color, start);
                    continue;
                }
                switch (dr.Interval)
                {
                    case Enumerations.AMInterval.Minutes:
                        var diff = (int)start.Subtract(now).TotalMinutes;
                        if (diff % dr.IntervalQty == 0)
                            dm.message += color + dr.Message + "</label>";
                        break;
                    case Enumerations.AMInterval.Hours:
                        var hrDiff = (int)now.Subtract(start).TotalHours;
                        if (hrDiff % dr.IntervalQty == 0)
                            dm.message = SetMsg(dr, now, dm.message, color, start);
                        break;
                    case Enumerations.AMInterval.Days:
                        if (dr.IntervalQty == 1)
                        {
                            dm.message = SetMsg(dr, now, dm.message, color, start);
                            break;
                        }
                        var dayDiff = (int)now.Subtract(start).TotalDays;

                        if (dayDiff % dr.IntervalQty == 0)
                        {
                            dm.message = SetMsg(dr, now, dm.message, color, start);
                        }
                        break;
                    case Enumerations.AMInterval.Weeks:
                        var weekDiff = (int)(now.Subtract(start).TotalDays / 7);
                        if (weekDiff % dr.IntervalQty == 0)
                            dm.message = SetMsg(dr, now, dm.message, color, start);
                        break;
                }
            }
            if (dm.message.Length == 0)
                dm.Image = null;
            return dm;
        }

        private static string SetMsg(AutoMessage dr, DateTime now, string msg, string color, DateTime start)
        {
            if (start.TimeOfDay <= now.TimeOfDay && start.AddMinutes(dr.Duration).TimeOfDay >= now.TimeOfDay)
                msg += color + dr.Message + "(" +
                       (dr.Duration - now.TimeOfDay.Subtract(start.TimeOfDay).TotalMinutes).ToString("N1") + ")" + "</label>";
            return msg;
        }

        #endregion


        private IEnumerable<maindisp> Query(Enumerations.CallType eType)
        {
            var query = from cl in _repo.Get<CallLog>(x => x.Call.Type == eType && ((x.Call.DT && x.ResolveDt == null) || (x.ResponseDt == null && !x.Call.DT)), includeProperties: "Call, WorkCenter")
                        orderby cl.Call.DT descending, cl.WorkCenter.Name, cl.Call.Name
                        select
                            new maindisp()
                            {
                                workcenter = cl.WorkCenter.Name,
                                Responded = cl.ResponseDt != null,
                                DT = cl.Call.DT,
                                call = cl.Call.Name,
                                InitiateDt = cl.InitiateDt,
                                minpassed = 0
                            };

            var nList = query.ToList();
            // Add Product Change Over to list...  Hmm
            foreach (var dr in nList)
            {
                dr.minpassed = (int)DateTime.UtcNow.Subtract(dr.InitiateDt).TotalMinutes;
            }
            return nList;
        }

    }

    public class dispMsg
    {
        public string message { get; set; }
        public byte[] Image { get; set; }
    }

    class maindisp
    {
        public string workcenter { get; set; }
        public bool Responded { get; set; }
        public bool DT { get; set; }
        public string call { get; set; }
        public int minpassed { get; set; }
        public DateTime InitiateDt { get; set; }
    }

}