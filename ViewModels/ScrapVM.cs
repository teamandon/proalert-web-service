﻿using System.Collections.Generic;
using ProAlert.Andon.Service.Models;

namespace ProAlert.Andon.Service.ViewModels
{
    public class ScrapVM
    {
        public Scrap Scrap { get; set; }
        public IEnumerable<WorkCenter> WorkCenters { get; set; }
        public IEnumerable<Product> Products { get; set; }
        public IEnumerable<LastStage> LastStages { get; set; }
        public IEnumerable<RejectCode> RejectCodes { get; set; }
        public IEnumerable<ScrapType> ScrapTypes { get; set; }
    }
}
