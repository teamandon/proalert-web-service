﻿using System.Collections.Generic;
using System.Linq;
using ProAlert.Andon.Service.Common;
using ProAlert.Andon.Service.Models;

namespace ProAlert.Andon.Service.ViewModels
{
    public class UnplannedDtRpts : BstBase
    {
        private List<UnplannedDtRpt> _list;

        public UnplannedDtRpts()
        {
            _list = new List<UnplannedDtRpt>();
        }

        public List<UnplannedDtRpt> Get()
        {
            return _list;
        }
        public void AddByTl(List<ProductTimelineStatsVM> list)
        {
            foreach (var i in list)
            {
                foreach (var ts in i.TL.Timesegments)
                {
                    foreach (var updt in ts.CallLogs.Where(x => x.Call.DT))
                    {
                        var p = updt.Employee == null ? "WC: " + i.TL.WorkCenter.Name : updt.Employee.LastName + ", " + updt.Employee.FirstName;
                        _list.Add(new UnplannedDtRpt
                        {
                            Name = updt.Call == null ? "" : updt.Call.Name,
                            OperatorNotes = updt.OperatorNotes,
                            ResolutionNotes = updt.Resolution,
                            Product = updt.Product == null ? "" : updt.Product.Name,
                            Initiated = updt.InitiateDt,
                            Response = updt.ResponseDt,
                            Resolved = updt.ResolveDt,
                            SignOff = updt.SignOffDT,
                            Maintenance = p,
                            Issue = updt.Issue == null ? "" : updt.Issue.Name,
                            Subcategory = updt.SubCategory == null ? "" : updt.SubCategory.Name,
                            WorkCenter = updt.WorkCenter == null ? "" : updt.WorkCenter.Name
                        });
                    }
                }
            }
        }

        public void Clear()
        {
            _list.Clear();
        }
    }
}
