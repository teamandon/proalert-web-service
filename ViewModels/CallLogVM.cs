﻿using ProAlert.Andon.Service.Models;

namespace ProAlert.Andon.Service.ViewModels
{
    public class CallLogVM
    {
        public CallLog CallLog { get; set; }
        public SubCategory SubCategory { get; set; }
    }
}