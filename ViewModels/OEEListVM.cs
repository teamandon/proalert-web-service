﻿using System.Collections.Generic;
using System.Linq;

namespace Vistech.Andon.Service.ViewModels
{
    public class OEEListVM
    {
        public List<OEEVM> List { get; set; }

        public OEEListVM()
        {
            List = new List<OEEVM>();
        }

        public List<StatsVM> GetTimeLineList()
        {
            var tlList = new List<StatsVM>();
            foreach (var item in List.ToList())
            {
                tlList.AddRange(item.TimeSegmentList);
            }
            return tlList;
        }

        public void Add(OEEVM oeevm)
        {
            List.Add(oeevm);
        }
    }
}