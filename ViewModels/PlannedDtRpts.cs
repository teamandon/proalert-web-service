﻿
using System.Collections.Generic;
using ProAlert.Andon.Service.Common;
using ProAlert.Andon.Service.Models;

namespace ProAlert.Andon.Service.ViewModels
{
    public class PlannedDtRpts : BstBase
    {
        private List<PlannedDtRpt> _list;

        public PlannedDtRpts()
        {
            _list=new List<PlannedDtRpt>();
        }

        public List<PlannedDtRpt> Get()
        {
            return _list;
        }

        public void AddByTl(List<ProductTimelineStatsVM> list)
        {
            foreach (var i in list)
            {
                foreach (var ts in i.TL.Timesegments)
                {
                    foreach (var pdt in ts.PlannedDtLogs)
                    {
                        //var p = pdt.Employee == null ? "WC: " + i.TL.WorkCenter.Name : pdt.Employee.LastName + ", " + pdt.Employee.FirstName;
                        _list.Add(new PlannedDtRpt
                        {
                            Name= pdt.PlannedDt == null ? "" : pdt.PlannedDt.Name,
                            Notes= pdt.Notes,
                            Product = pdt.Product == null ? "" : pdt.Product.Name,
                            Start = pdt.Start,
                            Stop = pdt.Stop,
                            WorkCenter = pdt.WorkCenter == null ? "" : pdt.WorkCenter.Name
                        });
                    }
                }
            }
        }

        public void Clear()
        {
            _list.Clear();
        }
    }
}
