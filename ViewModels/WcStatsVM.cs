﻿namespace ProAlert.Andon.Service.ViewModels
{
    public class WcStatsVM
    {
        public string LastDTReason { get; set; }
        public string TotalDTInMin { get; set; }
        public string Scrap { get; set; }
        public decimal Rework { get; set; }
        public string PieceCnt { get; set; }
        public string Cycles { get; set; }
        public string MinutesPerPiece { get; set; }
        public double OperationTimeInMin { get; set; }
    }
}
