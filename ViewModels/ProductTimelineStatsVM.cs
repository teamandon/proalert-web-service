﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Web.UI.WebControls;
using ProAlert.Andon.Service.Common;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;

namespace ProAlert.Andon.Service.ViewModels
{
    public class ProductTimelineStatsVM
    {
        //
        // GET: /Scrap/

        //  Intended to hold stats for one product/WC contiguous time line

        #region Private
        //private string _CacheID;
        private DateTime _start;
        private DateTime _stop;
        private double _plannedOperationTime;
        private double _runTime;
        private double _totalDt;
        private double _totalPlannedDt;
        private decimal _availability;
        private decimal _performance;
        private decimal _quality;
        private decimal _targetCycles;
        private decimal _targetPieces;
        private decimal _targetMinPerPiece;
        private decimal _actualMinPerPiece;
        //private decimal _targetOEE;
        private int _actualCycles;
        private decimal _actualPieces;
        //private decimal _Scrap;
        private decimal _mfg;
        private decimal _matl;
        private decimal _other;
        private decimal _rework;
        private int _lastCycleRead;
        private int _lastCallRead;
        private int _lastPlanRead;
        private string _lastDtReason;
        private int _lastScrapRead;
        private DateTime _lastUpdate;
        private bool _isDirty;
        private bool _updating;
        private int _workCenterId;
        private string _productName;
        private string _pieceCnt;
        private decimal _cyclesPerMin;
        private IRepository _repo;
        private string _user;
        private string _cacheId;
        //private WcOeeHeaderVM _wcOeeHeaderVm;

        #endregion

        #region Members

        /// <summary>
        /// WCProductTimeline should include "ShiftLog, ProductWCStatsHistory, Timesegments, WorkCenter, Product"
        /// </summary>
        /// <param name="tl"></param>
        public ProductTimelineStatsVM(WCProductTimeline tl, IRepository repo, bool rpt = false)
        {
            _repo = repo;
            TL = tl;
            // need method to fill all related data so we can run the calculations
            if (tl.Current && !rpt)
                Stop = DateTime.UtcNow;
            else
            {
                Stop = tl.Stop ?? DateTime.UtcNow;
            }

            if (rpt)
            {
                SetValuesTlRpt(TL.Start, Stop);
            }
            else
            {
                FillTl();
                SetValues(TL.Start, Stop);

            }
        }
        public ProductTimelineStatsVM() { }
        public IRepository repo
        {
            get { return _repo; }
            set { _repo = value; }
        }

        public string CacheID
        {
            get { return _cacheId; }
            set { _cacheId = value; }
        }

        public string User
        {
            get { return _user; }
            set { _user = value; }
        }

        public WCProductTimeline TL { get; set; }

        public DateTime LastUpdate
        {
            get { return _lastUpdate; }
            set { _lastUpdate = value; }
        }

        public bool IsDirty
        {
            get { return _isDirty; }
            set { _isDirty = value; }
        }

        public bool Updating
        {
            get { return _updating; }
            set { _updating = value; }
        }

        public int WorkCenterID
        {
            get { return _workCenterId; }
            set { _workCenterId = value; }
        }
        public DateTime Start { get; set; }
        public DateTime Stop { get; set; }

        public double OperationTime => _runTime;
        public double OperationTimeInMin => _runTime/60000;
        public double PlannedOperationTime => _plannedOperationTime;
        public double TotalDT => _totalDt;
        public double TotalPlannedDT => _totalPlannedDt;
        public decimal Availability => _availability;
        public decimal Performance => _performance;
        public decimal Quality => _quality;
        public decimal OEE => _availability*_performance*_quality;
        public decimal Target => TL == null ? 0 : TL.ProductWcStatsHistory == null ? 0 : TL.ProductWcStatsHistory.OEE;
        public decimal TargetCycles => _targetCycles;
        public decimal TargetPieces => _targetPieces;
        public decimal TargetMinPerPiece => _targetMinPerPiece;
        public decimal ActualMinPerPiece => _actualMinPerPiece;
        public int ActualCycles => _actualCycles;
        public decimal ActualPieces => _actualPieces;
        public string LastDTReason => _lastDtReason;
        public string TotalDTInMin => (_totalDt/60000).ToString("N1");
        public string Scrap => _mfg + "(" + _matl + ")(" + _other + ")";
        public decimal Rework => _rework;

        public string PieceCnt
        {
            get { return _pieceCnt; }
            set { _pieceCnt = value; }
        }

        public decimal CyclesPerMin
        {
            get { return _cyclesPerMin; }
            set { _cyclesPerMin = value; }
        }

        //public String Cycles { get { return _targetCycles.ToString("N1") + "@" + _actualCycles.ToString("N0"); } }
        public string Cycles => TL == null ? "0@0" : CyclesPerMin.ToString("N2") + "@" + (OperationTimeInMin == 0 ? 0 : _actualCycles / OperationTimeInMin).ToString("N2");
        public string Pieces => TL == null ? "0@0" : _targetMinPerPiece.ToString("N2") + "@" + _actualMinPerPiece.ToString("N2");
        public decimal Mfg => _mfg;
        public decimal Matl => _matl;
        public decimal Other => _other;
        public string WCProduct { get; set; }
        public bool PlannedDt { get; set; }

        public string ProductName
        {
            get { return _productName; }
            set { _productName = value; }
        }

        public bool UnplannedDt { get; set; }

        public WcOeeHeaderVM WcOeeHeaderVm => new WcOeeHeaderVM
        {
            Performance = Performance,
            Availability = Availability,
            Quality = Quality,
            OEE = OEE,
            Target = Target
        };

        public OeeHeaderWc OeeHeaderWc => new OeeHeaderWc
        {
            Performance = Performance.ToString("P1"),
            Availability = Availability.ToString("P1"),
            Quality = Quality.ToString("P1"),
            OEE = OEE.ToString("P1"),
            Target = Target.ToString("P1")
        };

        public WcStatsVM WcStatsVm => new WcStatsVM
        {
            LastDTReason = LastDTReason,
            TotalDTInMin = TotalDTInMin,
            Scrap = Scrap,
            Rework = Rework,
            PieceCnt = PieceCnt,
            Cycles = Cycles,
            MinutesPerPiece = Pieces,
            OperationTimeInMin = PlannedOperationTime / 60000
        };

        public StatsWc StatsWc => new StatsWc
        {
            LastDt = LastDTReason,
            TotalDt = TotalDTInMin,
            Scrap = Scrap,
            Rework = Rework.ToString("N0"),
            PieceCnt = PieceCnt,
            Cycles = Cycles,
            MinutesPerPiece = Pieces,
            OperationTimeInMin = (PlannedOperationTime / 60000).ToString("N1")+ " min"
        };

        public OEESummaryDetail OeeSummaryDetail => new OEESummaryDetail
        {
            CacheId = TL.Id.ToString(),
            TimeFrame =
                "OEE timeframe:&#013; " + timeconversions.UTCtoClient(null, Start).ToString("M/dd/yyyy HH:mm:ss") +
                " &#013; - " + timeconversions.UTCtoClient(null, Stop).ToString("M/dd/yyyy HH:mm:ss"),
            WcProductCyclesPerMin = TL.WCProduct + " (" + CyclesPerMin.ToString("N0") + ")",
            PlannedVsOperational =
                (TL.PlannedProduction / 60000).ToString("N0") + "@" + (TL.OperationTime / 60000).ToString("N0"),
            Down = TL.TotalDTInMin.ToString("N1"),
            LastDown = TL.LastDTReason,
            ScrapTitle =
                (TL.Mfg > 0 ? "MFG: " + TL.Mfg + " " : string.Empty) +
                (TL.Matl > 0 ? "Matl: " + TL.Matl + " " : string.Empty) +
                (TL.Other > 0 ? "Other: " + TL.Other + " " : string.Empty) +
                (TL.Rework > 0 ? "Rework: " + TL.Rework + " " : string.Empty),
            Scrap = TL.ScrapOneNumber.ToString("N0"),
            TargetVsActual = PieceCnt,
            Availability = TL.Availability.ToString("P1"),
            Performance = TL.Performance.ToString("P1"),
            Quality = TL.Quality.ToString("P1"),
            Oee = TL.OEE.ToString("P1"),
            AvailColor = TL.Availability < TL.ProductWcStatsHistory.Available ? "redCell" : "greenCell",
            PerformColor = TL.Performance < TL.ProductWcStatsHistory.Performance ? "redCell" : "greenCell",
            QualColor = TL.Quality < TL.ProductWcStatsHistory.Quality ? "redCell" : "greenCell",
            OeeColor = TL.OEE < TL.ProductWcStatsHistory.OEE ? "redCell" : "greenCell",
            WcStatusColor =
                TL.PlannedDt
                    ? "lightblueCell"
                    : TL.UnplannedDt ? "redCell" : !TL.Current ? "greenCell" : "lightgreenCell",
            Ocolor = "",
            TotalOeeAvg = "",
            TargetOeeAvg = "",
            Current = TL.Current.ToString()
        };
        #endregion

        #region Methods

        private void FillTl()
        {
            if (TL.Timesegments.Count == 0)
            {
                // record error 
                _repo.InsertError(new AppError
                {
                    ErrorMsg = "TlId: " + TL.Id + " did not have any associated timesegments for FillTl()",
                    ErrorDT = DateTime.UtcNow,
                    UserID = TL.WorkCenterID
                });
            }
            foreach (var ts in TL.Timesegments)
            {
                var dStop = ts.Stop ?? (TL.Current ? Stop : TL.Stop);
                ts.PlannedDtLogs.Clear();
                ts.Scraps.Clear();
                ts.CallLogs.Clear();
                foreach (
                    var p in
                    _repo.Get<PlannedDtLog>(x => x.TimesegmentID == ts.Id,
                        includeProperties: "PlannedDt, Employee, Product"))
                {
                    ts.PlannedDtLogs.Add(p);
                }
                foreach (
                    var c in
                    _repo.Get<CallLog>(x => x.TimesegmentID == ts.Id,
                        includeProperties: "Call, Employee, Product, Issue, SubCategory"))
                {
                    ts.CallLogs.Add(c);
                }
                foreach (
                    var s in //_repo.Get<Scrap>(x => x.TimesegmentID == tsid))
                    _repo.Get<Scrap>(x => x.TimesegmentID == ts.Id && !x.Allocated,
                        includeProperties: "ScrapType, Product, LastStage, RejectCode, Employee"))
                {
                    ts.Scraps.Add(s);
                }
                if (ts.CycleSummaryID != null)
                {
                    ts.CycleSummary = _repo.GetById<CycleSummary>(ts.CycleSummaryID);
                    foreach (
                        var ch in
                        _repo.Get<CycleHistory>(
                            x => x.WorkCenterID == TL.WorkCenterID && x.Id > ts.CycleSummary.LastCycleHistoryID && x.UpdateDT >= ts.Start && x.UpdateDT < dStop))
                    {
                        ts.CycleSummary.Count += ch.Count;
                        ts.CycleSummary.LastCycleHistoryID = ch.Id;
                    }
                }
                else
                {
                    // find out if there are any cyclehistory rows for the given timeframe. If so, create a new cyclesummary record
                    var total = 0;
                    var lastHist = 0;
                    foreach (
                        var ch in
                        _repo.Get<CycleHistory>(x => x.WorkCenterID == TL.WorkCenterID && x.UpdateDT >= ts.Start && x.UpdateDT < dStop))
                    {
                        total += ch.Count;
                        lastHist = ch.Id;
                    }
                    if (total > 0)
                    {
                        ts.CycleSummary = new CycleSummary
                        {
                            Count = total,
                            LastCycleHistoryID = lastHist
                        };
                    }
                }
                _repo.Update(ts, User);
            }
        }
        public void SaveCurrentProduct(int wcid, int pid)
        {
            var wc = _repo.GetById<WorkCenter>(wcid);
            var prod = _repo.GetById<Product>(pid);
            var pdtl = _repo.GetOne<PlannedDtLog>(x => x.WorkCenterID.Equals(wcid) && x.Stop == null);
            wc.ProductID = pid;
            pdtl.ProductID = pid;
            SetAndSaveProductWcStats(wcid, pid);
            _repo.Update(wc, _user);
            _repo.Update(pdtl, _user);
            TL.ProductID = pid;
            TL.WCProduct = wc.Name + " : " + prod.Name;
            _repo.Update(TL, _user);
            SendOEEStatus();
        }
        public void CreateCycles(CycleHistory c)
        {
            if (TL == null) return;
            var now = DateTime.UtcNow;
            _repo.Create(new CycleHistory
            {
                Count = c.Count
                , UpdateDT = now
                , WorkCenterID = c.WorkCenterID
            }, _user);
        }
        /// <summary>
        /// Creates a scrap record associated with the latest time segment
        /// </summary>
        /// <param name="scrap"></param>
        public void InsertScrap(Scrap scrap)
        {
            if (TL == null) return;
            var now = DateTime.UtcNow;
            var nScrap = new Scrap
            {
                ScrapTypeID = scrap.ScrapTypeID,
                Notes = scrap.Notes,
                ScrapDT = now,
                ScrapCount = scrap.ScrapCount,
                WorkCenterID = scrap.WorkCenterID,
                ProductID = scrap.ProductID,
                TimesegmentID = TL.Timesegments.Max(x => x.Id)
            };
            _repo.Create(nScrap, _user);
        }

        #region Calls
        /// <summary>
        /// create a non downtime call
        /// </summary>
        /// <param name="wccid"></param>
        /// <param name="pid"></param>
        public void CreateNonDTCall(WorkCenterCall wcc, int pid)
        {
            if (TL == null) return;
            var tsId = TL.Timesegments.Max(x => x.Id);
            var currTs = TL.Timesegments.Single(x => x.Id == tsId);
            if (currTs == null) return;

            var now = DateTime.UtcNow;
            _repo.Create(new CallLog
            {
                CallID = wcc.CallID,
                InitiateDt = now,
                ProductID = pid,
                WorkCenterId = wcc.WorkCenterID,
                TimesegmentID = tsId
            }, _user);
        }
        /// <summary>
        /// create DT Call
        /// </summary>
        /// <param name="callLog"></param>
        public void CreateCall(CallLogCreateVM vm)
        {
            if (TL == null) return;
            var now = DateTime.UtcNow;
            var tsId = TL.Timesegments.Max(x => x.Id);
            var currTs = TL.Timesegments.Single(x => x.Id == tsId);
            if (currTs == null) return;
            var callLog = new CallLog
            {
                InitiateDt = now,
                CallID = vm.CallID,
                IssueID = vm.IssueID,
                SubCategoryID = vm.SubCategoryID,
                ProductID = vm.ProductID,
                WorkCenterId = vm.WorkCenterId,
                OperatorNotes = vm.OperatorNotes
            };
            if (TL.WorkCenter.UnplannedDt == true || TL.WorkCenter.PlannedDt == true)
            {
                callLog.TimesegmentID = tsId;
                if (currTs.UnplannedDt != true)
                {
                    currTs.UnplannedDt = true;
                    PropigateDownFlags(currTs);
                }
                _repo.Create(callLog, _user);
            }
            else
            {
                // close the current timesegment
                currTs.Stop = now;
                _repo.Update(currTs, _user);

                // create new timesegment and associate with new calllog
                var nts = new Timesegment
                {
                    WCProductTimelineID = TL.Id,
                    Start = now,
                    ProductChangeOver = false,
                    PlannedDt = false,
                    UnplannedDt = true
                };
                callLog.Timesegment = nts;
                _repo.Create(callLog, _user);
                PropigateDownFlags(nts);
            }
        }

        /// <summary>
        /// Close DT Call
        /// </summary>
        /// <param name="vm"></param>
        /// <param name="signoff"></param>
        public void CloseCall(CallLogCloseVM vm, bool signoff = true)
        {
            //  Check if this is the only DT event open for the time segement
            //     if it is, close the time segment and open a new one.

            var now = DateTime.UtcNow;
            var clToUpdate = _repo.GetOne<CallLog>(x => x.Id == vm.CallLog.Id, includeProperties: "Call, Timesegment");

            clToUpdate.ResolveDt = now;
            clToUpdate.Resolution = vm.CallLog.Resolution;
            clToUpdate.IssueID = vm.CallLog.IssueID;
            clToUpdate.SubCategoryID = vm.CallLog.SubCategoryID;
            clToUpdate.EmployeeID = vm.CallLog.EmployeeID;
            //  SignoffDT being populated is the official closing of a call
            if (signoff)
                clToUpdate.SignOffDT = now;
            _repo.Update(clToUpdate, _user);
            var anyOthers =
                _repo.Get<CallLog>(x => x.TimesegmentID ==vm.CallLog.TimesegmentID && x.Call.DT && x.ResolveDt == null && x.Id != vm.CallLog.Id).Any() ||
                _repo.Get<PlannedDtLog>(x => x.TimesegmentID == vm.CallLog.TimesegmentID && x.Stop == null).Any();

            if (!anyOthers)
            {
                var ts = clToUpdate.Timesegment;
                if (ts != null)
                {
                    ts.Stop = now;
                    _repo.Update(ts, _user);
                    var nTs = new Timesegment { WCProductTimelineID = TL.Id, Start = now, UnplannedDt = false, PlannedDt = false, ProductChangeOver = false };
                    _repo.Create(nTs, _user);
                    PropigateDownFlags(nTs);
                }
                else
                {
                    _repo.InsertError(new AppError { ErrorDT = now, ErrorMsg = "Call Log ID: " + vm.CallLog.Id + " no associated ts.", Source = "Close Call"});
                }
            }
        }
        /// <summary>
        /// Save Call for later assigning to user related to pin
        /// </summary>
        /// <param name="vm"></param>
        //public void LaterCall(CallLogCloseVM vm)
        //{
        //    var now = DateTime.UtcNow;
        //    var clToUpdate = _repo.GetOne<CallLog>(x => x.Id == vm.CallLog.Id, includeProperties: "Call");

        //    clToUpdate.ResolveDt = now;
        //    clToUpdate.Resolution = vm.CallLog.Resolution;
        //    clToUpdate.IssueID = vm.CallLog.IssueID;
        //    clToUpdate.SubCategoryID = vm.CallLog.SubCategoryID;
        //    clToUpdate.EmployeeID = vm.CallLog.EmployeeID;

        //    // SignoffDT does not get populated.  Not officially closed
        //    _repo.Update(clToUpdate, _user);

        //    // can we closed the DT time segment?
        //    var anyOthers =
        //        _repo.Get<CallLog>(x => x.TimesegmentID == vm.CallLog.TimesegmentID && x.Call.DT && x.ResolveDt == null && x.Id != vm.CallLog.Id).Any() ||
        //        _repo.Get<PlannedDtLog>(x => x.TimesegmentID == vm.CallLog.TimesegmentID && x.Stop == null).Any();

        //    var wc = clToUpdate.WorkCenter;
        //    if (!anyOthers)
        //    {
        //        wc.UnplannedDt = false;
        //        wc.PlannedDt = false;
        //        // close the DT time segment
        //        var ts = clToUpdate.Timesegment;
        //        if (ts != null)
        //        {
        //            ts.Stop = now;
        //            _repo.Update(ts, _user);
        //            var nTs = new Timesegment {WCProductTimelineID = TL.Id, Start = now, UnplannedDt = false, PlannedDt = false, ProductChangeOver = false};
        //            _repo.Create(nTs, _user);
        //        }
        //        else
        //        {
        //            _repo.InsertError(new AppError
        //            {
        //                ErrorDT = now,
        //                ErrorMsg = "Call Log ID: " + vm.CallLog.Id + " no associated ts.",
        //                Source = "Later Call"
        //            });
        //        }
        //    }
        //    else
        //    {
        //        wc.PlannedDt = _repo.Get<PlannedDtLog>(x => x.WorkCenterID == vm.CallLog.WorkCenterId && x.TimesegmentID == vm.CallLog.TimesegmentID && x.Stop != null).Any();
        //        wc.UnplannedDt = true;
        //    }
        //    // Set Work Center properties
        //    _repo.Update(wc, _user);

        //}
        #endregion
        #region Planned DT
        /// <summary>
        /// Start a planned downtime
        ///     Affected tables
        ///     PlannedDtLog
        ///     WCProductTimeline
        ///     Timesegement
        ///     ShiftLog
        ///     WorkCenter
        /// </summary>
        /// <param name="pdtLog"></param>
        public void StartPlannedDt(PlannedDtLog pdtLog)
        {
            var now = DateTime.UtcNow;

            pdtLog.Start = now;
            // if Call is open, add the planned dt to the current time segment

            var name = _repo.GetById<PlannedDt>(pdtLog.PlannedDtID).Name;
            if (name.Equals("Product Change Over"))
            {
                //  Call to this method validates that there is not already a Planned DT event.
                // close current time line and start a new.

                // close current will not run if this is the first time the WC has been started.  'Cause there is not a current TL
                TL.WorkCenter.OEEStartTime = now;
                CloseCurrent(now);
                CreateNewCurrent(now, pdtLog);
            }
            else
            {
                //TL.Down = true;
                //_repo.Update(TL, _user);
                // Check to see if the WC is already in at DT event. If so, just add to existing timesegment
                //var cts = _repo.GetFirst<Timesegment>(x => x.WcProductTimeline.WorkCenterID == wc.Id && x.Stop == null && x.UnplannedDt == true);
                var cts = TL.Timesegments.Single(x => x.Stop == null);
                if (cts.UnplannedDt == true)
                {
                    cts.PlannedDt = true;
                    pdtLog.TimesegmentID = cts.Id;
                    _repo.Create(pdtLog, _user);
                    PropigateDownFlags(cts);
                }
                else
                {
                    // Not in DT, find current time segment and close if found.
                    cts.Stop = pdtLog.Start;
                    _repo.Update(cts, _user);

                    var ts = new Timesegment
                    {
                        WCProductTimelineID = TL.Id,
                        Start = now,
                        ProductChangeOver = false,
                        UnplannedDt = false,
                        PlannedDt = true
                    };
                    pdtLog.ProductID = TL.ProductID;
                    pdtLog.Timesegment = ts;
                    _repo.Create(pdtLog, _user);
                    PropigateDownFlags(ts);
                }
            }
        }
        /// <summary>
        /// Stop a planned downtime
        /// populating PlannedDtLog.Stop and closing Timesegment if closing only DT event open.  Then create new Timesegment
        /// </summary>
        /// <param name="pdtLog"></param>
        public void StopPlannedDt(PlannedDtLog pdtLog)
        {
            var pdtlToUpdate = _repo.GetFirst<PlannedDtLog>(x => x.Id == pdtLog.Id, includeProperties: "PlannedDt, Timesegment");
            var now = DateTime.UtcNow;
            pdtlToUpdate.Stop = now;
            pdtlToUpdate.Notes = pdtLog.Notes;

            var name = pdtlToUpdate.PlannedDt.Name;
            if (name.Equals("Product Change Over"))
            {
                pdtlToUpdate.Timesegment.Stop = now;
                var nts = new Timesegment
                {
                    Start = now,
                    ProductChangeOver = false,
                    PlannedDt = false,
                    UnplannedDt = false,
                    WCProductTimelineID = TL.Id
                };
                _repo.Create(nts, _user);
                PropigateDownFlags(nts);
            }
            else
            {
                var anyOpenDTCalls = _repo.Get<CallLog>(x => x.WorkCenterId == pdtlToUpdate.WorkCenterID && x.Call.DT && x.ResolveDt == null).Any();
                if (!anyOpenDTCalls)
                {
                    pdtlToUpdate.Timesegment.Stop = now;
                    var nts = new Timesegment
                    {
                        Start = now,
                        ProductChangeOver = false,
                        PlannedDt = false,
                        UnplannedDt = false,
                        WCProductTimelineID = TL.Id
                    };
                    _repo.Create(nts, _user);
                    PropigateDownFlags(nts);
                }
            }
            _repo.Update(pdtlToUpdate, _user);
        }
        #endregion
        /// <summary>
        /// close current WCProducttimeline and associated time segment and create new time line with first ts for product change over.
        /// </summary>
        /// <param name="stop"></param>
        public void CloseCurrent(DateTime stop)
        {
            // set TL.Current to false
            // set time segment where stop == null to passed time
            // Controller is looking for any planned DT events.  If there are, product change over cannot be called until they are closed.
            if (TL == null) return;
            // close the old


            TL.Current = false;
            TL.Stop = stop;
            var cts = TL.Timesegments.Single(x => x.Stop == null);

            if (cts == null)
            {
                // error - record and return
                return;
            }
            cts.Stop = stop;
            _repo.Update(cts, _user);
            _repo.Update(TL, _user);
        }
        /// <summary>
        /// Create a new timeline in Product Change Over status
        /// </summary>
        /// <param name="stop"></param>
        /// <param name="pdtLog"></param>
        public void CreateNewCurrent(DateTime stop, PlannedDtLog pdtLog)
        {
            // create a full related WCProductTimeline with Timesegment and PlannedDtLog records.
            // create the new
            pdtLog.Start = stop;
            var shifttimes = _repo.GetFirst<ShiftStartTimes>();
            var newShift = new ShiftLog
            {
                CreatedBy = _user,
                WorkCenterId = pdtLog.WorkCenterID,
                Start = stop
            };
            var tzi = _repo.GetFirst<Employee>(x => x.LogIn == "tzi");
            var now = DateTime.Now;
            var first = timeconversions.UTCtoClient(tzi.GetTimeZoneInstance(), shifttimes.FirstStart);
            var second = timeconversions.UTCtoClient(tzi.GetTimeZoneInstance(), shifttimes.SecondStart);
            var third = timeconversions.UTCtoClient(tzi.GetTimeZoneInstance(), shifttimes.ThirdStart);
            if (now.TimeOfDay >= first.AddMinutes(-5).TimeOfDay && now.TimeOfDay < second.TimeOfDay)
            { 
                newShift.Shift = Enumerations.Shift.First;
                newShift.Start = DateTime.UtcNow.Date.AddHours(shifttimes.FirstStart.TimeOfDay.Hours);
                newShift.Stop = newShift.Start.AddHours(8);
            }
            if (now.TimeOfDay >= second.AddMinutes(-5).TimeOfDay && now.TimeOfDay < third.TimeOfDay)
            {
                newShift.Shift = Enumerations.Shift.Second;
                newShift.Start = DateTime.UtcNow.Date.AddHours(shifttimes.SecondStart.TimeOfDay.Hours);
                newShift.Stop = newShift.Start.AddHours(8);
            }
            if (now.TimeOfDay >= third.AddMinutes(-5).TimeOfDay || now.TimeOfDay < first.TimeOfDay)
            {
                var baseValue = DateTime.UtcNow.Date;
                if (now.TimeOfDay < third.AddMinutes(-5).TimeOfDay)
                    baseValue = baseValue.AddDays(-1);
                newShift.Shift = Enumerations.Shift.Third;
                newShift.Start = baseValue.AddHours(shifttimes.ThirdStart.TimeOfDay.Hours);
                newShift.Stop = newShift.Start.AddHours(8);
            }
            // see if shift has already been created, if so, use it instead of creating new record.
            var bFoundLog = _repo.Get<ShiftLog>(x => x.Shift == newShift.Shift && stop >= DbFunctions.AddMinutes(x.Start, -5) && stop < x.Stop).Any();

            var nTl = new WCProductTimeline()
            {
                WorkCenterID = pdtLog.WorkCenterID,
                Start = stop,
                Current = true,
                UnplannedDt = true,
                PlannedDt = true,
                CreatedBy = _user
            };
            if (bFoundLog)
            {
                var log =
                    _repo.GetFirst<ShiftLog>(
                        x => x.Shift == newShift.Shift && stop >= DbFunctions.AddMinutes(x.Start, -5) && stop < x.Stop);

                nTl.ShiftLogId = log.Id;
            }
            else
            {
                nTl.ShiftLog = newShift;
            }

            var nTs = new Timesegment()
            {
                ProductChangeOver = true,
                UnplannedDt = false,
                PlannedDt = true,
                Start = stop,
                WcProductTimeline = nTl
            };


            pdtLog.Timesegment = nTs;
            //_repo.Create(nTl, _user);
            _repo.Create(nTs, _user);
            _repo.Create(pdtLog, _user);
            TL = nTl;
            PropigateDownFlags(nTs);
        }
        /// <summary>
        /// Create a new Timeline for shift change
        /// </summary>
        /// <param name="stop"></param>
        public void CreateNewCurrent(DateTime stop)
        {
            // create a full related WCProductTimeline with Timesegment and PlannedDtLog records.
            // create the new
            var workCenterId = TL.WorkCenterID ?? 0;
            var shifttimes = _repo.GetFirst<ShiftStartTimes>();
            var newShift = new ShiftLog
            {
                CreatedBy = _user,
                WorkCenterId = workCenterId,
                Start = stop
            };
            var tzi = _repo.GetFirst<Employee>(x => x.LogIn == "tzi");
            var now = DateTime.Now;
            var first = timeconversions.UTCtoClient(tzi.GetTimeZoneInstance(), shifttimes.FirstStart);
            var second = timeconversions.UTCtoClient(tzi.GetTimeZoneInstance(), shifttimes.SecondStart);
            var third = timeconversions.UTCtoClient(tzi.GetTimeZoneInstance(), shifttimes.ThirdStart);
            if (now.TimeOfDay >= first.AddMinutes(-5).TimeOfDay && now.TimeOfDay < second.TimeOfDay)
            {
                newShift.Shift = Enumerations.Shift.First;
                newShift.Start = DateTime.UtcNow.Date.AddHours(shifttimes.FirstStart.TimeOfDay.Hours);
                newShift.Stop = newShift.Start.AddHours(8);
            }
            if (now.TimeOfDay >= second.AddMinutes(-5).TimeOfDay && now.TimeOfDay < third.TimeOfDay)
            {
                newShift.Shift = Enumerations.Shift.Second;
                newShift.Start = DateTime.UtcNow.Date.AddHours(shifttimes.SecondStart.TimeOfDay.Hours);
                newShift.Stop = newShift.Start.AddHours(8);
            }
            if (now.TimeOfDay >= third.AddMinutes(-5).TimeOfDay || now.TimeOfDay < first.TimeOfDay)
            {
                var baseValue = DateTime.UtcNow.Date;
                if (now.TimeOfDay < third.AddMinutes(-5).TimeOfDay)
                    baseValue = baseValue.AddDays(-1);
                newShift.Shift = Enumerations.Shift.Third;
                newShift.Start = baseValue.AddHours(shifttimes.ThirdStart.TimeOfDay.Hours);
                newShift.Stop = newShift.Start.AddHours(8);
            }
            // see if shift has already been created, if so, use it instead of creating new record.
            var bFoundLog = _repo.Get<ShiftLog>(x => x.Shift == newShift.Shift && stop >= DbFunctions.AddMinutes(x.Start, -5) && stop < x.Stop).Any();

            var nTl = new WCProductTimeline()
            {
                WorkCenterID = workCenterId,
                Start = stop,
                ProductID = TL.ProductID,
                Current = true,
                UnplannedDt = false,
                PlannedDt = false,
                CreatedBy = _user,
                ProductWcStatsHistoryID = TL.ProductWcStatsHistoryID,
                WCProduct = TL.WCProduct
            };
            if (bFoundLog)
            {
                var log =
                    _repo.GetFirst<ShiftLog>(
                        x => x.Shift == newShift.Shift && stop >= DbFunctions.AddMinutes(x.Start, -5) && stop < x.Stop);

                nTl.ShiftLogId = log.Id;
            }
            else
            {
                nTl.ShiftLog = newShift;
            }

            var nTs = new Timesegment()
            {
                ProductChangeOver = false,
                UnplannedDt = false,
                PlannedDt = false,
                Start = stop,
                WcProductTimeline = nTl
            };
            
            //_repo.Create(nTl, _user);
            _repo.Create(nTs, _user);
            TL = nTl;
            PropigateDownFlags(nTs);
        }
        public void SetValuesTlRpt(DateTime start, DateTime stop)
        {
            _start = start;
            _stop = stop;
            TL.Start = _start;
            TL.Stop = _stop;
            Start = start;
            Stop = stop;
            SetLastSegments();
            AddNewSegments(true);
            //_totalPlannedDt = SetPlannedDT();
            _totalDt = SetDT();
            _plannedOperationTime = SetPlannedOperation();
            _runTime = SetRunTime();
            _availability = SetAvailability();
            _targetCycles = SetTargetCycles();
            _targetPieces = SetTargetPieces();
            _targetMinPerPiece = _targetPieces == 0 ? 0 : (decimal)OperationTimeInMin / _targetPieces;
            _actualCycles = SetActualCyclesRpt();
            _actualPieces = SetActualPieces();
            _actualMinPerPiece = _actualPieces == 0 ? 0 : (decimal)OperationTimeInMin / _actualPieces;
            _performance = SetPerformance();
            _mfg = SetMfg();
            _matl = SetMatl();
            _rework = SetRework();
            _other = SetOther();
            _quality = SetQuality();
            _lastDtReason = setLastDtReason();
            if (TL.ProductWcStatsHistory == null)
                _cyclesPerMin = 0;
            else
                _cyclesPerMin = TL.ProductWcStatsHistory.CyclesPerHour / 60;
            _pieceCnt = _targetPieces.ToString("N0") + "@" + (_actualPieces - (_mfg + _matl + _rework + _other)).ToString("N0");
            SetWCProduct();
            UpdateTl();

        }
        public void SetValues(DateTime start, DateTime stop)
        {
            _start = start;
            _stop = stop;
            TL.Start = _start;
            TL.Stop = _stop;
            Start = start;
            Stop = stop;
            SetLastSegments();
            //AddNewSegments(false);
            //_totalPlannedDt = SetPlannedDT();
            _totalDt = SetDT();
            _plannedOperationTime = SetPlannedOperation();
            _runTime = SetRunTime();
            _availability = SetAvailability();
            _targetCycles = SetTargetCycles();
            _targetPieces = SetTargetPieces();
            _targetMinPerPiece = _targetPieces == 0 ? 0 : (decimal) OperationTimeInMin/ _targetPieces ;
            _actualCycles = SetActualCycles();
            _actualPieces = SetActualPieces();
            _actualMinPerPiece = _actualPieces == 0 ? 0 : (decimal)OperationTimeInMin / _actualPieces;
            _performance = SetPerformance();
            _mfg = SetMfg();
            _matl = SetMatl();
            _rework = SetRework();
            _other = SetOther();
            _quality = SetQuality();
            _lastDtReason = setLastDtReason();
            if (TL.ProductWcStatsHistory == null)
                _cyclesPerMin = 0;
            else
                _cyclesPerMin = TL.ProductWcStatsHistory.CyclesPerHour/60;
            _pieceCnt =  _targetPieces.ToString("N0") + "@" + (_actualPieces -(_mfg + _matl + _rework + _other)).ToString("N0");
            SetWCProduct();
            UpdateTl();
      
        }
        private void UpdateTl()
        {
            TL.ActualPieces = ActualPieces;
            TL.TotalDT = (decimal)TotalDT;
            TL.Availability = Availability;
            TL.Performance = Performance;
            TL.Quality = Quality;
            TL.OEE = OEE;
            TL.OperationTime = (decimal)OperationTime;
            TL.PlannedProduction = (decimal)PlannedOperationTime;
            TL.PlannedDt = PlannedDt;
            TL.LastDTReason = LastDTReason;
            TL.ActualPieces = ActualPieces;
            TL.ActualCycles = ActualCycles;
            TL.UnplannedDt = UnplannedDt;
            TL.Mfg = (decimal)_mfg;
            TL.Matl = _matl;
            TL.Rework = _rework;
            TL.Other = _other;
            TL.TotalPlannedDT = (decimal)TotalPlannedDT;
        }
        public void UpdateValues(DateTime start, DateTime stop)
        {
            if (_start != start)
            {
                SetValues(start, stop);
            }
            else
            {
                _stop = stop;
            }
        }
        private void SetAndSaveProductWcStats(int wcid, int pid)
        {
            var pwcstats =
                _repo.GetOne<ProductWorkCenterStat>(x => x.WorkCenterID.Equals(wcid) && x.ProductID.Equals(pid));

            var statsHist = new ProductWCStatsHistory(pwcstats);
            TL.ProductWcStatsHistory = statsHist;
        }
        private void SetWCProduct()
        {
            if (TL.WorkCenter == null) return;
            ProductName = TL.ProductID == null ? "" : _repo.GetById<Product>(TL.ProductID).Name;
            WCProduct = TL.WorkCenter.Name + " : " + ProductName;

            if (TL.Current)
            {
                UnplannedDt = TL.WorkCenter.UnplannedDt ?? false;
                PlannedDt = TL.WorkCenter.PlannedDt ?? false;
            }
            else
            {
                UnplannedDt = TL.UnplannedDt;
                PlannedDt = TL.PlannedDt;
            }
        }
        private void AddNewSegments(bool? rpt)
        {
            // have to parse out time line
            //FillCalls();
            //FillPlannedDT();
            FillCycles(rpt);
            FillScrap();
        }
        private void SetLastSegments()
        {
            if (TL.Timesegments == null) return;
            if (TL.Timesegments.Count == 0)
            {
                _lastCallRead = 0;
                _lastCycleRead = 0;
                _lastPlanRead = 0;
                return;
            }
            _lastCallRead = TL.Timesegments.Max(x => x.CallLogs == null || x.CallLogs.Count == 0 ? 0 : x.CallLogs.Max(c => c == null ? 0 : c.Id));
            _lastCycleRead = TL.Timesegments.Max(x => x.CycleSummary == null || x.CycleSummary.Count == 0 ? 0 : x.CycleSummary.LastCycleHistoryID);
            _lastPlanRead = TL.Timesegments.Max(x => x.PlannedDtLogs == null || x.PlannedDtLogs.Count == 0 ? 0 : x.PlannedDtLogs.Max(p => p == null ? 0 : p.Id));
            _lastScrapRead =
                TL.Timesegments.Max(
                    x => x.Scraps == null || x.Scraps.Count == 0 ? 0 : x.Scraps.Max(s => s == null ? 0 : s.Id));
        }
        private void FillScrap()
        {
            var scrap = from s in _repo.Get<Scrap>(p => p.WorkCenterID == TL.WorkCenterID && p.Id > _lastScrapRead && !p.Allocated && p.ScrapDT >= TL.Start && p.ScrapDT <= TL.Stop, includeProperties: "ScrapType")
                        select s;

            foreach (var s in scrap)
            {
                foreach (var ts in TL.Timesegments.Where(x => x.Stop == null))
                {
                    if (s.ScrapDT >= ts.Start && s.ScrapDT <= (ts.Stop ?? TL.Stop))
                    {
                        s.TimesegmentID = ts.Id;
                        _repo.Update(s);
                    }
                }
            }
       }

        private void FillCycles(bool? rpt)
        {
            FillInBlanks(rpt);
            // first get list of WC's & ProductID's with start/stops
            var cTl = _repo.GetById<WCProductTimeline>(TL.Id);
            if (cTl.Timesegments == null) return;
            foreach (var item in cTl.Timesegments.ToList())
            {
                var itemStart = item.Start;
                var itemStop = item.Stop ?? TL.Stop;

                var query = from ch in _repo.Get<CycleHistory>(ch => ch.WorkCenterID == TL.WorkCenterID
                                                                     && ch.Id > _lastCycleRead
                                                                     && ch.UpdateDT >= itemStart &&
                                                                     ch.UpdateDT <= itemStop)
                    select ch;

                var lastcycle = 0;
                var cnt = 0;
                foreach (var dr in query.OrderBy(x => x.Id))
                {
                    _lastCycleRead = dr.Id;
                    lastcycle = dr.Id;
                    cnt += dr.Count;
                }

                if (cnt == 0) continue;
                var cs = _repo.GetById<CycleSummary>(item.CycleSummaryID);

                if (cs == null)
                {
                    cs = new CycleSummary
                    {
                        Count = cnt,
                        LastCycleHistoryID = lastcycle
                    };
                    _repo.Create(cs, _user);
                    item.CycleSummary = cs;
                    if (item.Id == 0)
                        _repo.Create(item, _user);
                    else
                        _repo.Update(item, _user);
                }
                else
                {
                    cs.Count += cnt;
                    cs.LastCycleHistoryID = lastcycle;
                    _repo.Update(cs, _user);
                }
            }
        }
        public void FillInBlanks(bool? rpt)
        {
            if (rpt ?? false) return;
            // Honestly, this should never be needed.
            if (TL.Timesegments.Count > 0)
            {
                if (!TL.Timesegments.Any(x => x.WCProductTimelineID == TL.Id && x.Stop == null))
                {
                    // add a new time segment where the last left off.
                    var lastStop = TL.Timesegments.Max(x => x.Stop);

                    _repo.Create(new Timesegment
                    {
                        WCProductTimelineID = TL.Id,
                        Start = lastStop ?? TL.Start,
                        UnplannedDt = false,
                        PlannedDt = false,
                        ProductChangeOver = false
                    }, _user);
                }
            }
        }
        private void PropigateDownFlags(Timesegment ts)
        {
            TL.WorkCenter.PlannedDt = ts.PlannedDt;
            TL.WorkCenter.UnplannedDt = ts.UnplannedDt;

            TL.PlannedDt = ts.PlannedDt ?? false;
            TL.UnplannedDt = ts.UnplannedDt ?? false;
            SendOEEStatus();
        }
        /// <summary>
        /// Send OEE Status to Mod2SQL application for PLC to read.
        /// </summary>
        private void SendOEEStatus()
        {
            var status = TL.PlannedDt || TL.UnplannedDt ? 0 : 1;
            var wcStatus = Convert.ToString(status, 2).PadLeft(8, '0'); //Convert to binary in a string
            var product = Convert.ToString(TL.ProductID ?? 0, 2).PadLeft(8, '0'); //Convert to binary in a string

            var oeeStatus =  wcStatus + product;
            var m2ss = new Mod2SqlStatus
            {
                OeeStatus = Convert.ToInt16(oeeStatus, 2),
                UpdateDt = DateTime.UtcNow,
                WorkCenterId = TL.WorkCenterID.ToString()
            };
            var om2ss = _repo.GetMod2SqlStatus(m2ss);
            if (om2ss != null)
            {
                om2ss.OeeStatus = m2ss.OeeStatus;
                om2ss.UpdateDt = m2ss.UpdateDt;
                _repo.UpdateMod2SqlStatus(om2ss);
            }
            else
            {
                _repo.InsertMod2SqlStatus(m2ss);
            }

        }
        #endregion

        #region Functions
        private TmpSeg TestStart(TmpSeg ts, IEnumerable<TmpSeg> l)
        {
            return l.FirstOrDefault(x => x.Start <= ts.Start && (x.Stop ?? TL.Stop) > ts.Start);
        }

        private double SetPlannedOperation()
        {
            return _stop.Subtract(_start).TotalMilliseconds - _totalPlannedDt;
        }

        private double SetDT()
        {
            var planList = new List<TmpSeg>();

            double dTime = 0;
            double pTime = 0;
 
            if (TL == null)
            {
                _totalPlannedDt = 0;
                return 0;
            }

            //var cTl = Db.TlRepository.Get(x => x.WCProductTimelineID == TL.WCProductTimelineID, includeProperties: "Timesegments").Single();
            if (TL.Timesegments == null)
            {
                _totalPlannedDt = 0;
                return 0;
            }
            // only putting in time segments that belong to time frame of time line.  Don't have to weed out.  BUT, need to find partials
            foreach (var ts in TL.Timesegments.Where(x => (x.UnplannedDt == true || x.PlannedDt == true) ))
            {
                var tsid = ts.Id;
                DateTime ts_stop = ts.Stop ?? Stop;
                
                var tempdt = ts_stop.Subtract(ts.Start < Start ? Start : ts.Start).TotalMilliseconds;
                double tempP = 0;
                //var pLogs = Db.PlannedDtLogRepository.Get(x => x.TimesegmentID == tsid && ((p)),
                //    includeProperties: "PlannedDt");
                if (ts.PlannedDtLogs != null)
                {
                    //if (pLogs.Any())
                    //{
                    foreach (var p in ts.PlannedDtLogs.OrderBy(s => s.Start).ThenByDescending(s => s.Stop ?? TL.Stop))
                    {
                        if (p.Stop <= TL.Start) continue;  // skip planned dt's that occurred before the start of selected time line.
                        var nts = new TmpSeg
                        {
                            Start = p.Start < TL.Start ? TL.Start : p.Start,
                            Stop = p.Stop ?? TL.Stop
                        };
                        var dr = TestStart(nts, planList);
                        if (dr == null)
                            planList.Add(nts);
                        else
                        {
                            if (dr.Stop < nts.Stop)
                                dr.Stop = nts.Stop;
                        }
                    }
                    foreach (var t in planList)
                    {
                        var d = (DateTime)t.Stop;
                        var s = (DateTime)t.Start;
                        tempP += d.Subtract(s).TotalMilliseconds;
                    }
                    planList.Clear();
                    //}
                }
                pTime += tempP;
                dTime += tempdt - tempP < 0 ? 0 : tempdt - tempP;

            }
            _totalPlannedDt = pTime;
            return dTime;
        }

        private double SetRunTime()
        {
            return _plannedOperationTime - _totalDt;
        }

        private decimal SetTargetCycles()
        {
            return TL.ProductWcStatsHistory == null ? 0 : (decimal)_runTime*(TL.ProductWcStatsHistory.CyclesPerHour/3600000);
        }

        private decimal SetTargetPieces()
        {
            return TL.ProductWcStatsHistory == null ? 0 : _targetCycles * TL.ProductWcStatsHistory.UnitsPerCycle;
        }

        private int SetActualCyclesRpt()
        {
            var totCycles = 0;
            if (TL.Timesegments == null) return 0;
            foreach (var ts in TL.Timesegments.Where(x => (x.UnplannedDt != true) && (x.ProductChangeOver != true) && (x.PlannedDt != true)))
            {
                totCycles += ts.CycleSummary == null ? 0 : ts.CycleSummary.Count;
            }

            return totCycles;
        }
        private int SetActualCycles()
        {
            var totCycles = 0;
            if (TL.Timesegments == null) return 0;
            foreach (var ts in TL.Timesegments.Where(x => (x.UnplannedDt != true) && (x.ProductChangeOver != true) && (x.PlannedDt != true)))
            {
                if (ts.Stop <= Start) continue;
                var tsStart = ts.Start < Start ? Start : ts.Start;
                var tsStop = ts.Stop > Stop ? Stop : (ts.Stop ?? Stop);

                if (tsStart >= Start && tsStop <= Stop && ts.CycleSummary != null)
                    totCycles += _repo.GetById<CycleSummary>(ts.CycleSummaryID).Count;
                else
                {
                    totCycles +=
                        _repo.Get<CycleHistory>(
                            x =>
                                x.WorkCenterID == TL.WorkCenterID &&
                                (x.UpdateDT >= tsStart && x.UpdateDT <= (tsStop))).Sum(x => x.Count);
                }
            }

            return totCycles;
        }

        private decimal SetActualPieces()
        {
            return TL.ProductWcStatsHistory == null ? 0 : _actualCycles * TL.ProductWcStatsHistory.UnitsPerCycle;
        }

        private decimal SetAvailability()
        {
            // needs cap removed.  Can't create time
            const double TOLERANCE = 0;
            var caps = _repo.GetFirst<ProductWCStatsHistory>(x => x.Id == TL.ProductWcStatsHistoryID);
            var cap = (decimal) 1;

            if (caps != null)
                cap = caps.AvailableCap == 0 ? 1 : caps.AvailableCap;
            if (_plannedOperationTime == 0) return 0;
            var value = Math.Abs(_plannedOperationTime) < TOLERANCE ? 0 : (decimal)_runTime / (decimal)_plannedOperationTime;
            if (value < 0)
                value = 0;

            return value > cap ? cap : value;
        }

        private decimal SetPerformance()
        {
            // cap works here because a machine can run faster than expected.
            var caps = _repo.GetFirst<ProductWCStatsHistory>(x => x.Id == TL.ProductWcStatsHistoryID);
            var cap = (decimal)1;
            if (caps != null)
                cap = caps.PerformanceCap == 0 ? 1 : caps.PerformanceCap;

            var value = _targetCycles == 0 ? 0 : _actualCycles/_targetCycles;
            if (value < 0)
                value = 0;

            return value > cap ? cap : value;
        }

        private decimal SetQuality()
        {
            // needs cap removed.  Pieces don't reproduce
            var caps = _repo.GetFirst<ProductWCStatsHistory>(x => x.Id == TL.ProductWcStatsHistoryID);
            var cap = (decimal)1;
            if (caps != null)
                cap = caps.QualityCap == 0 ? 1 : caps.QualityCap;

            if (_actualPieces == 0) return 0;
            var value = (_actualPieces - (_mfg + _other + _rework)) / _actualPieces;

            if (value < 0)
                value = 0;

            return value > cap ? cap : value;
        }

        private int SetMfg()
        {
            if (TL.Timesegments == null) return 0;
            return TL.Timesegments.Where(ts => ts.Scraps != null).SelectMany(ts => ts.Scraps.Where(x => x.ScrapType.Name.Equals("Manufacturing") && x.ScrapDT >= Start && !x.Allocated)).Sum(s => s.ScrapCount);
        }

        private int SetMatl()
        {
            if (TL.Timesegments == null) return 0;
            return TL.Timesegments.Where(ts => ts.Scraps != null).SelectMany(ts => ts.Scraps.Where(x => x.ScrapType.Name.Equals("Material") && x.ScrapDT >= Start && !x.Allocated)).Sum(s => s.ScrapCount);
        }

        private int SetRework()
        {
            if (TL.Timesegments == null) return 0;
            return TL.Timesegments.Where(ts => ts.Scraps != null).SelectMany(ts => ts.Scraps.Where(x => x.ScrapType.Name.Equals("Rework") && x.ScrapDT >= Start && !x.Allocated)).Sum(s => s.ScrapCount);
        }

        private int SetOther()
        {
            if (TL.Timesegments == null) return 0;
            return TL.Timesegments.Where(ts => ts.Scraps != null).SelectMany(ts => ts.Scraps.Where(x => x.ScrapType.Name.Equals("Other") && x.ScrapDT >= Start && !x.Allocated)).Sum(s => s.ScrapCount);
        }

        private Timesegment TestStart(Timesegment ts, IEnumerable<Timesegment> l)
        {
            return l.FirstOrDefault(x => x.Start <= ts.Start && (x.Stop ?? TL.Stop) > ts.Start);
        }
        private string setLastDtReason()
        {
            var dtReason = (from c in _repo.Get<CallLog>(x => x.WorkCenterId == TL.WorkCenterID && x.Call.DT && x.InitiateDt <= TL.Stop,
                o => o.OrderByDescending(r => r.InitiateDt), "Call")
                            select new { c.InitiateDt, c.Call.Name }).FirstOrDefault();
            var pdtReason = (from pdt in
                _repo.Get<PlannedDtLog>(x => x.WorkCenterID == TL.WorkCenterID && x.Start <= TL.Stop, o => o.OrderByDescending(r => r.Start), "PlannedDt")
                             select new { pdt.Start, pdt.PlannedDt.Name }).FirstOrDefault();

            if (dtReason == null && pdtReason == null)
                return "N/A";
            if (dtReason == null && pdtReason.Name != null)
                return pdtReason.Name;
            if (dtReason != null && pdtReason == null)
                return dtReason.Name;
            if (dtReason != null && pdtReason.Name != null)
                return dtReason.InitiateDt > pdtReason.Start ? dtReason.Name : pdtReason.Name;
            return "not sure";  // shouldn't ever happen.
        }
        #endregion

        public class TmpSeg
        {
            public DateTime? Start { get; set; }
            public DateTime? Stop { get; set; }
        }

    }
}