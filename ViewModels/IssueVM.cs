﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ProAlert.Andon.Service.Models;

namespace ProAlert.Andon.Service.ViewModels
{
    public class IssueVM
    {
        //public Issue Issue { get; set; }
        //public IEnumerable<SubCategory> SubCategories { get; set; }
        [ScaffoldColumn(false)]
        public int Id { get; set; }

        [Display(Name = "Problem"), StringLength(50), Required]
        public string Name { get; set; }

        [Display(Name = "Description"), StringLength(256)]
        public string Description { get; set; }
    }
}
