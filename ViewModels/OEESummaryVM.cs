﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Vistech.Andon.Service.ViewModels
{
    public class OEESummaryVM 
    {

        [Display(Name="P = ")]
        public decimal Performance { get; set; }
        [Display(Name="A = ")]
        public decimal Availablity { get; set; }
        [Display(Name="Q = ")]
        public decimal Quality { get; set; }
        [Display(Name="OEE = ")]
        public decimal OEE { get; set; }



        public void CalcAvgOEE()
        {
            using (var db = new AndonVistechContext())
            {
                var nSummary = new OEESumList();

                var stoptime = DateTime.Now;
                foreach (var dr in db.WorkCenters.Where(x => x.ProductID != null))
                {
                    var nOEEVM = new OEEVM
                    {
                        OEEStartTime = dr.OEEStartTime,
                        OEEStopTime = stoptime,
                        Weighted = false,
                        WorkCenterID = dr.WorkCenterID
                    };
                    nOEEVM.SetValues();
                    nSummary.Add(new OEESum
                    {
                        P = nOEEVM.Performance,
                        A = nOEEVM.Availablity,
                        Q = nOEEVM.Quality,
                        OEE = nOEEVM.OEE
                    });
                }

                Performance = nSummary.GetPerformance();
                Availablity = nSummary.GetAvailablity();
                Quality = nSummary.GetQuality();
                OEE = Performance * Availablity * Quality;
            }
        }


        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }

    public class OEESumList
    {
        private readonly List<OEESum> _list;
        public int Count { get; set; }

        public OEESumList()
        {
            _list = new List<OEESum>();
            Count = 0;
        }

        public void Add(OEESum oee)
        {
            _list.Add(oee);
            Count += 1;
        }

        public void Clear()
        {
            _list.Clear();
            Count = 0;
        }

        public decimal GetPerformance()
        {
            return _list.Sum(x => x.P)/Count;
        }

        public decimal GetAvailablity()
        {
            return _list.Sum(x => x.A)/Count;
        }

        public decimal GetQuality()
        {
            return _list.Sum(x => x.Q)/Count;
        }
    }

    public class OEESum
    {
        public decimal P { get; set; }
        public decimal A { get; set; }
        public decimal Q { get; set; }
        public decimal OEE { get; set; }
    }


}