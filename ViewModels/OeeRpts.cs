﻿using System.Collections.Generic;
using ProAlert.Andon.Service.Common;
using ProAlert.Andon.Service.Models;

namespace ProAlert.Andon.Service.ViewModels
{
    public class OeeRpts : BstBase
    {
        private List<OeeRpt> _oeeRpts;

        public OeeRpts()
        {
            _oeeRpts = new List<OeeRpt>();
        }

        public List<OeeRpt> List
        {
            get { return _oeeRpts; }
            set { _oeeRpts = value; }
        }

        public List<OeeRpt> Get()
        {
            return _oeeRpts;
        }

        public void AddByTl(List<ProductTimelineStatsVM> vm)
        {
            foreach (var i in vm)
            {
                _oeeRpts.Add(new OeeRpt
                {
                    WorkCenter = i.TL.WorkCenter.Name,
                    Product = i.ProductName ?? "N/A",
                    Start = i.Start,
                    Stop = i.Stop,
                    TotalDT = i.TotalDT / 60000,
                    TotalPlannedDT = i.TotalPlannedDT / 60000,
                    PlannedProduction = i.PlannedOperationTime / 60000,
                    OperationTime = i.OperationTime / 60000,
                    UnitsPerCycle = i.TL.ProductWcStatsHistory == null ? 0 : i.TL.ProductWcStatsHistory.UnitsPerCycle,
                    CyclesPerHour = i.TL.ProductWcStatsHistory == null ? 0 : i.TL.ProductWcStatsHistory.CyclesPerHour,
                    Availability = i.Availability,
                    Performance = i.Performance,
                    Quality = i.Quality,
                    OEE = i.OEE,
                    LastDtReason = i.LastDTReason ?? "N/A",
                    WCProduct = i.WCProduct ?? "N/A",
                    MFGScrap = i.TL.Mfg,
                    MatlScrap = i.TL.Matl,
                    ReworkScrap = i.TL.Rework,
                    OtherScrap = i.TL.Other,
                    ActualCycles = i.ActualCycles,
                    ActualPieces = i.ActualPieces
                });
            }

        }
    }
}