﻿namespace ProAlert.Andon.Service.ViewModels
{
    public class WcOeeHeaderVM
    {
        public decimal Performance { get; set; }
        public decimal Availability { get; set; }
        public decimal Quality { get; set; }
        public decimal OEE { get; set; }
        public decimal Target { get; set; }
    }
}
