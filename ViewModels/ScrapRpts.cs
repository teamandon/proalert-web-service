﻿using System.Collections.Generic;
using ProAlert.Andon.Service.Common;
using ProAlert.Andon.Service.Models;

namespace ProAlert.Andon.Service.ViewModels
{
    public class ScrapRpts : BstBase
    {
        private List<ScrapRpt> _list;

        public ScrapRpts()
        {
            _list = new List<ScrapRpt>();
        }

        public List<ScrapRpt> List
        {
            get { return _list; }
            set { _list = value; }
        }

        public List<ScrapRpt> Get()
        {
            return _list;
        }

        public void AddByTl(List<ProductTimelineStatsVM> list)
        {
            foreach (var i in list)
            {
                foreach (var j in i.TL.Timesegments)
                {
                    foreach (var s in j.Scraps)
                    {
                        var p = s.Employee == null
                            ? "WC: " + i.TL.WorkCenter.Name
                            : s.Employee.LastName + ", " + s.Employee.FirstName;
                        _list.Add(new ScrapRpt
                        {
                            WC = i.TL.WorkCenter.Name,
                            Product = i.ProductName ?? "N/A",
                            SignOffBy = p,
                            SignoffDT = s.SignoffDT,
                            ScrapDT = s.ScrapDT,
                            ScrapCount = s.ScrapCount,
                            Notes = s.Notes,
                            DefectLocation = s.DefectLocation,
                            ScrapType = s.ScrapType == null ? "" : s.ScrapType.Name,
                            LastStage = s.LastStage == null ? "" : s.LastStage.Name,
                            RejectCode = s.RejectCode == null ? "" : s.RejectCode.Name
                        });
                    }
                }
            }
        }
    }
}
