﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vistech.Andon.Service.Models;


namespace Vistech.Andon.Service.ViewModels
{
    public class DrillDownVM
    {
        //  Intended to hold stats for one product/WC contiguous time line
        #region Private
        private DateTime _start;
        private DateTime _stop;
        private Double _plannedOperationTime;
        private Double _operationTime;
        private Double _totalDt;
        private Double _totalPlannedDt;
        private Decimal _availability;
        private Decimal _performance;
        private Decimal _quality;
        private Decimal _targetCycles;
        private Decimal _targetPieces;
        private Decimal _targetOEE;
        private Int32 _actualCycles;
        private Int32 _actualPieces;
        private Decimal _Scrap;
        private Decimal _mfg;
        private Decimal _matl;
        private Decimal _other;
        private Decimal _rework;
        private String _lastDtReason;
        private DateTime _lastUpdate;
        private bool _isDirty;
        private bool _updating;
        private int _workCenterId;
        private string _productName;
        private List<Timesegment> _list;
        //private decimal _avgAvailability;
        //private decimal _avgPerformance;
        //private decimal _avgQuality;
        //private decimal _avgOee;

        #endregion

        #region Members

        public WCProductTimeline TL { get; set; }

        public List<Timesegment> List
        {
            get { return _list; }
            set { _list = value; }
        }

        public UnitOfWork Db
        {
            get { return _db; }
            set { _db = value; }
        }

        public DateTime LastUpdate
        {
            get { return _lastUpdate; }
            set { _lastUpdate = value; }
        }

        public bool IsDirty
        {
            get { return _isDirty; }
            set { _isDirty = value; }
        }

        public bool Updating
        {
            get { return _updating; }
            set { _updating = value; }
        }

        public int WorkCenterID
        {
            get { return _workCenterId; }
            set { _workCenterId = value; }
        }
        public DateTime Start { get; set; }
        public DateTime Stop { get; set; }
        public Double OperationTime { get { return _operationTime; } }
        public Double OperationTimeInMin { get { return _operationTime/60000; }}
        public Double PlannedProduction { get; set; }
        public Double PlannedOperationTime { get { return _plannedOperationTime; } }
        public Double TotalDT { get { return _totalDt; } }
        public Double TotalPlannedDT { get { return _totalPlannedDt; } }
        public Decimal Availability { get { return _availability; } } 
        public Decimal Performance { get { return _performance; } }
        public Decimal Quality { get { return _quality; } }
        public Decimal OEE { get { return _availability*_performance*_quality; } }

        //public Decimal AvgAvailability
        //{
        //    get { return _avgAvailability; }
        //    set { _avgAvailability = value; }
        //}

        //public Decimal AvgPerformance
        //{
        //    get { return _avgPerformance; }
        //    set { _avgPerformance = value; }
        //}

        //public Decimal AvgQuality
        //{
        //    get { return _avgQuality; }
        //    set { _avgQuality = value; }
        //}

        //public Decimal AvgOEE
        //{
        //    get { return _avgOee; }
        //    set { _avgOee = value; }
        //}

        public Decimal Target { get { return TL == null ? 0 : TL.ProductWcStatsHistory == null ? 0 : TL.ProductWcStatsHistory.OEE; } }
        public Decimal TargetCycles { get { return _targetCycles; } }
        public Decimal TargetPieces { get { return _targetPieces; } }
        public Int32 ActualCycles { get { return _actualCycles; } }
        public Int32 ActualPieces { get { return _actualPieces; } }
        public String LastDTReason { get { return _lastDtReason; }}
        public String TotalDTInMin { get { return (_totalDt/60000).ToString("N1"); }}
        public String Scrap { get { return _mfg.ToString() + "(" + _matl.ToString() + ")(" + _other.ToString() + ")"; } }
        public int ScrapOneNumber { get; set; }
        public Decimal Rework { get { return _rework; }}
        public String PieceCnt { get { return _targetPieces.ToString("N0") + "@" + (_actualPieces -(_mfg + _matl + _rework + _other)).ToString("N0"); }}
        public String Cycles { get { return _targetCycles.ToString("N1") + "@" + _actualCycles.ToString("N0"); }}
        public Decimal Mfg { get { return _mfg; } }
        public Decimal Matl { get { return _matl; }}
        public Decimal Other { get { return _other; }}
        public String WCProduct { get; set; }
        public bool WCPlanned { get; set; }

        public String ProductName
        {
            get { return _productName; }
            set { _productName = value; }
        }

        public bool Down { get; set; }
        #endregion

        #region Methods

        public DrillDownVM()
        {
            List = new List<Timesegment>();
        }

        public void ClearList()
        {
            List.Clear();
        }

        public void SetValues(DateTime start, DateTime stop)
        {
            _start = start;
            _stop = stop;
            Start = _start;
            Stop = _stop;
            _totalDt = SetDT();
            _plannedOperationTime = SetPlannedOperation();
            _operationTime = SetOperationTime();
            _availability = SetAvailability();
            _targetCycles = SetTargetCycles();
            _targetPieces = SetTargetPieces();
            _actualCycles = SetActualCycles();
            _actualPieces = SetActualPieces();
            _performance = SetPerformance();
            _mfg = SetMfg();
            _matl = SetMatl();
            _rework = SetRework();
            _other = SetOther();
            _quality = SetQuality();
            _lastDtReason = setLastDtReason();
        }

        public void UpdateValues(DateTime start, DateTime stop)
        {
            if (_start != start)
            {
                SetValues(start, stop);
            }
            else
            {
                _stop = stop;
            }
        }
        #endregion

        #region Functions

        private TmpSeg TestStart(TmpSeg ts, IEnumerable<TmpSeg> l)
        {
            return l.FirstOrDefault(x => x.Start <= ts.Start && (x.Stop ?? Stop) > ts.Start);
        }

        private Double SetDT()
        {
            var planList = new List<TmpSeg>();

            double dTime = 0;
            double pTime = 0;
            var cTl = Db.TlRepository.Get(x => x.WCProductTimelineID == TL.WCProductTimelineID, includeProperties: "Timesegments").Single();
            if (cTl.Timesegments == null) return 0;
            var tlStart = Start;
            var tlStop = Stop;
            foreach (var ts in cTl.Timesegments.Where(x => (x.DT == true || x.plannedDT == true) && x.ProductChangeOver != true && ((x.Start >= tlStart && x.Start < tlStop) || (x.Start < tlStart && x.Stop > tlStart))))
            {
                DateTime ts_stop = ts.Stop ?? Stop;
                if (ts_stop > Stop)
                {
                    ts_stop = Stop;
                    ts.Stop = Stop;
                }
                if (ts.Start < Start)
                    ts.Start = Start;
                var tempdt = ts_stop.Subtract(ts.Start).TotalMilliseconds;
                double tempP = 0;
                var pLogs = Db.PlannedDtLogRepository.Get(x => x.TimesegmentID == ts.TimesegmentID,
                    includeProperties: "PlannedDt");
                if (pLogs != null)
                {
                    //if (pLogs.Any())
                    //{
                    foreach (var p in pLogs.OrderBy(s => s.Start).ThenByDescending(s => s.Stop ?? Stop))
                    {
                        var nts = new TmpSeg
                        {
                            Start = p.Start,
                            Stop = p.Stop ?? Stop
                        };
                        var dr = TestStart(nts, planList);
                        if (dr == null)
                            planList.Add(nts);
                        else
                        {
                            if (dr.Stop < nts.Stop)
                                dr.Stop = nts.Stop;
                        }
                    }
                    foreach (var t in planList)
                    {
                        var d = (DateTime)t.Stop;
                        var s = (DateTime)t.Start;
                        tempP += d.Subtract(s).TotalMilliseconds;
                    }
                    planList.Clear();
                    //}
                }
                pTime += tempP;
                dTime += tempdt - tempP < 0 ? 0 : tempdt - tempP;

            }
            _totalPlannedDt = pTime;
            return dTime;
            //return TL.Timesegments == null ? 0 : dTime;
            //return TL.Timesegments == null ? 0 : TL.Timesegments.Where(x => x.DT == true && (x.plannedDT ?? false) == false && (x.ProductChangeOver ?? false) == false).Sum(ts => (ts.Stop ?? DateTime.UtcNow).Subtract(ts.Start).TotalMilliseconds);
        }


        private Double SetPlannedOperation()
        {
            return _stop.Subtract(_start).TotalMilliseconds - _totalPlannedDt;
        }

        private Double SetOperationTime()
        {
            return _plannedOperationTime - _totalDt;
        }

        private Decimal SetTargetCycles()
        {

            return TL.ProductWcStatsHistory == null ? 0 : (decimal)_operationTime*(TL.ProductWcStatsHistory.CyclesPerHour/3600000);
        }

        private Decimal SetTargetPieces()
        {
            return TL.ProductWcStatsHistory == null ? 0 : _targetCycles * TL.ProductWcStatsHistory.UnitsPerCycle;
        }

        private Int32 SetActualCycles()
        {
            var totCycles = 0;
            if (TL.Timesegments == null) return 0;
            foreach (var ts in TL.Timesegments.Where(x => (x.DT != true) && (x.ProductChangeOver != true) && (x.plannedDT != true)))
            {
                var tsStart = ts.Start < Start ? Start : ts.Start;
                var tsStop = ts.Stop > Stop ? Stop : ts.Stop;

                if (tsStart > Start && tsStop < Stop && ts.CycleSummary != null)
                    totCycles += Db.CycleSummaryRepository.GetByID(ts.CycleSummaryID).Count;
                else
                {
                    totCycles +=
                        Db.CycleHistoryRepository.Get(
                            x =>
                                x.WorkCenterID == WorkCenterID &&
                                (x.UpdateDT >= tsStart && x.UpdateDT <= (tsStop ?? Stop))).Sum(x => x.Count);
                }
            }

            return totCycles;
            //return TL.Timesegments == null
            //    ? 0
            //    : TL.Timesegments.Where(x => (x.DT != true) && (x.ProductChangeOver != true) && (x.plannedDT != true))
            //    .Sum(ts => ts.CycleSummary == null ? 0 : ts.CycleSummary.Count);
        }

        private Int32 SetActualPieces()
        {
            return TL.ProductWcStatsHistory == null ? 0 : _actualCycles * TL.ProductWcStatsHistory.UnitsPerCycle;
        }

        private Decimal SetAvailability()
        {
            const double TOLERANCE = 0;
            var caps = Db.ProductWcStatHistoryRepository.Get(x => x.ProductWCStatsHistoryID == TL.ProductWcStatsHistoryID).FirstOrDefault();
            var cap = (decimal) 1;

            if (caps != null)
                cap = caps.AvailableCap == 0 ? 1 : caps.AvailableCap;

            var value = Math.Abs(_plannedOperationTime) < TOLERANCE ? 0 : (decimal)_operationTime / (decimal)_plannedOperationTime;
            if (value < 0)
                value = 0;

            return value > cap ? cap : value;
        }

        private Decimal SetPerformance()
        {
            var caps = Db.ProductWcStatHistoryRepository.Get(x => x.ProductWCStatsHistoryID == TL.ProductWcStatsHistoryID).FirstOrDefault();
            var cap = (decimal)1;
            if (caps != null)
                cap = caps.PerformanceCap == 0 ? 1 : caps.PerformanceCap;

            var value = _targetCycles == 0 ? 0 : _actualCycles/_targetCycles;
            if (value < 0)
                value = 0;

            return value > cap ? cap : value;
        }

        private Decimal SetQuality()
        {
            var caps = Db.ProductWcStatHistoryRepository.Get(x => x.ProductWCStatsHistoryID == TL.ProductWcStatsHistoryID).FirstOrDefault();
            var cap = (decimal)1;
            if (caps != null)
                cap = caps.QualityCap == 0 ? 1 : caps.QualityCap;

            if (_actualPieces == 0) return 0;
            var value = (_actualPieces - (_mfg + _other + _rework)) / _actualPieces;

            if (value < 0)
                value = 0;

            return value > cap ? cap : value;
        }

        private Int32 SetMfg()
        {
            if (TL.Timesegments == null) return 0;
            return (from ts in TL.Timesegments where ts.Scraps != null where ts.Scraps.Count != 0 select (from s in ts.Scraps where s.ScrapType != null where s.ScrapType.Name.Equals("Manufacturing") select s.ScrapCount).Sum()).Sum();
        }

        private Int32 SetMatl()
        {
            if (TL.Timesegments == null) return 0;
            return (from ts in TL.Timesegments where ts.Scraps != null where ts.Scraps.Count != 0 select (from s in ts.Scraps where s.ScrapType != null where s.ScrapType.Name.Equals("Material") select s.ScrapCount).Sum()).Sum();
        }

        private Int32 SetRework()
        {
            if (TL.Timesegments == null) return 0;
            return (from ts in TL.Timesegments where ts.Scraps != null where ts.Scraps.Count != 0 select (from s in ts.Scraps where s.ScrapType != null where s.ScrapType.Name.Equals("Rework") select s.ScrapCount).Sum()).Sum();
        }

        private Int32 SetOther()
        {
            if (TL.Timesegments == null) return 0;
            return (from ts in TL.Timesegments where ts.Scraps != null where ts.Scraps.Count != 0 select (from s in ts.Scraps where s.ScrapType != null where s.ScrapType.Name.Equals("Other") select s.ScrapCount).Sum()).Sum();
        }



        private Timesegment TestStart(Timesegment ts, IEnumerable<Timesegment> l)
        {
            return l.FirstOrDefault(x => x.Start <= ts.Start && (x.Stop ?? TL.Stop) > ts.Start);
        }


        public class TmpSeg
        {
            public DateTime? Start { get; set; }
            public DateTime? Stop { get; set; }
        }

        private string setLastDtReason()
        {
            using (var db = new UnitOfWork())
            {
                var reason = db.CallLogRepository.Get(x => x.WorkCenterId == TL.WorkCenterID,
                    o => o.OrderByDescending(r => r.ResolveDt), "Call").FirstOrDefault();
                if (reason == null)
                    return "N/A";
                return reason.Call.Name;
            }
        }
        #endregion
    }
}