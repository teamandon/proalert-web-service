﻿using System.Collections.Generic;
using ProAlert.Andon.Service.Models;

namespace ProAlert.Andon.Service.ViewModels
{
    public class WCVM
    {
        //public IEnumerable<WorkCenterCall> WorkCenterCalls { get; set; }
        public IEnumerable<Call> Calls { get; set; }
        public IEnumerable<CallLog> CallLogs { get; set; }
        public IEnumerable<Product> Products { get; set; }
        public WorkCenter WorkCenter { get; set; }
        public Employee Employee { get; set; }
        public ProductTimelineStatsVM ProductTimelineStatsVm { get; set; }
        //public OEEVM  Oeevm { get; set; }
        //public StatsVM StatsVM { get; set; }
    }
}