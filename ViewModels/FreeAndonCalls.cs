﻿using System.Collections.Generic;
using System.Linq;
using ProAlert.Andon.Service.Common;
using ProAlert.Andon.Service.Models;

namespace ProAlert.Andon.Service.ViewModels
{
    public class FreeAndonCalls : BstBase
    {
        private List<FreeAndonCall> _list;

        public FreeAndonCalls()
        {
            _list = new List<FreeAndonCall>();
        }

        public List<FreeAndonCall> Get()
        {
            return _list;
        }

        public void AddByTl(List<ProductTimelineStatsVM> list)
        {
            foreach (var i in list)
            {
                foreach (var ts in i.TL.Timesegments)
                {
                    foreach (var call in ts.CallLogs.Where(x => !x.Call.DT))
                    {
                        _list.Add(new FreeAndonCall
                        {
                            WorkCenter = i.TL.WorkCenter == null ? "" : i.TL.WorkCenter.Name,
                            Product = call.Product == null ? "" : call.Product.Name,
                            Call = call.Call == null ? "" : call.Call.Name,
                            Initiated = call.InitiateDt,
                            Response = call.ResponseDt
                        });

                    }
                }
            }
        }
    }
}
