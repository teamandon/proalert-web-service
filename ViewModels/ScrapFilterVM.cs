﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ProAlert.Andon.Service.Models;

namespace ProAlert.Andon.Service.ViewModels
{
    public class ScrapFilterVM
    {
        public IEnumerable<Scrap> Scraps { get; set; }
        [Display(Name="Work Center")]
        public IEnumerable<WorkCenter> WorkCenters { get; set; }
        [Display(Name="Scrap Type")]
        public IEnumerable<ScrapType> ScrapTypes { get; set; }
        [Display(Name="Employee")]
        public IEnumerable<emp> Employees { get; set; }
    }
    public class emp
    {
        public int EmployeeID { get; set; }
        public string FullName { get; set; }
    }
}
