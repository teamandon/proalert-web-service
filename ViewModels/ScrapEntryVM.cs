﻿using System.Collections.Generic;
using ProAlert.Andon.Service.Models;

namespace ProAlert.Andon.Service.ViewModels
{
    public class ScrapEntryVM
    {
        public Scrap Scrap { get; set; }
        public IEnumerable<ScrapType> ScrapTypes { get; set; }
    }
}
