﻿using System;
using ProAlert.Andon.Service.Models;

namespace ProAlert.Andon.Service.ViewModels
{
    public class CycleVM
    {
        public int WorkCenterID { get; set; }
        public int ProductID { get; set; }
        public DateTime UpdateDt { get; set; }
        public int Count { get; set; }
        public ProductWorkCenterStat pwcs { get; set; }
    }
}