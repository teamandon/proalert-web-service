﻿using System.Collections.Generic;
using ProAlert.Andon.Service.Models;

namespace ProAlert.Andon.Service.ViewModels
{
    public class WorkCenterCallVM
    {
        public IEnumerable<WorkCenterCall> WorkCenterCalls { get; set; } 
        //public IEnumerable<WorkCenter> WorkCenters { get; set; } 
        public IEnumerable<Call> Calls { get; set; } 
    }
}