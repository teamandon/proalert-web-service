﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ProAlert.Andon.Service.Models;

namespace ProAlert.Andon.Service.ViewModels
{
    public class CallLogCreateVM
    {
        [Display(Name="Call")]
        public string CallName { get; set; }
        public int CallID { get; set; }
        public int? IssueID { get; set; }
        public int?  SubCategoryID { get; set; }
        public int ProductID { get; set; }
        public int WorkCenterId { get; set; }
        public string OperatorNotes { get; set; }
        public IEnumerable<Issue> Issues { get; set; }
        public IEnumerable<SubCategory> SubCategories { get; set; }

    }
}
