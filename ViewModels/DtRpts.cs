﻿using System.Collections.Generic;
using ProAlert.Andon.Service.Common;
using ProAlert.Andon.Service.Models;

namespace ProAlert.Andon.Service.ViewModels
{
    public class DtRpts : BstBase
    {
        private List<DtRpt> _list;

        public DtRpts()
        {
            _list = new List<DtRpt>();
        }

        public List<DtRpt> List
        {
            get { return _list; }
            set { _list = value; }
        }

        public List<DtRpt> Get()
        {
            return _list;
        }
    }
}