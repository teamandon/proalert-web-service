﻿using System.ComponentModel.DataAnnotations;

namespace ProAlert.Andon.Service.ViewModels
{
    public class ShiftTimeVM
    {
        [Display(Name = "First Shift")]
        public string First { get; set; }
        [Display(Name = "Second Shift")]
        public string Second { get; set; }
        [Display(Name = "Third Shift")]
        public string Third { get; set; }
    }
}
