﻿using System.Collections.Generic;
using System.Web.Mvc;
using ProAlert.Andon.Service.Models;


namespace ProAlert.Andon.Service.ViewModels
{
    public class EmployeeVM
    {
        public Employee Employee { get; set; }
        public IEnumerable<Position> Positions { get; set; } 
        public SelectList TimeZones { get; set; }
    }
}