﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProAlert.Andon.Service.Models
{
    public class RptDateTimeRange
    {
        [Display(Name="Start Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime StartDate { get; set; }
        public string StartTime { get; set; }
        public DateTime StopDate { get; set; }
        public string StopTime { get; set; }
    }
}
