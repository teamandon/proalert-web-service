using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using ProAlert.Andon.Service.Interfaces;

namespace ProAlert.Andon.Service.Models
{
    public partial class WorkCenterCall : Entity<int>
    {
        [Display(Name = "Work Center")]
        public int WorkCenterID { get; set; }

        [Display(Name = "Call")]
        public int CallID { get; set; }

        [Display(Name = "Order")]
        public int? OrderID { get; set; }

        // Navigations 
        [JsonIgnore]
        public virtual WorkCenter WorkCenter { get; set; }
        [JsonIgnore]
        public virtual Call Call { get; set; }
        //[NotMapped]
        //public string Pair { get; set; }
        //public int WorkCenterCallID { get; set; }
        //public int WorkCenterID { get; set; }
        //public int CallID { get; set; }
        //public Nullable<int> OrderID { get; set; }
        //public byte[] RowVersion { get; set; }
        //public virtual Call Call { get; set; }
        //public virtual WorkCenter WorkCenter { get; set; }
    }
}
