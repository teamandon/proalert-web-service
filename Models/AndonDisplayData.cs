﻿namespace ProAlert.Andon.Service.Models
{
    public class AndonDisplayData
    {
        public displayMsg Maintenance { get; set; }
        public displayMsg QC { get; set; }
        public displayMsg Materials { get; set; }
        public displayMsg Advisory { get; set; }
    }
}
