﻿namespace ProAlert.Andon.Service.Models
{
    public class ShippingLabelRequest
    {
        public int WorkCenterId { get; set; }
        public int ProductId { get; set; }
    }
}
