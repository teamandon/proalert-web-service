using System.Data.Entity.ModelConfiguration;

namespace ProAlert.Andon.Service.Models.Mapping
{
    public class WorkCenterCallMap : EntityTypeConfiguration<WorkCenterCall>
    {
        public WorkCenterCallMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.RowVersion)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("WorkCenterCall");
            this.Property(t => t.Id).HasColumnName("WorkCenterCallID");
            this.Property(t => t.WorkCenterID).HasColumnName("WorkCenterID");
            this.Property(t => t.CallID).HasColumnName("CallID");
            this.Property(t => t.OrderID).HasColumnName("OrderID");
            this.Property(t => t.RowVersion).HasColumnName("RowVersion");

            // Relationships
            this.HasRequired(t => t.Call)
                .WithMany(t => t.WorkCenterCalls)
                .HasForeignKey(d => d.CallID);
            this.HasRequired(t => t.WorkCenter)
                .WithMany(t => t.WorkCenterCalls)
                .HasForeignKey(d => d.WorkCenterID);

        }
    }
}
