using System.Data.Entity.ModelConfiguration;

namespace ProAlert.Andon.Service.Models.Mapping
{
    public class AutoMessageMap : EntityTypeConfiguration<Service.Models.AutoMessage>
    {
        public AutoMessageMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Message)
                .HasMaxLength(256);

            this.Property(t => t.RowVersion)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("AutoMessage");
            this.Property(t => t.Id).HasColumnName("AutoMessageID");
            this.Property(t => t.Message).HasColumnName("Message");
            this.Property(t => t.AudioFileID).HasColumnName("AudioFileID");
            this.Property(t => t.Duration).HasColumnName("Duration");
            this.Property(t => t.DisplayLine).HasColumnName("DisplayLine");
            this.Property(t => t.DisplayColors).HasColumnName("DisplayColors");
            this.Property(t => t.Start).HasColumnName("Start");
            this.Property(t => t.Reoccurring).HasColumnName("Reoccurring");
            this.Property(t => t.Interval).HasColumnName("Interval");
            this.Property(t => t.IntervalQty).HasColumnName("IntervalQty");
            this.Property(t => t.EmployeeID).HasColumnName("EmployeeID");
            this.Property(t => t.RowVersion).HasColumnName("RowVersion");
            this.Property(t => t.Image).HasColumnName("Image");

            // Relationships
            //this.HasRequired(t => t.AudioFile)
            //    .WithMany(t => t.AutoMessages)
            //    .HasForeignKey(d => d.AudioFileID);
            //this.HasOptional(t => t.Employee)
            //    .WithMany(t => t.AutoMessages)
            //    .HasForeignKey(d => d.EmployeeID);

        }
    }
}
