using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Vistech.Andon.Model.Models.Mapping
{
    public class DtCompressedMap : EntityTypeConfiguration<DtCompressed>
    {
        public DtCompressedMap()
        {
            // Primary Key
            this.HasKey(t => t.DtCompressedID);

            // Properties
            // Table & Column Mappings
            this.ToTable("DtCompressed");
            this.Property(t => t.DtCompressedID).HasColumnName("DtCompressedID");
            this.Property(t => t.WCProductTimelineID).HasColumnName("WCProductTimelineID");
            this.Property(t => t.Start).HasColumnName("Start");
            this.Property(t => t.Stop).HasColumnName("Stop");
            this.Property(t => t.Milliseconds).HasColumnName("Milliseconds");

            // Relationships
            //this.HasRequired(t => t.WCProductTimeline)
            //    .WithMany(t => t.DtCompresseds)
            //    .HasForeignKey(d => d.WCProductTimelineID);

        }
    }
}
