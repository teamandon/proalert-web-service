﻿

using System.Data.Entity.ModelConfiguration;

namespace ProAlert.Andon.Service.Models.Mapping
{
    public class Mod2SqlCountMap : EntityTypeConfiguration<Mod2SqlCount>
    {
        public Mod2SqlCountMap()
        {
            HasKey(t => t.WorkCenterId);

            ToTable("Mod2SqlCount");
            Property(t => t.WorkCenterId).HasColumnName("WorkCenterId");
            Property(t => t.Count).HasColumnName("Count");
            Property(t => t.Timestamp).HasColumnName("Timestamp");
        }
    }
}
