using System.Data.Entity.ModelConfiguration;

namespace ProAlert.Andon.Service.Models.Mapping
{
    public class CallLogMap : EntityTypeConfiguration<CallLog>
    {
        public CallLogMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.RowVersion)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("CallLog");
            this.Property(t => t.Id).HasColumnName("CallLogID");
            this.Property(t => t.CallID).HasColumnName("CallID");
            this.Property(t => t.InitiateDt).HasColumnName("InitiateDt");
            this.Property(t => t.ResponseDt).HasColumnName("ResponseDt");
            this.Property(t => t.ResolveDt).HasColumnName("ResolveDt");
            this.Property(t => t.WorkCenterId).HasColumnName("WorkCenterId");
            this.Property(t => t.EmployeeID).HasColumnName("EmployeeID");
            this.Property(t => t.Planned).HasColumnName("Planned");
            this.Property(t => t.IssueID).HasColumnName("IssueID");
            this.Property(t => t.SubCategoryID).HasColumnName("SubCategoryID");
            this.Property(t => t.Resolution).HasColumnName("Resolution");
            this.Property(t => t.OperatorNotes).HasColumnName("OperatorNotes");
            this.Property(t => t.ProductID).HasColumnName("ProductID");
            this.Property(t => t.SignOffDT).HasColumnName("SignOffDT");
            this.Property(t => t.TimesegmentID).HasColumnName("TimesegmentID");
            this.Property(t => t.RowVersion).HasColumnName("RowVersion");

            // Relationships
            //this.HasRequired(t => t.Call)
            //    .WithMany(t => t.CallLogs)
            //    .HasForeignKey(d => d.CallID);
            //this.HasOptional(t => t.Employee)
            //    .WithMany(t => t.CallLogs)
            //    .HasForeignKey(d => d.EmployeeID);
            //this.HasOptional(t => t.Issue)
            //    .WithMany(t => t.CallLogs)
            //    .HasForeignKey(d => d.IssueID);
            //this.HasOptional(t => t.Product)
            //    .WithMany(t => t.CallLogs)
            //    .HasForeignKey(d => d.ProductID);
            //this.HasOptional(t => t.SubCategory)
            //    .WithMany(t => t.CallLogs)
            //    .HasForeignKey(d => d.SubCategoryID);
            //this.HasOptional(t => t.Timesegment)
            //    .WithMany(t => t.CallLogs)
            //    .HasForeignKey(d => d.TimesegmentID);
            //this.HasRequired(t => t.WorkCenter)
            //    .WithMany(t => t.CallLogs)
            //    .HasForeignKey(d => d.WorkCenterId);

        }
    }
}
