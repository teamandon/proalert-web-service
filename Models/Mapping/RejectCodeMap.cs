using System.Data.Entity.ModelConfiguration;

namespace ProAlert.Andon.Service.Models.Mapping
{
    public class RejectCodeMap : EntityTypeConfiguration<RejectCode>
    {
        public RejectCodeMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Name)
                .HasMaxLength(50);

            this.Property(t => t.Description)
                .HasMaxLength(256);

            this.Property(t => t.RowVersion)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("RejectCode");
            this.Property(t => t.Id).HasColumnName("RejectCodeID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.DispOrder).HasColumnName("DispOrder");
            this.Property(t => t.RowVersion).HasColumnName("RowVersion");
        }
    }
}
