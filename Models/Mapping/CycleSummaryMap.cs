using System.Data.Entity.ModelConfiguration;

namespace ProAlert.Andon.Service.Models.Mapping
{
    public class CycleSummaryMap : EntityTypeConfiguration<CycleSummary>
    {
        public CycleSummaryMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("CycleSummary");
            this.Property(t => t.Id).HasColumnName("CycleSummaryID");
            this.Property(t => t.Count).HasColumnName("Count");
            this.Property(t => t.LastCycleHistoryID).HasColumnName("LastCycleHistoryID");
        }
    }
}
