using System.Data.Entity.ModelConfiguration;

namespace ProAlert.Andon.Service.Models.Mapping
{
    public class ProductWCStatsHistoryMap : EntityTypeConfiguration<ProductWCStatsHistory>
    {
        public ProductWCStatsHistoryMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.RowVersion)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();
            Property(x => x.Yield).HasPrecision(18, 5);
            Property(x => x.SetUpTime).HasPrecision(18, 5);
            Property(x => x.CyclesPerHour).HasPrecision(18, 5);
            Property(x => x.Yield).HasPrecision(18, 5);
            Property(x => x.SetUpTime).HasPrecision(18, 5);
            Property(x => x.CyclesPerHour).HasPrecision(18, 5);

            // Table & Column Mappings
            this.ToTable("ProductWCStatsHistory");
            this.Property(t => t.Id).HasColumnName("ProductWCStatsHistoryID");
            this.Property(t => t.ProductWorkCenterStatID).HasColumnName("ProductWorkCenterStatID");
            this.Property(t => t.WorkCenterID).HasColumnName("WorkCenterID");
            this.Property(t => t.ProductID).HasColumnName("ProductID");
            this.Property(t => t.CyclesPerHour).HasColumnName("CyclesPerHour").HasPrecision(18, 5);
            this.Property(t => t.UnitsPerCycle).HasColumnName("UnitsPerCycle").HasPrecision(18, 5);
            this.Property(t => t.Yield).HasColumnName("Yield").HasPrecision(18, 5);
            this.Property(t => t.SetUpTime).HasColumnName("SetUpTime");
            this.Property(t => t.Available).HasColumnName("Available");
            this.Property(t => t.Performance).HasColumnName("Performance");
            this.Property(t => t.Quality).HasColumnName("Quality");
            this.Property(t => t.AvailableCap).HasColumnName("AvailableCap");
            this.Property(t => t.PerformanceCap).HasColumnName("PerformanceCap");
            this.Property(t => t.QualityCap).HasColumnName("QualityCap");
            this.Property(t => t.RowVersion).HasColumnName("RowVersion");
        }
    }
}
