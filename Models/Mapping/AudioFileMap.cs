using System.Data.Entity.ModelConfiguration;

namespace ProAlert.Andon.Service.Models.Mapping
{
    public class AudioFileMap : EntityTypeConfiguration<AudioFile>
    {
        public AudioFileMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Name)
                .HasMaxLength(50);

            this.Property(t => t.Path)
                .HasMaxLength(256);

            this.Property(t => t.RowVersion)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("AudioFile");
            this.Property(t => t.Id).HasColumnName("AudioFileID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Path).HasColumnName("Path");
            this.Property(t => t.Sound).HasColumnName("Sound");
            this.Property(t => t.RepeatCnt).HasColumnName("RepeatCnt");
            this.Property(t => t.RowVersion).HasColumnName("RowVersion");
        }
    }
}
