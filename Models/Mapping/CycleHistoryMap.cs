using System.Data.Entity.ModelConfiguration;

namespace ProAlert.Andon.Service.Models.Mapping
{
    public class CycleHistoryMap : EntityTypeConfiguration<CycleHistory>
    {
        public CycleHistoryMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("CycleHistory");
            this.Property(t => t.Id).HasColumnName("CycleHistoryID");
            this.Property(t => t.WorkCenterID).HasColumnName("WorkCenterID");
            this.Property(t => t.Count).HasColumnName("Count");
            this.Property(t => t.UpdateDT).HasColumnName("UpdateDT");

            // Relationships
            //this.HasRequired(t => t.WorkCenter)
            //    .WithMany(t => t.CycleHistories)
            //    .HasForeignKey(d => d.WorkCenterID);

        }
    }
}
