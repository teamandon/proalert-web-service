﻿using System.Data.Entity.ModelConfiguration;

namespace ProAlert.Andon.Service.Models.Mapping
{
    public class Mod2SqlStatusMap : EntityTypeConfiguration<Mod2SqlStatus>
    {
        public Mod2SqlStatusMap()
        {
            HasKey(t => t.WorkCenterId);

            ToTable("Mod2SqlStatus");
            Property(t => t.WorkCenterId).HasColumnName("WorkCenterId");
            Property(t => t.OeeStatus).HasColumnName("OeeStatus");
            Property(t => t.UpdateDt).HasColumnName("UpdateDt");

        }
    }
}
