using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProAlert.Andon.Service.Models.Mapping
{
    public class CallMap : EntityTypeConfiguration<Call>
    {
        public CallMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Name)
                .HasMaxLength(50);

            this.Property(t => t.Description)
                .HasMaxLength(256);

            this.Property(t => t.RowVersion)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("Call");
            this.Property(t => t.Id).HasColumnName("CallID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.DT).HasColumnName("DT");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.AudioFileID).HasColumnName("AudioFileID");
            this.Property(t => t.RowVersion).HasColumnName("RowVersion");

            // Relationships
            //this.HasRequired(t => t.AudioFile)
            //    .WithMany(t => t.Calls)
            //    .HasForeignKey(d => d.AudioFileID);

        }
    }
}
