using System.Data.Entity.ModelConfiguration;

namespace ProAlert.Andon.Service.Models.Mapping
{
    public class EmailMap : EntityTypeConfiguration<Email>
    {
        public EmailMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Recipient)
                .HasMaxLength(100);

            this.Property(t => t.RecipientEmail)
                .HasMaxLength(256);

            // Table & Column Mappings
            this.ToTable("Email");
            this.Property(t => t.Id).HasColumnName("EmailID");
            this.Property(t => t.WorkCenterID).HasColumnName("WorkCenterID");
            this.Property(t => t.Recipient).HasColumnName("Recipient");
            this.Property(t => t.RecipientEmail).HasColumnName("RecipientEmail");
            this.Property(t => t.Delay).HasColumnName("Delay");
            this.Property(t => t.Interval).HasColumnName("Interval");
            this.Property(t => t.Down).HasColumnName("Down");
            this.Property(t => t.Availablilty).HasColumnName("Availablilty");
            this.Property(t => t.Performance).HasColumnName("Performance");
            this.Property(t => t.Quality).HasColumnName("Quality");
            this.Property(t => t.OEE).HasColumnName("OEE");
            this.Property(t => t.Scrap).HasColumnName("Scrap");
            Property(t => t.SetupExceeded).HasColumnName("SetupExceeded");

            // Relationships
            //this.HasOptional(t => t.WorkCenter)
            //    .WithMany(t => t.Emails)
            //    .HasForeignKey(d => d.WorkCenterID);

        }
    }
}
