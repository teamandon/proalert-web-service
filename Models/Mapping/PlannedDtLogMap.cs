using System.Data.Entity.ModelConfiguration;

namespace ProAlert.Andon.Service.Models.Mapping
{
    public class PlannedDtLogMap : EntityTypeConfiguration<PlannedDtLog>
    {
        public PlannedDtLogMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Notes)
                .HasMaxLength(256);

            this.Property(t => t.RowVersion)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("PlannedDtLog");
            this.Property(t => t.Id).HasColumnName("PlannedDtLogID");
            this.Property(t => t.PlannedDtID).HasColumnName("PlannedDtID");
            this.Property(t => t.Start).HasColumnName("Start");
            this.Property(t => t.Stop).HasColumnName("Stop");
            this.Property(t => t.Notes).HasColumnName("Notes");
            this.Property(t => t.EmployeeID).HasColumnName("EmployeeID");
            this.Property(t => t.WorkCenterID).HasColumnName("WorkCenterID");
            this.Property(t => t.ProductID).HasColumnName("ProductID");
            this.Property(t => t.TimesegmentID).HasColumnName("TimesegmentID");
            this.Property(t => t.RowVersion).HasColumnName("RowVersion");

            // Relationships
            //this.HasOptional(t => t.Employee)
            //    .WithMany(t => t.PlannedDtLogs)
            //    .HasForeignKey(d => d.EmployeeID);
            //this.HasRequired(t => t.PlannedDt)
            //    .WithMany(t => t.PlannedDtLogs)
            //    .HasForeignKey(d => d.PlannedDtID);
            //this.HasOptional(t => t.Product)
            //    .WithMany(t => t.PlannedDtLogs)
            //    .HasForeignKey(d => d.ProductID);
            //this.HasOptional(t => t.Timesegment)
            //    .WithMany(t => t.PlannedDtLogs)
            //    .HasForeignKey(d => d.TimesegmentID);
            //this.HasRequired(t => t.WorkCenter)
            //    .WithMany(t => t.PlannedDtLogs)
            //    .HasForeignKey(d => d.WorkCenterID);

        }
    }
}
