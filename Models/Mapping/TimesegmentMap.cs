using System.Data.Entity.ModelConfiguration;

namespace ProAlert.Andon.Service.Models.Mapping
{
    public class TimesegmentMap : EntityTypeConfiguration<Timesegment>
    {
        public TimesegmentMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("Timesegment");
            this.Property(t => t.Id).HasColumnName("TimesegmentID");
            this.Property(t => t.WCProductTimelineID).HasColumnName("WCProductTimelineID");
            this.Property(t => t.CycleSummaryID).HasColumnName("CycleSummaryID");
            this.Property(t => t.Start).HasColumnName("Start");
            this.Property(t => t.Stop).HasColumnName("Stop");
            this.Property(t => t.UnplannedDt).HasColumnName("DT");
            this.Property(t => t.PlannedDt).HasColumnName("plannedDT");
            this.Property(t => t.ProductChangeOver).HasColumnName("ProductChangeOver");

            // Relationships
            //this.HasOptional(t => t.CycleSummary)
            //    .WithMany(t => t.Timesegments)
            //    .HasForeignKey(d => d.CycleSummaryID);
            //this.HasRequired(t => t.WCProductTimeline)
            //    .WithMany(t => t.Timesegments)
            //    .HasForeignKey(d => d.WCProductTimelineID);

        }
    }
}
