using System.Data.Entity.ModelConfiguration;

namespace ProAlert.Andon.Service.Models.Mapping
{
    public class WorkCenterMap : EntityTypeConfiguration<WorkCenter>
    {
        public WorkCenterMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Name)
                .HasMaxLength(50);

            this.Property(t => t.Description)
                .HasMaxLength(256);

            this.Property(t => t.RowVersion)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("WorkCenter");
            this.Property(t => t.Id).HasColumnName("WorkCenterID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.UnplannedDt).HasColumnName("Down");
            this.Property(t => t.PlannedDt).HasColumnName("Planned");
            this.Property(t => t.OEEStartTime).HasColumnName("OEEStartTime");
            this.Property(t => t.ProductID).HasColumnName("ProductID");
            Property(t => t.Serial).HasColumnName("Serial");
            this.Property(t => t.WorkCenterGUIOptionsID).HasColumnName("WorkCenterGUIOptionsID");
            Property(t => t.WorkCenterPrintingOptionsID).HasColumnName("WorkCenterPrintingOptionsID");
            this.Property(t => t.RowVersion).HasColumnName("RowVersion");

            // Relationships
            //this.HasOptional(t => t.Product)
            //    .WithMany(t => t.WorkCenters)
            //    .HasForeignKey(d => d.ProductID);

        }
    }
}
