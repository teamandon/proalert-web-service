using System.Data.Entity.ModelConfiguration;

namespace ProAlert.Andon.Service.Models.Mapping
{
    public class AppErrorMap : EntityTypeConfiguration<AppError>
    {
        public AppErrorMap()
        {
            // Primary Key
            this.HasKey(t => t.AppErrorID);

            // Properties
            // Table & Column Mappings
            this.ToTable("AppError");
            this.Property(t => t.AppErrorID).HasColumnName("AppErrorID");
            this.Property(t => t.ErrorMsg).HasColumnName("ErrorMsg");
            this.Property(t => t.ErrorDT).HasColumnName("ErrorDT");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.Source).HasColumnName("Source");
        }
    }
}
