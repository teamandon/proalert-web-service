﻿using System.Data.Entity.ModelConfiguration;

namespace ProAlert.Andon.Service.Models.Mapping
{
    public class DdVmJsonMap : EntityTypeConfiguration<DdVmJson>
    {
        public DdVmJsonMap()
        {
            HasKey(t => t.TlId);

            ToTable("DdVmJson");
            Property(t => t.Json).HasColumnName("Json");
        }
    }

}
