using System.Data.Entity.ModelConfiguration;

namespace ProAlert.Andon.Service.Models.Mapping
{
    public class ProductWorkCenterStatMap : EntityTypeConfiguration<ProductWorkCenterStat>
    {
        public ProductWorkCenterStatMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.RowVersion)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("ProductWorkCenterStat");
            this.Property(t => t.Id).HasColumnName("ProductWorkCenterStatID");
            this.Property(t => t.WorkCenterID).HasColumnName("WorkCenterID");
            this.Property(t => t.ProductID).HasColumnName("ProductID");
            this.Property(t => t.CyclesPerHour).HasColumnName("CyclesPerHour").HasPrecision(18,5);
            this.Property(t => t.UnitsPerCycle).HasColumnName("UnitsPerCycle").HasPrecision(18, 5);
            this.Property(t => t.Yield).HasColumnName("Yield").HasPrecision(18, 5);
            this.Property(t => t.SetUpTime).HasColumnName("SetUpTime");
            this.Property(t => t.Available).HasColumnName("Available");
            this.Property(t => t.Performance).HasColumnName("Performance");
            this.Property(t => t.Quality).HasColumnName("Quality");
            this.Property(t => t.AvailableCap).HasColumnName("AvailableCap");
            this.Property(t => t.PerformanceCap).HasColumnName("PerformanceCap");
            this.Property(t => t.QualityCap).HasColumnName("QualityCap");
            this.Property(t => t.RowVersion).HasColumnName("RowVersion");

            // Relationships
            this.HasRequired(t => t.Product)
                .WithMany(t => t.ProductWorkCenterStats)
                .HasForeignKey(d => d.ProductID);
            this.HasRequired(t => t.WorkCenter)
                .WithMany(t => t.ProductWorkCenterStats)
                .HasForeignKey(d => d.WorkCenterID);

        }
    }
}
