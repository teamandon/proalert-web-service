﻿using System.Data.Entity.ModelConfiguration;

namespace ProAlert.Andon.Service.Models.Mapping
{
    public class WorkCenterGUIOptionsMap : EntityTypeConfiguration<WorkCenterGUIOptions>
    {
        public WorkCenterGUIOptionsMap()
        {
            HasKey(t => t.Id);

            ToTable("WorkCenterGUIOptions");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.ManualCycles).HasColumnName("ManualCycles");
            Property(t => t.SingleScrap).HasColumnName("SingleScrap");
        

        }
    }
}
