﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace ProAlert.Andon.Service.Models.Mapping
{
    public class WorkCenterPrintingOptionsMap : EntityTypeConfiguration<WorkCenterPrintingOptions>
    {
        public WorkCenterPrintingOptionsMap()
        {
            HasKey(t => t.Id);

            Property(t => t.ScrapPrinter).HasMaxLength(100);
            Property(t => t.ShippingPrinter).HasMaxLength(100);

            ToTable("WorkCenterPrintingOptions");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.ScrapLabel).HasColumnName("ScrapLabel");
            Property(t => t.ShippingLabel).HasColumnName("ShippingLabel");
        }
    }
}
