using System.Data.Entity.ModelConfiguration;

namespace ProAlert.Andon.Service.Models.Mapping
{
    public class CycleMap : EntityTypeConfiguration<Cycle>
    {
        public CycleMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("Cycle");
            this.Property(t => t.Id).HasColumnName("CycleID");
            this.Property(t => t.WorkCenterID).HasColumnName("WorkCenterID");
            this.Property(t => t.Count).HasColumnName("Count");
            this.Property(t => t.UpdateDT).HasColumnName("UpdateDT");

            // Relationships
            //this.HasRequired(t => t.WorkCenter)
            //    .WithMany(t => t.Cycles)
            //    .HasForeignKey(d => d.WorkCenterID);

        }
    }
}
