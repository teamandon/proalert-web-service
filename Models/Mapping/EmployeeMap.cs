using System.Data.Entity.ModelConfiguration;

namespace ProAlert.Andon.Service.Models.Mapping
{
    public class EmployeeMap : EntityTypeConfiguration<Employee>
    {
        public EmployeeMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.LogIn)
                .HasMaxLength(50);

            this.Property(t => t.TimeZone)
                .HasMaxLength(50);

            this.Property(t => t.RowVersion)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            this.Property(t => t.FirstName)
                .HasMaxLength(50);

            this.Property(t => t.LastName)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Employee");
            this.Property(t => t.Id).HasColumnName("EmployeeID");
            this.Property(t => t.PositionID).HasColumnName("PositionID");
            this.Property(t => t.HireDate).HasColumnName("HireDate");
            this.Property(t => t.TerminationDate).HasColumnName("TerminationDate");
            this.Property(t => t.LogIn).HasColumnName("LogIn");
            this.Property(t => t.Pin).HasColumnName("Pin");
            this.Property(t => t.TimeZone).HasColumnName("TimeZone");
            this.Property(t => t.RowVersion).HasColumnName("RowVersion");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.Cell).HasColumnName("Cell");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Picture).HasColumnName("Picture");

            // Relationships
            //this.HasOptional(t => t.Position)
            //    .WithMany(t => t.Employees)
            //    .HasForeignKey(d => d.PositionID);

        }
    }
}
