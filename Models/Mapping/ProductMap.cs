using System.Data.Entity.ModelConfiguration;

namespace ProAlert.Andon.Service.Models.Mapping
{
    public class ProductMap : EntityTypeConfiguration<Product>
    {
        public ProductMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Name)
                .HasMaxLength(50);

            this.Property(t => t.Description)
                .HasMaxLength(256);

            this.Property(t => t.ImagePath)
                .HasMaxLength(256);

            this.Property(t => t.Material)
                .HasMaxLength(256);

            this.Property(t => t.BlanketPo)
                .HasMaxLength(50);

            this.Property(t => t.RowVersion)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("Product");
            this.Property(t => t.Id).HasColumnName("ProductID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.ImagePath).HasColumnName("ImagePath");
            this.Property(t => t.Material).HasColumnName("Material");
            this.Property(t => t.StandardPack).HasColumnName("StandardPack");
            this.Property(t => t.BlanketPo).HasColumnName("BlanketPo");
            this.Property(t => t.Image).HasColumnName("Image");
            this.Property(t => t.RowVersion).HasColumnName("RowVersion");
        }
    }
}
