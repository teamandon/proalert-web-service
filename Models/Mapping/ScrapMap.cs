using System.Data.Entity.ModelConfiguration;

namespace ProAlert.Andon.Service.Models.Mapping
{
    public class ScrapMap : EntityTypeConfiguration<Scrap>
    {
        public ScrapMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Notes)
                .HasMaxLength(500);

            this.Property(t => t.DefectLocation)
                .HasMaxLength(256);

            // Table & Column Mappings
            this.ToTable("Scrap");
            this.Property(t => t.Id).HasColumnName("ScrapID");
            this.Property(t => t.WorkCenterID).HasColumnName("WorkCenterID");
            this.Property(t => t.ProductID).HasColumnName("ProductID");
            this.Property(t => t.LastStageID).HasColumnName("LastStageID");
            this.Property(t => t.RejectCodeID).HasColumnName("RejectCodeID");
            this.Property(t => t.ScrapTypeID).HasColumnName("ScrapTypeID");
            this.Property(t => t.ScrapDT).HasColumnName("ScrapDT");
            this.Property(t => t.ScrapCount).HasColumnName("ScrapCount");
            this.Property(t => t.Notes).HasColumnName("Notes");
            this.Property(t => t.DefectLocation).HasColumnName("DefectLocation");
            this.Property(t => t.SignoffDT).HasColumnName("SignoffDT");
            this.Property(t => t.EmployeeID).HasColumnName("EmployeeID");
            this.Property(t => t.TimesegmentID).HasColumnName("TimesegmentID");
            this.Property(t => t.ScrapParentID).HasColumnName("ScrapParentID");
            this.Property(t => t.Allocated).HasColumnName("Allocated");

            // Relationships
            HasOptional(a => a.ScrapParent).WithMany().HasForeignKey(k => k.ScrapParentID).WillCascadeOnDelete(false);
            //this.HasOptional(t => t.Employee)
            //    .WithMany(t => t.Scraps)
            //    .HasForeignKey(d => d.EmployeeID);
            //this.HasOptional(t => t.LastStage)
            //    .WithMany(t => t.Scraps)
            //    .HasForeignKey(d => d.LastStageID);
            //this.HasOptional(t => t.Product)
            //    .WithMany(t => t.Scraps)
            //    .HasForeignKey(d => d.ProductID);
            //this.HasOptional(t => t.RejectCode)
            //    .WithMany(t => t.Scraps)
            //    .HasForeignKey(d => d.RejectCodeID);
            //this.HasOptional(t => t.Scrap2)
            //    .WithMany(t => t.Scrap1)
            //    .HasForeignKey(d => d.ScrapParentID);
            //this.HasOptional(t => t.ScrapType)
            //    .WithMany(t => t.Scraps)
            //    .HasForeignKey(d => d.ScrapTypeID);
            //this.HasOptional(t => t.Timesegment)
            //    .WithMany(t => t.Scraps)
            //    .HasForeignKey(d => d.TimesegmentID);
            //this.HasOptional(t => t.WorkCenter)
            //    .WithMany(t => t.Scraps)
            //    .HasForeignKey(d => d.WorkCenterID);

        }
    }
}
