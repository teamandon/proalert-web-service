using System.Data.Entity.ModelConfiguration;

namespace ProAlert.Andon.Service.Models.Mapping
{
    public class IssueMap : EntityTypeConfiguration<Issue>
    {
        public IssueMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Name)
                .HasMaxLength(50);

            this.Property(t => t.Description)
                .HasMaxLength(256);

            this.Property(t => t.RowVersion)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("Issue");
            this.Property(t => t.Id).HasColumnName("IssueID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Material).HasColumnName("Material");
            this.Property(t => t.Manufacturing).HasColumnName("Manufacturing");
            this.Property(t => t.Rework).HasColumnName("Rework");
            this.Property(t => t.DT).HasColumnName("DT");
            this.Property(t => t.SubCategoryID).HasColumnName("SubCategoryID");
            this.Property(t => t.DispOrder).HasColumnName("DispOrder");
            this.Property(t => t.RowVersion).HasColumnName("RowVersion");

            // Relationships
            //this.HasOptional(t => t.SubCategory)
            //    .WithMany(t => t.Issues)
            //    .HasForeignKey(d => d.SubCategoryID);

        }
    }
}
