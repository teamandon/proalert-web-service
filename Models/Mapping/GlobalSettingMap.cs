﻿using System.Data.Entity.ModelConfiguration;

namespace ProAlert.Andon.Service.Models.Mapping
{
    public class GlobalSettingMap : EntityTypeConfiguration<GlobalSetting>
    {
        public GlobalSettingMap()
        {
            this.HasKey(t => t.Id);

            ToTable("GlobalSetting");
            Property(t => t.Id).HasColumnName("GlobalSettingID");
            Property(t => t.OeeSummaryStartDt).HasColumnName("OeeSumStartDt");
            Property(t => t.OeeSumRollEight).HasColumnName("RollEight");
        }
    }
}
