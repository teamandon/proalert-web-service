using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Vistech.Andon.Model.Models.Mapping
{
    public class PlannedDtCompressedMap : EntityTypeConfiguration<PlannedDtCompressed>
    {
        public PlannedDtCompressedMap()
        {
            // Primary Key
            this.HasKey(t => t.PlannedDtCompressedID);

            // Properties
            // Table & Column Mappings
            this.ToTable("PlannedDtCompressed");
            this.Property(t => t.PlannedDtCompressedID).HasColumnName("PlannedDtCompressedID");
            this.Property(t => t.WcProductTimelineID).HasColumnName("WcProductTimelineID");
            this.Property(t => t.Start).HasColumnName("Start");
            this.Property(t => t.Stop).HasColumnName("Stop");
            this.Property(t => t.Milliseconds).HasColumnName("Milliseconds");

            // Relationships
            //this.HasRequired(t => t.WCProductTimeline)
            //    .WithMany(t => t.PlannedDtCompresseds)
            //    .HasForeignKey(d => d.WcProductTimelineID);

        }
    }
}
