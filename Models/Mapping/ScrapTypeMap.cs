using System.Data.Entity.ModelConfiguration;

namespace ProAlert.Andon.Service.Models.Mapping
{
    public class ScrapTypeMap : EntityTypeConfiguration<ScrapType>
    {
        public ScrapTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Name)
                .HasMaxLength(50);

            this.Property(t => t.Description)
                .HasMaxLength(256);

            this.Property(t => t.RowVersion)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("ScrapType");
            this.Property(t => t.Id).HasColumnName("ScrapTypeID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Default).HasColumnName("Default");
            this.Property(t => t.DispOrder).HasColumnName("DispOrder");
            this.Property(t => t.RowVersion).HasColumnName("RowVersion");
        }
    }
}
