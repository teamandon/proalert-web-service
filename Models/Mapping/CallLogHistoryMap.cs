using System.Data.Entity.ModelConfiguration;

namespace ProAlert.Andon.Service.Models.Mapping
{
    public class CallLogHistoryMap : EntityTypeConfiguration<CallLogHistory>
    {
        public CallLogHistoryMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("CallLogHistory");
            this.Property(t => t.Id).HasColumnName("CallLogHistoryID");
            this.Property(t => t.CallLogID).HasColumnName("CallLogID");
            this.Property(t => t.CallID).HasColumnName("CallID");
            this.Property(t => t.InitiateDt).HasColumnName("InitiateDt");
            this.Property(t => t.ResponseDt).HasColumnName("ResponseDt");
            this.Property(t => t.ResolveDt).HasColumnName("ResolveDt");
            this.Property(t => t.WorkCenterId).HasColumnName("WorkCenterId");
            this.Property(t => t.EmployeeID).HasColumnName("EmployeeID");
            this.Property(t => t.Planned).HasColumnName("Planned");
            this.Property(t => t.IssueID).HasColumnName("IssueID");
            this.Property(t => t.SubCategoryID).HasColumnName("SubCategoryID");
            this.Property(t => t.Resolution).HasColumnName("Resolution");
            this.Property(t => t.OperatorNotes).HasColumnName("OperatorNotes");
            this.Property(t => t.ProductID).HasColumnName("ProductID");
            this.Property(t => t.SignOffDT).HasColumnName("SignOffDT");
        }
    }
}
