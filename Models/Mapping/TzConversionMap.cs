using System.Data.Entity.ModelConfiguration;

namespace ProAlert.Andon.Service.Models.Mapping
{
    public class TzConversionMap : EntityTypeConfiguration<TzConversion>
    {
        public TzConversionMap()
        {
            // Primary Key
            this.HasKey(t => t.Client);

            // Properties
            this.Property(t => t.Client)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.System)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("TzConversion");
            this.Property(t => t.Client).HasColumnName("Client");
            this.Property(t => t.System).HasColumnName("System");
        }
    }
}
