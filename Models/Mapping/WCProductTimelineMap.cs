using System.Data.Entity.ModelConfiguration;

namespace ProAlert.Andon.Service.Models.Mapping
{
    public class WCProductTimelineMap : EntityTypeConfiguration<WCProductTimeline>
    {
        public WCProductTimelineMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            Property(x => x.PlannedProduction).HasPrecision(18, 5);
            Property(x => x.OperationTime).HasPrecision(18, 5);
            Property(x => x.Availability).HasPrecision(18, 5);
            Property(x => x.Performance).HasPrecision(18, 5);
            Property(x => x.Quality).HasPrecision(18, 5);
            Property(x => x.OEE).HasPrecision(18, 5);
            Property(x => x.TotalDT).HasPrecision(18, 5);
            Property(x => x.TotalPlannedDT).HasPrecision(18, 5);
            Property(x => x.ActualPieces).HasPrecision(18, 5);


            // Table & Column Mappings
            this.ToTable("WCProductTimeline");
            this.Property(t => t.Id).HasColumnName("WCProductTimelineID");
            this.Property(t => t.WorkCenterID).HasColumnName("WorkCenterID");
            this.Property(t => t.ProductID).HasColumnName("ProductID");
            this.Property(t => t.Start).HasColumnName("Start");
            this.Property(t => t.Stop).HasColumnName("Stop");
            this.Property(t => t.TotalDT).HasColumnName("TotalDT");
            this.Property(t => t.TotalPlannedDT).HasColumnName("TotalPlannedDT");
            this.Property(t => t.PlannedProduction).HasColumnName("PlannedProduction");
            this.Property(t => t.OperationTime).HasColumnName("OperationTime");
            this.Property(t => t.ProductWcStatsHistoryID).HasColumnName("ProductWcStatsHistoryID");
            this.Property(t => t.Current).HasColumnName("Current");
            this.Property(t => t.Availability).HasColumnName("Availability");
            this.Property(t => t.Performance).HasColumnName("Performance");
            this.Property(t => t.Quality).HasColumnName("Quality");
            this.Property(t => t.OEE).HasColumnName("OEE");
            this.Property(t => t.LastDTReason).HasColumnName("LastDTReason");
            this.Property(t => t.WCProduct).HasColumnName("WCProduct");
            this.Property(t => t.UnplannedDt).HasColumnName("Down");
            this.Property(t => t.PlannedDt).HasColumnName("WCPlanned");
            this.Property(t => t.Mfg).HasColumnName("Mfg");
            this.Property(t => t.Matl).HasColumnName("Matl");
            this.Property(t => t.Rework).HasColumnName("Rework");
            this.Property(t => t.Other).HasColumnName("Other");
            this.Property(t => t.ActualPieces).HasColumnName("ActualPieces");
            this.Property(t => t.ActualCycles).HasColumnName("ActualCycles");

            // Relationships
            //this.HasOptional(t => t.Product)
            //    .WithMany(t => t.WCProductTimelines)
            //    .HasForeignKey(d => d.ProductID);
            //this.HasRequired(t => t.ProductWCStatsHistory)
            //    .WithMany(t => t.WCProductTimelines)
            //    .HasForeignKey(d => d.ProductWcStatsHistoryID);
            //this.HasOptional(t => t.WorkCenter)
            //    .WithMany(t => t.WCProductTimelines)
            //    .HasForeignKey(d => d.WorkCenterID);

        }
    }
}
