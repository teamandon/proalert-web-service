﻿using System;
using System.Data.Entity.ModelConfiguration;
namespace ProAlert.Andon.Service.Models.Mapping
{
    public class ContainerMap : EntityTypeConfiguration<Container>
    {
        public ContainerMap()
        {
            HasKey(t => t.Id);

            Property(t => t.Location).HasMaxLength(50);
            Property(t => t.PO).HasMaxLength(50);

            ToTable("Container");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.WorkCenterId).HasColumnName("WorkCenterId");
            Property(t => t.ProductId).HasColumnName("ProductId");
            Property(t => t.Quantity).HasColumnName("Quantity");
            Property(t => t.SerialNumber).HasColumnName("SerialNumber");
            Property(t => t.PrintDate).HasColumnName("PrintDate");
            Property(t => t.PO).HasColumnName("PO");
            Property(t => t.Location).HasColumnName("Location");
        }
    }
}
