using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProAlert.Andon.Service.Models.Mapping
{
    public class BOMMap : EntityTypeConfiguration<BOM>
    {
        public BOMMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Id, t.ComponentID });

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ComponentID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.RowVersion)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("BOM");
            this.Property(t => t.Id).HasColumnName("ProductID");
            this.Property(t => t.ComponentID).HasColumnName("ComponentID");
            this.Property(t => t.Quantity).HasColumnName("Quantity");
            this.Property(t => t.RowVersion).HasColumnName("RowVersion");

            // Relationships
            //this.HasRequired(t => t.Product)
            //    .WithMany(t => t.BOMs)
            //    .HasForeignKey(d => d.ComponentID);
            //this.HasRequired(t => t.Product1)
            //    .WithMany(t => t.BOMs1)
            //    .HasForeignKey(d => d.ProductID);

        }
    }
}
