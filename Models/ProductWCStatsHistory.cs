
using System.ComponentModel.DataAnnotations;
using ProAlert.Andon.Service.Interfaces;

namespace ProAlert.Andon.Service.Models
{
    public partial class ProductWCStatsHistory : Entity<int>
    {
        public ProductWCStatsHistory()
        {
            
        }
        /// <summary>
        /// Create new history record by original.
        /// </summary>
        /// <param name="pwcs"></param>
        public ProductWCStatsHistory(ProductWorkCenterStat pwcs)
        {
            ProductWorkCenterStatID = pwcs.Id;
            WorkCenterID = pwcs.WorkCenterID;
            ProductID = pwcs.ProductID;
            CyclesPerHour = pwcs.CyclesPerHour;
            UnitsPerCycle = pwcs.UnitsPerCycle;
            Yield = pwcs.Yield;
            SetUpTime = pwcs.SetUpTime;
            Available = pwcs.Available;
            Performance = pwcs.Performance;
            Quality = pwcs.Quality;
            AvailableCap = pwcs.AvailableCap;
            PerformanceCap = pwcs.PerformanceCap;
            QualityCap = pwcs.QualityCap;

        }
        //public ProductWCStatsHistory()
        //{
        //    this.WCProductTimelines = new List<WCProductTimeline>();
        //    this.WCProductTimelineHistories = new List<WCProductTimelineHistory>();
        //}

        //public int ProductWCStatsHistoryID { get; set; }
        //public int ProductWorkCenterStatID { get; set; }
        //public int WorkCenterID { get; set; }
        //public int ProductID { get; set; }
        //public decimal CyclesPerHour { get; set; }
        //public int UnitsPerCycle { get; set; }
        //public decimal Yield { get; set; }
        //public decimal SetUpTime { get; set; }
        //public decimal Available { get; set; }
        //public decimal Performance { get; set; }
        //public decimal Quality { get; set; }
        //public decimal AvailableCap { get; set; }
        //public decimal PerformanceCap { get; set; }
        //public decimal QualityCap { get; set; }
        //public byte[] RowVersion { get; set; }
        //public virtual ICollection<WCProductTimeline> WCProductTimelines { get; set; }
        //public virtual ICollection<WCProductTimelineHistory> WCProductTimelineHistories { get; set; }

        public int ProductWorkCenterStatID { get; set; }

        [Display(Name = "Work Center")]
        public int WorkCenterID { get; set; }

        [Display(Name = "Product")]
        public int ProductID { get; set; }

        [Display(Name = "Cycles Per Hour")]
        [DisplayFormat(DataFormatString = "{0:N5}", ApplyFormatInEditMode = true)]
        public decimal CyclesPerHour { get; set; }

        [Display(Name = "Units Per Cycle")]
        [DisplayFormat(DataFormatString = "{0:N5}", ApplyFormatInEditMode = true)]
        public decimal UnitsPerCycle { get; set; }

        [Display(Name = "Yield")]
        [DisplayFormat(DataFormatString = "{0:N5}", ApplyFormatInEditMode = true)]
        public decimal Yield { get; set; }

        [Display(Name = "Setup Time (Minutes)")]
        [DisplayFormat(DataFormatString = "{0:N5}", ApplyFormatInEditMode = true)]
        public decimal SetUpTime { get; set; }

        [Display(Name = "Availability Target (.85)")]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal Available { get; set; }

        [Display(Name = "Performance Target")]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal Performance { get; set; }

        [Display(Name = "Quality Target")]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal Quality { get; set; }

        [Display(Name = "OEE Target")]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal OEE => Available * Performance * Quality;

        [Display(Name = "Availability Cap (1.5)")]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal AvailableCap { get; set; }

        [Display(Name = "Performance Cap")]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal PerformanceCap { get; set; }

        [Display(Name = "Quality Cap")]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal QualityCap { get; set; }

    }
}
