using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using ProAlert.Andon.Service.Models.Mapping;

namespace ProAlert.Andon.Service.Models
{
    public interface IContext
    {
        IDbSet<AppError> AppErrors { get; set; }
        IDbSet<Mod2SqlStatus> Mod2SqlStatuses { get; set; }
        IDbSet<Mod2SqlCount> Mod2SqlCounts { get; set; }
        IDbSet<AudioFile> AudioFiles { get; set; }
        IDbSet<AutoMessage> AutoMessages { get; set; }
        IDbSet<BOM> Boms { get; set; }
        IDbSet<Call> Calls { get; set; }
        IDbSet<CallLog> CallLogs { get; set; }
        IDbSet<CallLogHistory> CallLogHistories { get; set; }
        IDbSet<Cycle> Cycles { get; set; }
        IDbSet<CycleHistory> CycleHistories { get; set; }
        IDbSet<CycleSummary> CycleSummaries { get; set; }
        IDbSet<Email> Emails { get; set; }
        IDbSet<Employee> Employees { get; set; }
        IDbSet<Issue> Issues { get; set; }
        IDbSet<LastStage> LastStages { get; set; }
        IDbSet<PlannedDt> PlannedDts { get; set; }
        IDbSet<PlannedDtLog> PlannedDtLogs { get; set; }
        IDbSet<Position> Positions { get; set; }
        IDbSet<Product> Products { get; set; }
        IDbSet<WCProductTimeline> WcProductTimelines { get; set; }
        IDbSet<WCProductTimelineHistory> WcProductTimelineHistories { get; set; }
        IDbSet<ProductWorkCenterStat> ProductWorkCenterStats { get; set; }
        IDbSet<ProductWCStatsHistory> ProductWcStatsHistories { get; set; }
        IDbSet<RejectCode> RejectCodes { get; set; }
        IDbSet<Scrap> Scraps { get; set; }
        IDbSet<ScrapType> ScrapTypes { get; set; }
        IDbSet<ShiftLog> ShiftLogs { get; set; }
        IDbSet<ShiftStartTimes> ShiftStartTimes { get; set; }
        IDbSet<SubCategory> SubCategories { get; set; }
        IDbSet<Timesegment> Timesegments { get; set; }
        IDbSet<TzConversion> TzConversions { get; set; }
        IDbSet<WorkCenter> WorkCenters { get; set; }
        IDbSet<WorkCenterCall> WorkCenterCalls { get; set; }
        IDbSet<WorkCenterGUIOptions> WorkCenterGUIOptions { get; set; }
        IDbSet<WorkCenterPrintingOptions> WorkCenterPrintingOptions { get; set; }
        IDbSet<GlobalSetting> GlobalSettings { get; set; }
        IDbSet<DdVmJson> DdVmJsons { get; set; }
        IDbSet<Container> Containers { get; set; }


        Database db { get; set; }
        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;
        //DbSqlQuery<TEntity> SqlQuery<TEntity>() where TEntity : class;
        int SaveChanges();
        //Task SaveChangesAysnc();

    }
    public partial class ProAlertContext : DbContext, IContext
    { 
        static ProAlertContext()
        {
            Database.SetInitializer<ProAlertContext>(null);
        }

        public ProAlertContext()
            : base("Name=ProAlertContext")
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }
        public Database db
        {
            get { return Database; }
            set { throw new NotImplementedException(); }
        }

        public IDbSet<AppError> AppErrors { get; set; }
        public IDbSet<Mod2SqlStatus> Mod2SqlStatuses { get; set; }
        public IDbSet<Mod2SqlCount> Mod2SqlCounts { get; set; }
        public IDbSet<AudioFile> AudioFiles { get; set; }
        public IDbSet<AutoMessage> AutoMessages { get; set; }
        public IDbSet<BOM> Boms { get; set; }
        public IDbSet<Call> Calls { get; set; }
        public IDbSet<CallLog> CallLogs { get; set; }
        public IDbSet<CallLogHistory> CallLogHistories { get; set; }
        public IDbSet<Cycle> Cycles { get; set; }
        public IDbSet<CycleHistory> CycleHistories { get; set; }
        public IDbSet<CycleSummary> CycleSummaries { get; set; }
        //public DbSet<DtCompressed> DtCompresseds { get; set; }
        public IDbSet<Email> Emails { get; set; }
        public IDbSet<Employee> Employees { get; set; }
        public IDbSet<Issue> Issues { get; set; }
        public IDbSet<LastStage> LastStages { get; set; }
        public IDbSet<PlannedDt> PlannedDts { get; set; }
        //public DbSet<PlannedDtCompressed> PlannedDtCompresseds { get; set; }
        public IDbSet<PlannedDtLog> PlannedDtLogs { get; set; }
        public IDbSet<Position> Positions { get; set; }
        public IDbSet<Product> Products { get; set; }
        public IDbSet<ProductWCStatsHistory> ProductWcStatsHistories { get; set; }
        public IDbSet<ProductWorkCenterStat> ProductWorkCenterStats { get; set; }
        public IDbSet<RejectCode> RejectCodes { get; set; }
        public IDbSet<Scrap> Scraps { get; set; }
        public IDbSet<ScrapType> ScrapTypes { get; set; }
        public IDbSet<ShiftLog> ShiftLogs { get; set; }
        public IDbSet<ShiftStartTimes> ShiftStartTimes { get; set; }
        public IDbSet<SubCategory> SubCategories { get; set; }
        public IDbSet<Timesegment> Timesegments { get; set; }
        public IDbSet<TzConversion> TzConversions { get; set; }
        public IDbSet<WCProductTimeline> WcProductTimelines { get; set; }
        public IDbSet<WCProductTimelineHistory> WcProductTimelineHistories { get; set; }
        public IDbSet<WorkCenter> WorkCenters { get; set; }
        public IDbSet<WorkCenterCall> WorkCenterCalls { get; set; }
        public IDbSet<WorkCenterGUIOptions> WorkCenterGUIOptions { get; set; }
        public IDbSet<WorkCenterPrintingOptions> WorkCenterPrintingOptions { get; set; }
        public IDbSet<GlobalSetting> GlobalSettings { get; set; }
        public IDbSet<DdVmJson> DdVmJsons { get; set; }
        public IDbSet<Container> Containers { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AppErrorMap());
            modelBuilder.Configurations.Add(new Mod2SqlStatusMap());
            modelBuilder.Configurations.Add(new Mod2SqlCountMap());
            modelBuilder.Configurations.Add(new AudioFileMap());
            modelBuilder.Configurations.Add(new AutoMessageMap());
            modelBuilder.Configurations.Add(new BOMMap());
            modelBuilder.Configurations.Add(new CallMap());
            modelBuilder.Configurations.Add(new CallLogMap());
            modelBuilder.Configurations.Add(new CallLogHistoryMap());
            modelBuilder.Configurations.Add(new CycleMap());
            modelBuilder.Configurations.Add(new CycleHistoryMap());
            modelBuilder.Configurations.Add(new CycleSummaryMap());
            //modelBuilder.Configurations.Add(new DtCompressedMap());
            modelBuilder.Configurations.Add(new EmailMap());
            modelBuilder.Configurations.Add(new EmployeeMap());
            modelBuilder.Configurations.Add(new IssueMap());
            modelBuilder.Configurations.Add(new LastStageMap());
            modelBuilder.Configurations.Add(new PlannedDtMap());
            //modelBuilder.Configurations.Add(new PlannedDtCompressedMap());
            modelBuilder.Configurations.Add(new PlannedDtLogMap());
            modelBuilder.Configurations.Add(new PositionMap());
            modelBuilder.Configurations.Add(new ProductMap());
            modelBuilder.Configurations.Add(new ProductWCStatsHistoryMap());
            modelBuilder.Configurations.Add(new ProductWorkCenterStatMap());
            modelBuilder.Configurations.Add(new RejectCodeMap());
            modelBuilder.Configurations.Add(new ScrapMap());
            modelBuilder.Configurations.Add(new ScrapTypeMap());
            modelBuilder.Configurations.Add(new SubCategoryMap());
            modelBuilder.Configurations.Add(new TimesegmentMap());
            modelBuilder.Configurations.Add(new TzConversionMap());
            modelBuilder.Configurations.Add(new WCProductTimelineMap());
            modelBuilder.Configurations.Add(new WCProductTimelineHistoryMap());
            modelBuilder.Configurations.Add(new WorkCenterMap());
            modelBuilder.Configurations.Add(new WorkCenterCallMap());
            modelBuilder.Configurations.Add(new WorkCenterGUIOptionsMap());
            modelBuilder.Configurations.Add(new WorkCenterPrintingOptionsMap());
            modelBuilder.Configurations.Add(new GlobalSettingMap());
            modelBuilder.Configurations.Add(new DdVmJsonMap());
            modelBuilder.Configurations.Add(new ContainerMap());
        }

        public override int SaveChanges()
        {
            //   var modifiedEntries = ChangeTracker.Entries()
            //.Where(x => x.Entity is IAuditableEntity
            //    && (x.State == System.Data.Entity.EntityState.Added || x.State == System.Data.Entity.EntityState.Modified));

            //   foreach (var entry in modifiedEntries)
            //   {
            //       IAuditableEntity entity = entry.Entity as IAuditableEntity;
            //       if (entity != null)
            //       {
            //           string identityName = Thread.CurrentPrincipal.Identity.Name;
            //           DateTime now = DateTime.UtcNow;

            //           if (entry.State == System.Data.Entity.EntityState.Added)
            //           {
            //               entity.CreatedBy = identityName;
            //               entity.CreatedDate = now;
            //           }
            //           else
            //           {
            //               base.Entry(entity).Property(x => x.CreatedBy).IsModified = false;
            //               base.Entry(entity).Property(x => x.CreatedDate).IsModified = false;
            //           }

            //           entity.UpdatedBy = identityName;
            //           entity.UpdatedDate = now;
            //       }
            //   }

            return base.SaveChanges();
        }

    }
}
