﻿using System;
using ProAlert.Andon.Service.Common;
using ProAlert.Andon.Service.Interfaces;

namespace ProAlert.Andon.Service.Models
{
    public class ShiftLog : Entity<int>
    {
        public Enumerations.Shift Shift { get; set; }
        public DateTime Start { get; set; }
        public DateTime Stop { get; set; }
        public int WorkCenterId { get; set; }

    }
}
