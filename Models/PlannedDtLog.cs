using System;
using System.ComponentModel.DataAnnotations;
using ProAlert.Andon.Service.Common;
using ProAlert.Andon.Service.Interfaces;

namespace ProAlert.Andon.Service.Models
{
    public partial class PlannedDtLog : Entity<int>
    {

        [Display(Name = "Planned Downtime Event")]
        public int PlannedDtID { get; set; }

        [Display(Name = "Start Time"), DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd hh:mm:ss tt}", ApplyFormatInEditMode = true)]
        public DateTime Start { get; set; }

        [Display(Name = "Stop Time"), DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd hh:mm:ss tt}", ApplyFormatInEditMode = true)]
        public DateTime? Stop { get; set; }

        /// <summary>
        /// Display utc start in local time
        /// </summary>
        /// <param name="tzi"></param>
        /// <returns></returns>
        public DateTime DispStart(TimeZoneInfo tzi)
        {
            return timeconversions.UTCtoClient(tzi, Start);
        }

        public DateTime? DispStop(TimeZoneInfo tzi)
        {
            if (Stop == null) return null;
            return timeconversions.UTCtoClient(tzi, Stop ?? DateTime.Now);
        }

        [Display(Name = "Notes"), StringLength(256)]
        public string Notes { get; set; }

        [ScaffoldColumn(false)]
        public int? EmployeeID { get; set; }

        [ScaffoldColumn(false)]
        public int WorkCenterID { get; set; }

        public int? ProductID { get; set; }

        public int? TimesegmentID { get; set; }

        // Navigations
        public virtual PlannedDt PlannedDt { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual WorkCenter WorkCenter { get; set; }
        public virtual Product Product { get; set; }
        public virtual Timesegment Timesegment { get; set; }

        //public int PlannedDtLogID { get; set; }
        //public int PlannedDtID { get; set; }
        //public System.DateTime Start { get; set; }
        //public Nullable<System.DateTime> Stop { get; set; }
        //public string Notes { get; set; }
        //public Nullable<int> EmployeeID { get; set; }
        //public int WorkCenterID { get; set; }
        //public Nullable<int> ProductID { get; set; }
        //public Nullable<int> TimesegmentID { get; set; }
        //public byte[] RowVersion { get; set; }
        //public virtual Employee Employee { get; set; }
        //public virtual PlannedDt PlannedDt { get; set; }
        //public virtual Product Product { get; set; }
        //public virtual Timesegment Timesegment { get; set; }
        //public virtual WorkCenter WorkCenter { get; set; }
    }
}
