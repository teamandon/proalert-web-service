using System.ComponentModel.DataAnnotations;
using ProAlert.Andon.Service.Interfaces;

namespace ProAlert.Andon.Service.Models
{
    public partial class SubCategory : Entity<int>
    {
        [Display(Name = "Detail"), StringLength(50), Required]
        public string Name { get; set; }

        [Display(Name = "Description"), StringLength(256)]
        public string Description { get; set; }

        [ScaffoldColumn(false)]
        public int? DispOrder { get; set; }

        //public SubCategory()
        //{
        //    this.CallLogs = new List<CallLog>();
        //    this.Issues = new List<Issue>();
        //}

        //public int SubCategoryID { get; set; }
        //public string Name { get; set; }
        //public string Description { get; set; }
        //public Nullable<int> DispOrder { get; set; }
        //public byte[] RowVersion { get; set; }
        //public virtual ICollection<CallLog> CallLogs { get; set; }
        //public virtual ICollection<Issue> Issues { get; set; }
    }
}
