using System;
using ProAlert.Andon.Service.Interfaces;

namespace ProAlert.Andon.Service.Models
{
    public partial class Cycle : Entity<int>
    {
        public int WorkCenterID { get; set; }

        public int Count { get; set; }

        public DateTime UpdateDT { get; set; }

        // Navigations
        public virtual WorkCenter WorkCenter { get; set; }
        //public int CycleID { get; set; }
        //public int WorkCenterID { get; set; }
        //public int Count { get; set; }
        //public System.DateTime UpdateDT { get; set; }
        //public virtual WorkCenter WorkCenter { get; set; }
    }
}
