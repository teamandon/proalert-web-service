using System;
using System.ComponentModel.DataAnnotations;
using ProAlert.Andon.Service.Common;

namespace ProAlert.Andon.Service.Models
{
    public partial class Employee : Person
    {


        [Display(Name = "Position")]
        public int? PositionID { get; set; }

        //[DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Hire Date")]
        public DateTime HireDate { get; set; }

        public DateTime DispHireDate(TimeZoneInfo tzi)
        {
            return timeconversions.UTCtoClient(tzi, HireDate);
        }

        //[DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Termination Date")]
        public DateTime? TerminationDate { get; set; }

        [Display(Name = "Log-in ID"), StringLength(50)]
        public string LogIn { get; set; }

        [Display(Name = "PIN")]
        public int Pin { get; set; }

        public TimeZoneInfo GetTimeZoneInstance()
        {
            if (_timeZoneInstance == null)
            {
                try
                {
                    _timeZoneInstance = TimeZoneInfo.FindSystemTimeZoneById(TimeZone);
                }
                catch
                {
                    TimeZone = "Eastern Standard Time";
                    _timeZoneInstance = TimeZoneInfo.FindSystemTimeZoneById(TimeZone);
                }
            }
            return _timeZoneInstance;
        }

        public void SetTimeZoneIntance(TimeZoneInfo value)
        {
            _timeZoneInstance = value;

        }

        // timezone conversions
        private TimeZoneInfo _timeZoneInstance;
        /// <summary>
        /// The users TimeZone using .NET TimeZoneNames
        /// </summary>
        [Display(Name = "Time Zone"), StringLength(50)]
        public string TimeZone
        {
            get { return _timeZone; }
            set
            {
                SetTimeZoneIntance(null);
                _timeZone = value;
            }
        }
        private string _timeZone;


        /// <summary>
        /// converts UTC to Local
        /// </summary>
        /// <param name="tz"></param>
        /// <param name="utcTime"></param>
        /// <returns></returns>
        public DateTime GetUserTime(DateTime? utcTime = null)
        {
            //TimeZoneInfo tzi = null;

            if (utcTime == null)
                utcTime = DateTime.UtcNow;

            return TimeZoneInfo.ConvertTimeFromUtc(utcTime.Value, GetTimeZoneInstance());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tz"></param>
        /// <param name="localServerTime"></param>
        /// <returns></returns>
        public DateTime GetUtcUserTime(System.TimeZoneInfo tz, DateTime? localServerTime)
        {
            if (localServerTime == null)
                localServerTime = DateTime.Now;

            return TimeZoneInfo.ConvertTime(localServerTime.Value, tz).ToUniversalTime();
        }

        // Navigation
        public virtual Position Position { get; set; }
        //public virtual Business Business { get;
        //public Employee()
        //{
        //    this.AutoMessages = new List<AutoMessage>();
        //    this.CallLogs = new List<CallLog>();
        //    this.PlannedDtLogs = new List<PlannedDtLog>();
        //    this.Scraps = new List<Scrap>();
        //}

        //public int EmployeeID { get; set; }
        //public Nullable<int> PositionID { get; set; }
        //public System.DateTime HireDate { get; set; }
        //public Nullable<System.DateTime> TerminationDate { get; set; }
        //public string LogIn { get; set; }
        //public int Pin { get; set; }
        //public string TimeZone { get; set; }
        //public byte[] RowVersion { get; set; }
        //public string FirstName { get; set; }
        //public string LastName { get; set; }
        //public string Cell { get; set; }
        //public string Email { get; set; }
        //public byte[] Picture { get; set; }
        //public virtual ICollection<AutoMessage> AutoMessages { get; set; }
        //public virtual ICollection<CallLog> CallLogs { get; set; }
        //public virtual Position Position { get; set; }
        //public virtual ICollection<PlannedDtLog> PlannedDtLogs { get; set; }
        //public virtual ICollection<Scrap> Scraps { get; set; }
    }
}
