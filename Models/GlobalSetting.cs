﻿using System;
using System.ComponentModel.DataAnnotations;
using ProAlert.Andon.Service.Interfaces;

namespace ProAlert.Andon.Service.Models
{
    public partial class GlobalSetting : Entity<int>
    {
        [Display(Name="OEE Summary Start Date")]
        public DateTime OeeSummaryStartDt { get; set; }
        [Display(Name="OEE Summary: Rolling 8hrs")]
        public bool OeeSumRollEight { get; set; }
    }
}
