﻿using System;

namespace ProAlert.Andon.Service.Models
{
    public class DtRpt
    {
        public DateTime InitiateDt { get; set; }
        public DateTime Responsedt { get; set; }
        public DateTime? Resolvedt { get; set; }
        public string WC { get; set; }
        public string Maint { get; set; }
        // cannot have a planned call by nature
        //public bool Planned { get; set; }
        public string Issue { get; set; }
        public string Subcategory { get; set; }
        public string Resolution { get; set; }
        public string OperatorNotes { get; set; }
        public string Product { get; set; }
        public DateTime? SignOffDT { get; set; }
    }
}