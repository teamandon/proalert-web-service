using System;
using System.ComponentModel.DataAnnotations;
using ProAlert.Andon.Service.Interfaces;

namespace ProAlert.Andon.Service.Models
{
    public partial class PlannedDt : Entity<int>
    {
        [Display(Name = "Planned Downtime Event"), StringLength(50)]
        public String Name { get; set; }

        [Display(Name = "Description"), StringLength(256)]
        public String Description { get; set; }

        public int? DisplayOrder { get; set; }
        //public PlannedDt()
        //{
        //    this.PlannedDtLogs = new List<PlannedDtLog>();
        //}

        //public int PlannedDtID { get; set; }
        //public string Name { get; set; }
        //public string Description { get; set; }
        //public Nullable<int> DisplayOrder { get; set; }
        //public virtual ICollection<PlannedDtLog> PlannedDtLogs { get; set; }
    }
}
