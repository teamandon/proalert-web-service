using System.ComponentModel.DataAnnotations;
using ProAlert.Andon.Service.Interfaces;

namespace ProAlert.Andon.Service.Models
{
    public partial class TzConversion : Entity<int>
    {
        [Display(Name = "Your web timezone"), StringLength(100)]
        public string Client { get; set; }
        [Display(Name = "System's timezone"), StringLength(100)]
        public string System { get; set; }
        //public string Client { get; set; }
        //public string System { get; set; }
    }
}
