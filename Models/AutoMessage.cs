using System;
using System.ComponentModel.DataAnnotations;
using ProAlert.Andon.Service.Common;
using ProAlert.Andon.Service.Interfaces;

namespace ProAlert.Andon.Service.Models
{
    public partial class AutoMessage : Entity<int>
    {
        [Display(Name = "Message"), StringLength(256)]
        public String Message { get; set; }

        [Display(Name = "Audio File")]
        public int AudioFileID { get; set; }

        [Display(Name = "Duration")]
        public int Duration { get; set; }

        [Display(Name = "Andon Display Line")]
        public Enumerations.DisplayLine? DisplayLine { get; set; }

        [Display(Name = "Message Color")]
        public Enumerations.DisplayColors? DisplayColors { get; set; }

        [Display(Name = "Start")]
        public DateTime Start { get; set; }

        [Display(Name = "Start")]
        public DateTime DispStart(TimeZoneInfo tzi)
        {
            return timeconversions.UTCtoClient(tzi, Start);
        }

        [Display(Name = "Reoccurring")]
        public bool Reoccurring { get; set; }

        [Display(Name = "Interval")]
        public Enumerations.AMInterval? Interval { get; set; }

        [Display(Name = "Interval Quantity")]
        public int? IntervalQty { get; set; }

        [Display(Name = "Creator")]
        public int? EmployeeID { get; set; }

        public byte[] Image { get; set; }

        // Navigations
        public AudioFile AudioFile { get; set; }
        public Employee Employee { get; set; }
        //public int AutoMessageID { get; set; }
        //public string Message { get; set; }
        //public int AudioFileID { get; set; }
        //public int Duration { get; set; }
        //public Nullable<int> DisplayLine { get; set; }
        //public Nullable<int> DisplayColors { get; set; }
        //public System.DateTime Start { get; set; }
        //public bool Reoccurring { get; set; }
        //public Nullable<int> Interval { get; set; }
        //public Nullable<int> IntervalQty { get; set; }
        //public Nullable<int> EmployeeID { get; set; }
        //public byte[] RowVersion { get; set; }
        //public byte[] Image { get; set; }
        //public virtual AudioFile AudioFile { get; set; }
        //public virtual Employee Employee { get; set; }
    }
}
