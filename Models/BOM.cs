

using ProAlert.Andon.Service.Interfaces;

namespace ProAlert.Andon.Service.Models
{
    public partial class BOM : Entity<int>
    {
        public int ComponentID { get; set; }
        public decimal Quantity { get; set; }
        public virtual Product Product { get; set; }
        public virtual Product Product1 { get; set; }
    }
}
