using System;
using System.ComponentModel.DataAnnotations;
using ProAlert.Andon.Service.Interfaces;

namespace ProAlert.Andon.Service.Models
{
    public partial class Email : Entity<int>
    {
        [Display(Name = "Work Center")]
        public int? WorkCenterID { get; set; }

        [Display(Name = "Recipient Name"), StringLength(100)]
        public string Recipient { get; set; }

        [Display(Name = "Recipient Address"), StringLength(256), DataType(DataType.EmailAddress)]
        public string RecipientEmail { get; set; }

        [Display(Name = "Delay (min)")]
        public int Delay { get; set; }

        [Display(Name = "Repeat Interval(min)")]
        public int Interval { get; set; }

        [Display(Name = "Down")]
        public bool Down { get; set; }

        [Display(Name = "Availability")]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal? Availablilty { get; set; }

        [Display(Name = "Performance")]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal? Performance { get; set; }

        [Display(Name = "Quality")]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal? Quality { get; set; }

        [Display(Name = "OEE")]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal? OEE { get; set; }

        [Display(Name = "Scrap (scrap:actual)")]
        public string Scrap { get; set; }

        [Display(Name = "Setup Time Exceeded by (min)")]
        public int SetupExceeded { get; set; }
    

    // Navigations
        public WorkCenter WorkCenter { get; set; }
        //public int EmailID { get; set; }
        //public Nullable<int> WorkCenterID { get; set; }
        //public string Recipient { get; set; }
        //public string RecipientEmail { get; set; }
        //public int Delay { get; set; }
        //public int Interval { get; set; }
        //public bool Down { get; set; }
        //public Nullable<decimal> Availablilty { get; set; }
        //public Nullable<decimal> Performance { get; set; }
        //public Nullable<decimal> Quality { get; set; }
        //public Nullable<decimal> OEE { get; set; }
        //public string Scrap { get; set; }
        //public virtual WorkCenter WorkCenter { get; set; }
    }
}
