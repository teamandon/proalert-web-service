﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProAlert.Andon.Service.Models
{
    public class StatsWc
    {
        public string LastDt { get; set; }
        public string TotalDt { get; set; }
        public string Scrap { get; set; }
        public string Rework { get; set; }
        public string PieceCnt { get; set; }
        public string Cycles { get; set; }
        public string MinutesPerPiece { get; set; }
        public string OperationTimeInMin { get; set; }
    }
}
