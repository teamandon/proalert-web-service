using System;
using System.ComponentModel.DataAnnotations;
using ProAlert.Andon.Service.Interfaces;

namespace ProAlert.Andon.Service.Models
{
    public partial class CallLogHistory : Entity<int>
    {

        public int CallLogID { get; set; }

        [Display(Name = "Call")]
        public int CallID { get; set; }

        [Display(Name = "Create Date"), DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd hh:mm}", ApplyFormatInEditMode = true)]
        public DateTime InitiateDt { get; set; }

        [Display(Name = "Response Time"), DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd hh:mm}", ApplyFormatInEditMode = true)]
        public DateTime? ResponseDt { get; set; }

        [Display(Name = "Resolved Time"), DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd hh:mm}", ApplyFormatInEditMode = true)]
        public DateTime? ResolveDt { get; set; }

        [Display(Name = "Work Center")]
        public int WorkCenterId { get; set; }

        [Display(Name = "Group Lead/Team Lead")]
        public int? EmployeeID { get; set; }

        [Display(Name = "Planned")]
        public Boolean Planned { get; set; }

        [Display(Name = "Issue")]
        public int? IssueID { get; set; }

        [Display(Name = "Subcategory")]
        public int? SubCategoryID { get; set; }

        [Display(Name = "Resolution Notes")]
        public String Resolution { get; set; }

        [Display(Name = "Operator Notes")]
        public String OperatorNotes { get; set; }

        [Display(Name = "Product")]
        public int? ProductID { get; set; }

        [Display(Name = "GL/TL Sign-Off")]
        public DateTime? SignOffDT { get; set; }
        //public int CallLogHistoryID { get; set; }
        //public int CallLogID { get; set; }
        //public int CallID { get; set; }
        //public System.DateTime InitiateDt { get; set; }
        //public Nullable<System.DateTime> ResponseDt { get; set; }
        //public Nullable<System.DateTime> ResolveDt { get; set; }
        //public int WorkCenterId { get; set; }
        //public Nullable<int> EmployeeID { get; set; }
        //public bool Planned { get; set; }
        //public Nullable<int> IssueID { get; set; }
        //public Nullable<int> SubCategoryID { get; set; }
        //public string Resolution { get; set; }
        //public string OperatorNotes { get; set; }
        //public Nullable<int> ProductID { get; set; }
        //public Nullable<System.DateTime> SignOffDT { get; set; }
    }
}
