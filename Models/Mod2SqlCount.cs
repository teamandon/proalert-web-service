﻿using System;

namespace ProAlert.Andon.Service.Models
{
    public class Mod2SqlCount
    {
        public string WorkCenterId { get; set; }
        public int Count { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
