﻿
using System;

namespace ProAlert.Andon.Service.Models
{
    public class OeeRptDtRange
    {
        public DateTime? Start { get; set; }
        public DateTime? Stop { get; set; }
    }
}