using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ProAlert.Andon.Service.Interfaces;

namespace ProAlert.Andon.Service.Models
{
    public partial class Product : Entity<int>
    {

        [Display(Name = "Product"), StringLength(50)]
        public string Name { get; set; }

        [Display(Name = "Description"), StringLength(256)]
        public string Description { get; set; }

        [Display(Name = "Image Path"), StringLength(256)]
        public string ImagePath { get; set; }

        [Display(Name = "Material"), StringLength(256)]
        public string Material { get; set; }

        [Display(Name = "Standard Pack")]
        public int StandardPack { get; set; }

        [Display(Name="Blanket PO")]
        public string BlanketPo { get; set; }

        public byte[] Image { get; set; }


        // Navigations
        public virtual ICollection<ProductWorkCenterStat> ProductWorkCenterStats { get; set; }
        //public Product()
        //{
        //    this.BOMs = new List<BOM>();
        //    this.BOMs1 = new List<BOM>();
        //    this.CallLogs = new List<CallLog>();
        //    this.PlannedDtLogs = new List<PlannedDtLog>();
        //    this.ProductWorkCenterStats = new List<ProductWorkCenterStat>();
        //    this.Scraps = new List<Scrap>();
        //    this.WCProductTimelines = new List<WCProductTimeline>();
        //    this.WorkCenters = new List<WorkCenter>();
        //}

        //public int ProductID { get; set; }
        //public string Name { get; set; }
        //public string Description { get; set; }
        //public string ImagePath { get; set; }
        //public string Material { get; set; }
        //public int StandardPack { get; set; }
        //public byte[] Image { get; set; }
        //public byte[] RowVersion { get; set; }
        //public virtual ICollection<BOM> BOMs { get; set; }
        //public virtual ICollection<BOM> BOMs1 { get; set; }
        //public virtual ICollection<CallLog> CallLogs { get; set; }
        //public virtual ICollection<PlannedDtLog> PlannedDtLogs { get; set; }
        //public virtual ICollection<ProductWorkCenterStat> ProductWorkCenterStats { get; set; }
        //public virtual ICollection<Scrap> Scraps { get; set; }
        //public virtual ICollection<WCProductTimeline> WCProductTimelines { get; set; }
        //public virtual ICollection<WorkCenter> WorkCenters { get; set; }
    }
}
