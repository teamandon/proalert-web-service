using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using ProAlert.Andon.Service.Interfaces;

namespace ProAlert.Andon.Service.Models
{
    public partial class Timesegment : Entity<int>
    {

        // Time segments are a unique contiguous timespan for a WC that will be one of four types
        // Only one of the boolean values should be true;thus, that type.  Else, none: then being uneventful production.
        public int WCProductTimelineID { get; set; }
        public int? CycleSummaryID { get; set; }

        public DateTime Start { get; set; }
        public DateTime? Stop { get; set; }
        public bool? UnplannedDt { get; set; }
        public bool? PlannedDt { get; set; }
        public bool? ProductChangeOver { get; set; }


        // Navigations
        [JsonIgnore]
        public virtual WCProductTimeline WcProductTimeline { get; set; }
        [JsonIgnore]
        public virtual ICollection<CallLog> CallLogs { get; set; }
        [JsonIgnore]
        public virtual ICollection<PlannedDtLog> PlannedDtLogs { get; set; }
        [JsonIgnore]
        public virtual ICollection<Scrap> Scraps { get; set; }
        [JsonIgnore]
        public virtual CycleSummary CycleSummary { get; set; }
        public Timesegment()
        {
            this.CallLogs = new List<CallLog>();
            this.PlannedDtLogs = new List<PlannedDtLog>();
            this.Scraps = new List<Scrap>();
        }

        //public int TimesegmentID { get; set; }
        //public int WCProductTimelineID { get; set; }
        //public Nullable<int> CycleSummaryID { get; set; }
        //public System.DateTime Start { get; set; }
        //public Nullable<System.DateTime> Stop { get; set; }
        //public Nullable<bool> DT { get; set; }
        //public Nullable<bool> plannedDT { get; set; }
        //public Nullable<bool> ProductChangeOver { get; set; }
        //public virtual ICollection<CallLog> CallLogs { get; set; }
        //public virtual CycleSummary CycleSummary { get; set; }
        //public virtual ICollection<PlannedDtLog> PlannedDtLogs { get; set; }
        //public virtual ICollection<Scrap> Scraps { get; set; }
        //public virtual WCProductTimeline WCProductTimeline { get; set; }
    }
}
