using System;
using ProAlert.Andon.Service.Interfaces;

namespace ProAlert.Andon.Service.Models
{
    public partial class AppError 
    {
        public int AppErrorID { get; set; }
        public string ErrorMsg { get; set; }
        public System.DateTime ErrorDT { get; set; }
        public Nullable<int> UserID { get; set; }
        public string Source { get; set; }
    }
}
