﻿namespace ProAlert.Andon.Service.Models
{
    public class AdvisoryMsgSet
    {
        public string Advisory { get; set; }
        public byte[] Image { get; set; }
    }
}
