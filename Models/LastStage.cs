using System.ComponentModel.DataAnnotations;
using ProAlert.Andon.Service.Interfaces;

namespace ProAlert.Andon.Service.Models
{
    public partial class LastStage : Entity<int>
    {
        [Display(Name = "Stage"), StringLength(50)]
        public string Name { get; set; }

        [Display(Name = "Description"), StringLength(256)]
        public string Description { get; set; }

        [ScaffoldColumn(false)]
        public int? DispOrder { get; set; }

        //public LastStage()
        //{
        //    this.Scraps = new List<Scrap>();
        //}

        //public int LastStageID { get; set; }
        //public string Name { get; set; }
        //public string Description { get; set; }
        //public Nullable<int> DispOrder { get; set; }
        //public byte[] RowVersion { get; set; }
        //public virtual ICollection<Scrap> Scraps { get; set; }
    }
}
