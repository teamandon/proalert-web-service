using System.ComponentModel.DataAnnotations;
using ProAlert.Andon.Service.Interfaces;

namespace ProAlert.Andon.Service.Models
{
    public partial class Position : Entity<int>
    {

        [Display(Name = "Title"), StringLength(50)]
        public string Title { get; set; }


        //public Position()
        //{
        //    this.Employees = new List<Employee>();
        //}

        //public int PositionID { get; set; }
        //public string Title { get; set; }
        //public byte[] RowVersion { get; set; }
        //public virtual ICollection<Employee> Employees { get; set; }
    }
}
