using System;
using System.Collections.Generic;

namespace Vistech.Andon.Model.Models
{
    public partial class DtCompressed
    {
        public int DtCompressedID { get; set; }
        public int WCProductTimelineID { get; set; }

        public DateTime Start { get; set; }
        public DateTime Stop { get; set; }
        public double Milliseconds { get; set; }


        // Navigations
        public virtual WCProductTimeline WcProductTimeline { get; set; }

        #region Methods
        public void SetMilliseconds()
        {
            Milliseconds = Stop.Subtract(Start).TotalMilliseconds;
        }
        #endregion
        //public int DtCompressedID { get; set; }
        //public int WCProductTimelineID { get; set; }
        //public System.DateTime Start { get; set; }
        //public System.DateTime Stop { get; set; }
        //public double Milliseconds { get; set; }
        //public virtual WCProductTimeline WCProductTimeline { get; set; }
    }
}
