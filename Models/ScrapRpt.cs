﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProAlert.Andon.Service.Models
{
    public class ScrapRpt
    {
        public string WC { get; set; }
        public string Product { get; set; }
        [Display(Name="Sign-off By")]
        public string SignOffBy { get; set; }
        [Display(Name="Scrap Date")]
        public DateTime ScrapDT { get; set; }
        [Display(Name="Scrap Count")]
        public int  ScrapCount { get; set; }
        public string Notes { get; set; }
        [Display(Name="Defect Location")]
        public string DefectLocation { get; set; }
        [Display(Name="Sign-off Date")]
        public DateTime? SignoffDT { get; set; }
        [Display(Name="Scrap Type")]
        public string ScrapType { get; set; }
        [Display(Name="Last Stage")]
        public string LastStage { get; set; }
        [Display(Name="Reject Code")]
        public string RejectCode { get; set; }
    }
}
