﻿using System;

namespace ProAlert.Andon.Service.Models
{
    public class Mod2SqlStatus
    {
        public string WorkCenterId { get; set; }
        public int OeeStatus { get; set; }
        public DateTime UpdateDt { get; set; }
    }
}
