using System;
using System.ComponentModel.DataAnnotations;
using ProAlert.Andon.Service.Common;
using ProAlert.Andon.Service.Interfaces;

namespace ProAlert.Andon.Service.Models
{
    public partial class Scrap : Entity<int>
    {
        [Display(Name = "Work Center")]
        public int? WorkCenterID { get; set; }

        [Display(Name = "Product")]
        public int? ProductID { get; set; }

        [Display(Name = "Last Stage")]
        public int? LastStageID { get; set; }

        [Display(Name = "Reject Code")]
        public int? RejectCodeID { get; set; }

        [Display(Name = "Scrap Type")]
        public int? ScrapTypeID { get; set; }

        [Display(Name = "Scrap Date / Time")]
        public DateTime ScrapDT { get; set; }

        /// <summary>
        /// Display UTC scrap dt in local time
        /// </summary>
        /// <param name="tzi"></param>
        /// <returns></returns>
        public DateTime DispScrapDt(TimeZoneInfo tzi)
        {
            return timeconversions.UTCtoClient(tzi, ScrapDT);
        }

        [Display(Name = "Scrap Count")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Please enter valid Number")]
        public int ScrapCount { get; set; }

        [Display(Name = "Cause of Scrap"), StringLength(500)]
        public String Notes { get; set; }

        [Display(Name = "Location of Defect"), StringLength(256)]
        public String DefectLocation { get; set; }

        [Display(Name = "Sign-off")]
        public DateTime? SignoffDT { get; set; }

        [ScaffoldColumn(false)]
        public int? EmployeeID { get; set; }

        public int? TimesegmentID { get; set; }

        // if Allocated is true, then this should not be null
        public int? ScrapParentID { get; set; }
        // Set to true if successful in creating child records.  Needs to be in transaction for roll back.
        [Display(Name="Original")]
        public bool Allocated { get; set; }

        // Navigations
        public WorkCenter WorkCenter { get; set; }
        public virtual Product Product { get; set; }
        public virtual LastStage LastStage { get; set; }
        public virtual RejectCode RejectCode { get; set; }
        public virtual ScrapType ScrapType { get; set; }
        public virtual Employee Employee { get; set; }
        //public virtual Timesegment Timesegment { get; set; }
        public virtual Scrap ScrapParent { get; set; }
        //public Scrap()
        //{
        //    this.Scrap1 = new List<Scrap>();
        //}

        //public int ScrapID { get; set; }
        //public Nullable<int> WorkCenterID { get; set; }
        //public Nullable<int> ProductID { get; set; }
        //public Nullable<int> LastStageID { get; set; }
        //public Nullable<int> RejectCodeID { get; set; }
        //public Nullable<int> ScrapTypeID { get; set; }
        //public System.DateTime ScrapDT { get; set; }
        //public int ScrapCount { get; set; }
        //public string Notes { get; set; }
        //public string DefectLocation { get; set; }
        //public Nullable<System.DateTime> SignoffDT { get; set; }
        //public Nullable<int> EmployeeID { get; set; }
        //public Nullable<int> TimesegmentID { get; set; }
        //public Nullable<int> ScrapParentID { get; set; }
        //public bool Allocated { get; set; }
        //public virtual Employee Employee { get; set; }
        //public virtual LastStage LastStage { get; set; }
        //public virtual Product Product { get; set; }
        //public virtual RejectCode RejectCode { get; set; }
        //public virtual ICollection<Scrap> Scrap1 { get; set; }
        //public virtual Scrap Scrap2 { get; set; }
        //public virtual ScrapType ScrapType { get; set; }
        //public virtual Timesegment Timesegment { get; set; }
        //public virtual WorkCenter WorkCenter { get; set; }
    }
}
