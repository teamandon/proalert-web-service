using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ProAlert.Andon.Service.Interfaces;

namespace ProAlert.Andon.Service.Models
{
    public partial class ScrapType : Entity<int>
    {
        [Display(Name = "Scrap Type"), StringLength(50)]
        public string Name { get; set; }

        [Display(Name = "Description"), StringLength(256)]
        public string Description { get; set; }

        [Display(Name = "Default")]
        public bool Default { get; set; }

        [ScaffoldColumn(false)]
        public int? DispOrder { get; set; }

        [ScaffoldColumn(false), NotMapped]
        public int? Qty { get; set; }

        //public ScrapType()
        //{
        //    this.Scraps = new List<Scrap>();
        //}

        //public int ScrapTypeID { get; set; }
        //public string Name { get; set; }
        //public string Description { get; set; }
        //public bool Default { get; set; }
        //public Nullable<int> DispOrder { get; set; }
        //public byte[] RowVersion { get; set; }
        //public virtual ICollection<Scrap> Scraps { get; set; }
    }
}
