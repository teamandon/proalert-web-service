﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ProAlert.Andon.Service.Interfaces;

namespace ProAlert.Andon.Service.Models
{
    public class DdVmJson
    {
        [Required]
        [Key, ForeignKey("WcProductTimeline")]
        public int TlId { get; set; }
        public string Json { get; set; }

        // Navigation
        public WCProductTimeline WcProductTimeline { get; set; }
    }
}
