using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ProAlert.Andon.Service.Common;
using ProAlert.Andon.Service.Interfaces;

namespace ProAlert.Andon.Service.Models
{
    public partial class WorkCenter : Entity<int>
    {

        [Display(Name = "Work Center"), StringLength(50)]
        public string Name { get; set; }

        [Display(Name = "Description"), StringLength(256)]
        public string Description { get; set; }

        [ScaffoldColumn(false)]
        public bool? UnplannedDt { get; set; }

        [ScaffoldColumn(false)]
        public bool? PlannedDt { get; set; }

        [ScaffoldColumn(false)]
        public DateTime OEEStartTime { get; set; }

        public DateTime DisplayOEEStart(TimeZoneInfo tzi)
        {
            return timeconversions.UTCtoClient(tzi, OEEStartTime);
        }

        [Display(Name = "Product")]
        public int? ProductID { get; set; }

        public int? WorkCenterGUIOptionsID { get; set; }

        public int? WorkCenterPrintingOptionsID { get; set; }

        [Display(Name="GUI Running")]
        public bool GuiRunning { get; set; }
        [Display(Name="Machine Running")]
        public bool MachineRunning { get; set; }

        [Display(Name = "Loaded Material")]
        public int? Serial { get; set; }
        // Navigations
        public virtual ICollection<WorkCenterCall> WorkCenterCalls { get; set; }
        public virtual ICollection<ProductWorkCenterStat> ProductWorkCenterStats { get; set; }
        public virtual WorkCenterGUIOptions WorkCenterGUIOptions { get; set; }
        public virtual WorkCenterPrintingOptions WorkCenterPrintingOptions { get; set; }
        //public virtual ICollection<Cycle> Cycles { get; set; } 
        //public virtual ICollection<CycleHistory> CycleHistories { get; set; }
        //public virtual ICollection<PlannedDtLog> PlannedDtLogs { get; set; }
        public virtual Product Product { get; set; }
        //public WorkCenter()
        //{
        //    this.CallLogs = new List<CallLog>();
        //    this.Cycles = new List<Cycle>();
        //    this.CycleHistories = new List<CycleHistory>();
        //    this.Emails = new List<Email>();
        //    this.PlannedDtLogs = new List<PlannedDtLog>();
        //    this.ProductWorkCenterStats = new List<ProductWorkCenterStat>();
        //    this.Scraps = new List<Scrap>();
        //    this.WCProductTimelines = new List<WCProductTimeline>();
        //    this.WorkCenterCalls = new List<WorkCenterCall>();
        //}

        //public int WorkCenterID { get; set; }
        //public string Name { get; set; }
        //public string Description { get; set; }
        //public Nullable<bool> Down { get; set; }
        //public Nullable<bool> Planned { get; set; }
        //public System.DateTime OEEStartTime { get; set; }
        //public Nullable<int> ProductID { get; set; }
        //public byte[] RowVersion { get; set; }
        //public virtual ICollection<CallLog> CallLogs { get; set; }
        //public virtual ICollection<Cycle> Cycles { get; set; }
        //public virtual ICollection<CycleHistory> CycleHistories { get; set; }
        //public virtual ICollection<Email> Emails { get; set; }
        //public virtual ICollection<PlannedDtLog> PlannedDtLogs { get; set; }
        //public virtual Product Product { get; set; }
        //public virtual ICollection<ProductWorkCenterStat> ProductWorkCenterStats { get; set; }
        //public virtual ICollection<Scrap> Scraps { get; set; }
        //public virtual ICollection<WCProductTimeline> WCProductTimelines { get; set; }
        //public virtual ICollection<WorkCenterCall> WorkCenterCalls { get; set; }
    }
}
