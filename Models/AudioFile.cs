using System.ComponentModel.DataAnnotations;
using ProAlert.Andon.Service.Interfaces;

namespace ProAlert.Andon.Service.Models
{
    public partial class AudioFile : Entity<int>
    {
        [Display(Name = "Sound Name"), StringLength(50)]
        public string Name { get; set; }

        [Display(Name = "Path"), StringLength(256)]
        public string Path { get; set; }

        public byte[] Sound { get; set; }

        [Display(Name = "Times to Repeat")]
        public int RepeatCnt { get; set; }


        //public AudioFile()
        //{
        //    this.AutoMessages = new List<AutoMessage>();
        //    this.Calls = new List<Call>();
        //}

        //public int AudioFileID { get; set; }
        //public string Name { get; set; }
        //public string Path { get; set; }
        //public byte[] Sound { get; set; }
        //public int RepeatCnt { get; set; }
        //public byte[] RowVersion { get; set; }
        //public virtual ICollection<AutoMessage> AutoMessages { get; set; }
        //public virtual ICollection<Call> Calls { get; set; }
    }
}
