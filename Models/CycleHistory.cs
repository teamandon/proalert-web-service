using System;
using ProAlert.Andon.Service.Common;
using ProAlert.Andon.Service.Interfaces;

namespace ProAlert.Andon.Service.Models
{
    public partial class CycleHistory : Entity<int>
    {
        public int WorkCenterID { get; set; }

        public int Count { get; set; }

        public DateTime UpdateDT { get; set; }

        /// <summary>
        /// Display UTC in users local time
        /// </summary>
        /// <param name="tzi"></param>
        /// <returns></returns>
        public DateTime DispUpdateDt(TimeZoneInfo tzi)
        {
            return timeconversions.UTCtoClient(tzi, UpdateDT);
        }

        // Navigations
        public virtual WorkCenter WorkCenter { get; set; }
        //public int CycleHistoryID { get; set; }
        //public int WorkCenterID { get; set; }
        //public int Count { get; set; }
        //public System.DateTime UpdateDT { get; set; }
        //public virtual WorkCenter WorkCenter { get; set; }
    }
}
