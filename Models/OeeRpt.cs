﻿using System;

namespace ProAlert.Andon.Service.Models
{
    public class OeeRpt
    {
        public string WorkCenter { get; set; }
        public string Product { get; set; }
        public DateTime Start { get; set; }
        public DateTime? Stop { get; set; }
        public double TotalDT { get; set; }
        public double TotalPlannedDT { get; set; }
        public double PlannedProduction { get; set; }
        public double OperationTime { get; set; }
        public decimal UnitsPerCycle { get; set; }
        public decimal CyclesPerHour { get; set; }
        public decimal Availability { get; set; }
        public decimal Performance { get; set; }
        public decimal Quality { get; set; }
        public decimal OEE { get; set; }
        public string LastDtReason { get; set; }
        public string WCProduct { get; set; }
        public decimal MFGScrap { get; set; }
        public decimal MatlScrap { get; set; }
        public decimal ReworkScrap { get; set; }
        public decimal OtherScrap { get; set; }
        public decimal ActualPieces { get; set; }
        public decimal ActualCycles { get; set; }
    }
}