﻿using System.ComponentModel.DataAnnotations;
using ProAlert.Andon.Service.Interfaces;

namespace ProAlert.Andon.Service.Models
{
    public class Person : Entity<int>
    {
        [Display(Name = "First Name"), StringLength(50)]
        public string FirstName { get; set; }
        [Display(Name = "Last Name"), StringLength(50)]
        public string LastName { get; set; }
        [Display(Name = "Cell"), DataType(DataType.PhoneNumber)]
        public string Cell { get; set; }
        [Display(Name = "Email"), DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Display(Name = "Picture")]
        public byte[] Picture { get; set; }

        [Display(Name="Name")]
        public string FullName => LastName + ", " + FirstName;
    }
}