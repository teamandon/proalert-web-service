using System;
using System.Collections.Generic;
using ProAlert.Andon.Service.Interfaces;

namespace ProAlert.Andon.Service.Models
{
    public partial class WCProductTimelineHistory : Entity<int>
    {
        public int WCProductTimelineID { get; set; }
        public Nullable<int> WorkCenterID { get; set; }
        public Nullable<int> ProductID { get; set; }
        public System.DateTime Start { get; set; }
        public Nullable<System.DateTime> Stop { get; set; }
        public decimal TotalDT { get; set; }
        public decimal TotalPlannedDT { get; set; }
        public decimal PlannedProduction { get; set; }
        public decimal OperationTime { get; set; }
        public int? ProductWcStatsHistoryID { get; set; }
        public bool Current { get; set; }
        public decimal Availability { get; set; }
        public decimal Performance { get; set; }
        public decimal Quality { get; set; }
        public decimal OEE { get; set; }
        public string LastDTReason { get; set; }
        public string WCProduct { get; set; }
        public bool Down { get; set; }
        public bool WCPlanned { get; set; }
        public decimal Mfg { get; set; }
        public decimal Matl { get; set; }
        public decimal Rework { get; set; }
        public decimal Other { get; set; }
        public decimal ActualPieces { get; set; }
        public decimal ActualCycles { get; set; }
        public decimal TotalDTInMin => TotalDT / 60000;

        public decimal ScrapOneNumber => Mfg + Matl + Rework + Other;
        //public virtual ProductWCStatsHistory ProductWCStatsHistory { get; set; }
        public ProductWCStatsHistory ProductWcStatsHistory { get; set; }

    }
}
