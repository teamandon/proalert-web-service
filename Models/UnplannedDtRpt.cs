﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.Security.Permissions;

namespace ProAlert.Andon.Service.Models
{
    public class UnplannedDtRpt
    {
        [Display(Name = "Work Center")]
        public string WorkCenter { get; set; }
        public string Product { get; set; }
        [Display(Name = "Event Type")]
        public string Name { get; set; }
        public DateTime Initiated { get; set; }
        public DateTime? Response { get; set; }
        public DateTime? Resolved { get; set; }
        public string Maintenance { get; set; }
        public DateTime? SignOff { get; set; }
        public string Issue { get; set; }
        public string Subcategory { get; set; }
        public string OperatorNotes { get; set; }
        public string ResolutionNotes { get; set; }
    }
}
