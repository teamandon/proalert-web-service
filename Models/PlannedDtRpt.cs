﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProAlert.Andon.Service.Models
{
    public class PlannedDtRpt
    {
        [Display(Name = "Work Center")]
        public string WorkCenter { get; set; }
        public string Product { get; set; }
        [Display(Name="Event Type")]
        public string Name { get; set; }
        public DateTime Start { get; set; }
        public DateTime? Stop { get; set; }
        public string Notes { get; set; }

    }
}
