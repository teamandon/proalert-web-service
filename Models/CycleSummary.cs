using ProAlert.Andon.Service.Interfaces;

namespace ProAlert.Andon.Service.Models
{
    public partial class CycleSummary : Entity<int>
    {
        public int Count { get; set; }

        public int LastCycleHistoryID { get; set; }
        //public CycleSummary()
        //{
        //    this.Timesegments = new List<Timesegment>();
        //}

        //public int CycleSummaryID { get; set; }
        //public int Count { get; set; }
        //public int LastCycleHistoryID { get; set; }
        //public virtual ICollection<Timesegment> Timesegments { get; set; }
    }
}
