﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProAlert.Andon.Service.Models
{
    public class OeeHeaderWc
    {
        public string Performance { get; set; }
        public string Availability { get; set; }
        public string Quality { get; set; }
        public string OEE { get; set; }
        public string Target { get; set; }

    }
}
