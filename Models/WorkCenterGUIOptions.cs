﻿using System.ComponentModel.DataAnnotations;
using ProAlert.Andon.Service.Interfaces;

namespace ProAlert.Andon.Service.Models
{
    public class WorkCenterGUIOptions : Entity<int>
    {
        [Display(Name="Manual Cycles")]
        public bool? ManualCycles { get; set; }
        [Display(Name="Single Scrap")]
        public bool? SingleScrap { get; set; }
    }
}
