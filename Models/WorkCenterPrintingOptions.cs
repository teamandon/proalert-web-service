﻿
using System.ComponentModel.DataAnnotations;
using ProAlert.Andon.Service.Interfaces;

namespace ProAlert.Andon.Service.Models
{
    public class WorkCenterPrintingOptions : Entity<int>
    {
        [Display(Name="Scrap Label")]
        public bool? ScrapLabel { get; set; }
        [Display(Name="Shipping Label")]
        public bool? ShippingLabel { get; set; }
        [Display(Name="Scrap Printer")]
        public string ScrapPrinter { get; set; }
        [Display(Name="Shipping Printer")]
        public string ShippingPrinter { get; set; }
    }
}
