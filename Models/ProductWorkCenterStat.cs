using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using ProAlert.Andon.Service.Interfaces;

namespace ProAlert.Andon.Service.Models
{
    public partial class ProductWorkCenterStat : Entity<int>
    {
        [Display(Name = "Work Center")]
        public int WorkCenterID { get; set; }

        [Display(Name = "Product")]
        public int ProductID { get; set; }

        [Display(Name = "Cycles Per Hour")]
        [DisplayFormat(DataFormatString = "{0:N5}", ApplyFormatInEditMode = true)]
        public decimal CyclesPerHour { get; set; }

        [Display(Name = "Units Per Cycle")]
        [DisplayFormat(DataFormatString = "{0:N5}", ApplyFormatInEditMode = true)]
        public decimal UnitsPerCycle { get; set; }

        [Display(Name = "Yield")]
        [DisplayFormat(DataFormatString = "{0:N5}", ApplyFormatInEditMode = true)]
        public decimal Yield { get; set; }

        [Display(Name = "Setup Time (Minutes)")]
        [DisplayFormat(DataFormatString = "{0:N5}", ApplyFormatInEditMode = true)]
        public decimal SetUpTime { get; set; }

        [Display(Name = "Availability Target (.94)")]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal Available { get; set; }

        [Display(Name = "Performance Target (.92)")]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal Performance { get; set; }

        [Display(Name = "Quality Target (.98)")]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal Quality { get; set; }

        [Display(Name = "OEE Target")]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal OEE
        {
            get { return Available * Performance * Quality; }
        }

        [Display(Name = "Availability Cap")]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal AvailableCap { get; set; }

        [Display(Name = "Performance Cap (1.5)")]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal PerformanceCap { get; set; }

        [Display(Name = "Quality Cap")]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal QualityCap { get; set; }


        public ProductWorkCenterStat()
        {
            Available = (decimal)0.94;
            Performance = (decimal)0.92;
            Quality = (decimal)0.98;
            AvailableCap = (decimal)1;
            PerformanceCap = (decimal)1;
            QualityCap = (decimal)1;
        }

        // Navigations
        [JsonIgnore]
        public virtual WorkCenter WorkCenter { get; set; }
        [JsonIgnore]
        public virtual Product Product { get; set; }

        //public int ProductWorkCenterStatID { get; set; }
        //public int WorkCenterID { get; set; }
        //public int ProductID { get; set; }
        //public decimal CyclesPerHour { get; set; }
        //public int UnitsPerCycle { get; set; }
        //public decimal Yield { get; set; }
        //public decimal SetUpTime { get; set; }
        //public decimal Available { get; set; }
        //public decimal Performance { get; set; }
        //public decimal Quality { get; set; }
        //public decimal AvailableCap { get; set; }
        //public decimal PerformanceCap { get; set; }
        //public decimal QualityCap { get; set; }
        //public byte[] RowVersion { get; set; }
        //public virtual Product Product { get; set; }
        //public virtual WorkCenter WorkCenter { get; set; }
    }
}
