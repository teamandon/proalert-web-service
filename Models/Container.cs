﻿
using System;
using ProAlert.Andon.Service.Interfaces;

namespace ProAlert.Andon.Service.Models
{
    public class Container : Entity<int>
    {
        public int WorkCenterId { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public int SerialNumber { get; set; }
        public string PO { get; set; }
        public string Location { get; set; }
        public DateTime PrintDate { get; set; }

        public string ContainerId()
            =>
                PrintDate.Year.ToString().PadLeft(2, '0') + PrintDate.DayOfYear.ToString().PadLeft(3, '0') + (Id > 9999
                    ? Id.ToString().Substring((Id.ToString().Length - 4), 4)
                    : Id.ToString().PadLeft(4, '0'));
    }
}
