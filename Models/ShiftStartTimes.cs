﻿using System;
using System.ComponentModel.DataAnnotations;
using ProAlert.Andon.Service.Interfaces;

namespace ProAlert.Andon.Service.Models
{
    public class ShiftStartTimes : Entity<int>
    {
        [Display(Name="First Shift Start Time")]
        public DateTime FirstStart { get; set; }
        [Display(Name = "Second Shift Start Time")]
        public DateTime SecondStart { get; set; }
        [Display(Name = "Third Shift Start Time")]
        public DateTime ThirdStart { get; set; }
    }
}
