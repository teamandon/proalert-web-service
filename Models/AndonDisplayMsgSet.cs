﻿namespace ProAlert.Andon.Service.Models
{
    public class AndonDisplayMsgSet
    {
        public string Maintenance { get; set; }
        public string Supervisor { get; set; }
        public string Materials { get; set; }
    }
}