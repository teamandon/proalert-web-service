﻿using System;

namespace ProAlert.Andon.Service.Models
{
    public class FreeAndonCall
    {
        public string WorkCenter { get; set; }
        public string Product { get; set; }
        public string Call { get; set; }
        public DateTime Initiated { get; set; }
        public DateTime? Response { get; set; }
    }
}
