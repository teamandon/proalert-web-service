﻿using System;

namespace ProAlert.Andon.Service.Models
{
    public class ActiveCall
    {
        public int WorkCenterId { get; set; }
        public int Id { get; set; }
        public int CallId { get; set; }
        public string CallName { get; set; }
        public DateTime InitiateDt { get; set; }
        public DateTime? ResponseDt { get; set; }

    }
}
