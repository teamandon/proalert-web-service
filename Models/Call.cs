using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ProAlert.Andon.Service.Common;
using ProAlert.Andon.Service.Interfaces;

namespace ProAlert.Andon.Service.Models
{
    public partial class Call : Entity<int>
    {
        [Display(Name = "Call Name"), StringLength(50)]
        public string Name { get; set; }

        [Display(Name = "Call Type")]
        public Enumerations.CallType? Type { get; set; }

        [Display(Name = "Downtime?")]
        public bool DT { get; set; }

        [Display(Name = "Description"), StringLength(256)]
        public String Description { get; set; }

        [Display(Name = "Audio File")]
        public int AudioFileID { get; set; }


        // Navigations

        //public virtual ICollection<CallLog> CallLogs { get; set; }
        public virtual ICollection<WorkCenterCall> WorkCenterCalls { get; set; }
        public virtual AudioFile AudioFile { get; set; }
        //public Call()
        //{
        //    this.CallLogs = new List<CallLog>();
        //    this.WorkCenterCalls = new List<WorkCenterCall>();
        //}

        //public int CallID { get; set; }
        //public string Name { get; set; }
        //public Nullable<int> Type { get; set; }
        //public bool DT { get; set; }
        //public string Description { get; set; }
        //public int AudioFileID { get; set; }
        //public byte[] RowVersion { get; set; }
        //public virtual AudioFile AudioFile { get; set; }
        //public virtual ICollection<CallLog> CallLogs { get; set; }
        //public virtual ICollection<WorkCenterCall> WorkCenterCalls { get; set; }
    }
}
