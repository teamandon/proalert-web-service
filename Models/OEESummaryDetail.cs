﻿
namespace ProAlert.Andon.Service.Models
{
    public class OEESummaryDetail
    {
        public string CacheId { get; set; }
        //  OEE timeframe:&#013; @(timeconversions.UTCtoClient((TimeZoneInfo) Session["tzi"], dr.Start).ToString("M/dd/yyyy HH:mm:ss")) &#013; - @timeconversions.UTCtoClient((TimeZoneInfo) Session["tzi"], dr.Stop).ToString("M/dd/yyyy HH:mm:ss")
        public string TimeFrame { get; set; }
        //@dr.TL.WCProduct ( @dr.CyclesPerMin.ToString("N0") )
        public string WcProductCyclesPerMin { get; set; }
        // (dr.TL.PlannedProduction / 60000).ToString("N0")) ( @((dr.TL.OperationTime / 60000).ToString("N0"))
        public string PlannedVsOperational { get; set; }
        //dr.TL.TotalDTInMin.ToString("N1")
        public string Down { get; set; }
        //dr.TL.LastDTReason
        public string LastDown { get; set; }
        // dr.TL.Mfg > 0 ? "MFG: " + dr.TL.Mfg : string.Empty; scrap += dr.TL.Matl > 0 ? " Matl: " + dr.TL.Matl : string.Empty; scrap += dr.TL.Other > 0 ? " Other: " + dr.TL.Other : string.Empty; scrap += dr.TL.Rework > 0 ? " Rework: " + dr.TL.Rework : string.Empty;
        public string ScrapTitle { get; set; }
        // dr.TL.ScrapOneNumber.ToString("N0")
        public string Scrap { get; set; }
        // dr.PieceCnt
        public string TargetVsActual { get; set; }
        // dr.TL.Availability.ToString("P1")
        public string Availability { get; set; }
        // dr.TL.Performance.ToString("P1")
        public string Performance { get; set; }
        // dr.TL.Quality.ToString("P1")
        public string Quality { get; set; }
        // dr.TL.OEE.ToString("P1")
        public string Oee { get; set; }
        public string AvailColor { get; set; }
        public string PerformColor { get; set; }
        public string QualColor { get; set; }
        public string OeeColor { get; set; }
        //  State of WC  (planned, unplanned, running)
        public string WcStatusColor { get; set; }
        public string Ocolor { get; set; }
        public string TotalOeeAvg { get; set; }
        public string TargetOeeAvg { get; set; }
        public string Current { get; set; }
    }
}
