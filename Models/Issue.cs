using System.ComponentModel.DataAnnotations;
using ProAlert.Andon.Service.Interfaces;

namespace ProAlert.Andon.Service.Models
{
    public partial class Issue : Entity<int>
    {
        [Display(Name = "Problem"), StringLength(50), Required]
        public string Name { get; set; }

        [Display(Name = "Description"), StringLength(256)]
        public string Description { get; set; }

        [Display(Name = "Material")]
        public bool Material { get; set; }

        [Display(Name = "Manufacturing")]
        public bool Manufacturing { get; set; }

        [Display(Name = "Rework")]
        public bool Rework { get; set; }

        [Display(Name = "Downtime")]
        public bool DT { get; set; }

        [Display(Name = "Sub Category")]
        public int? SubCategoryID { get; set; }

        [ScaffoldColumn(false)]
        public int? DispOrder { get; set; }

        // Navigations
        public virtual SubCategory SubCategory { get; set; }

        //public Issue()
        //{
        //    this.CallLogs = new List<CallLog>();
        //}

        //public int IssueID { get; set; }
        //public string Name { get; set; }
        //public string Description { get; set; }
        //public bool Material { get; set; }
        //public bool Manufacturing { get; set; }
        //public bool Rework { get; set; }
        //public bool DT { get; set; }
        //public Nullable<int> SubCategoryID { get; set; }
        //public Nullable<int> DispOrder { get; set; }
        //public byte[] RowVersion { get; set; }
        //public virtual ICollection<CallLog> CallLogs { get; set; }
        //public virtual SubCategory SubCategory { get; set; }
    }
}
