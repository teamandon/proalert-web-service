﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Vistech.Andon.Model.Models;

namespace Vistech.Andon.Service.Common
{
    public class GenericRepository<TEntity> where TEntity : class
    {
        internal AndonVistechContext context;
        internal DbSet<TEntity> dbSet;

        public GenericRepository(AndonVistechContext context)
        {
            this.context = context;
            this.dbSet = context.Set<TEntity>();
            context.Configuration.ProxyCreationEnabled = false;
        }

        public virtual IEnumerable<TEntity> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<TEntity> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            return query.ToList();
        }

        public virtual TEntity GetByID(object id)
        {
            return dbSet.Find(id);
        }

        public virtual TEntity Create()
        {
            return dbSet.Create();
        }

        public virtual void Insert(TEntity entity)
        {
            dbSet.Add(entity);
        }
       
        public virtual void Delete(object id)
        {
            TEntity entityToDelete = dbSet.Find(id);
            Delete(entityToDelete);
        }

        public virtual void Delete(TEntity entityToDelete)
        {
            if (context.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);
        }

        public virtual EntityState GetState(TEntity entity)
        {
            return context.Entry<TEntity>(entity).State;
        }

        public virtual void Update(TEntity entity)
        {
            dbSet.Attach(entity);
            context.Entry(entity).State = EntityState.Modified;
        }

    }
}