﻿using System;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;

namespace ProAlert.Andon.Service.Common
{
    public class timeconversions
    {
        private static IReadOnlyRepository _repo;

        public IReadOnlyRepository Repo
        {
            get { return _repo; }
            set { _repo = value; }
        }

        //private readonly UnitOfWork _db = new UnitOfWork();

        //private TimeZoneInfo tzi
        //{
        //    get
        //    {
        //        var currTzi = _db.EmployeeRepository.Get(x => x.LogIn == "tzi").FirstOrDefault();
        //        if (currTzi != null) return TimeZoneInfo.FindSystemTimeZoneById(currTzi.TimeZone);
        //        return TimeZoneInfo.Local;
        //    }
        //}
        public static DateTime ClientToUTC(TimeZoneInfo tzi, DateTime d)
        {
            var userOffset = tzi.GetUtcOffset(d).TotalHours;
            var webserverOffset = TimeZoneInfo.Local.GetUtcOffset(d).TotalHours;
            var offsetD = d.AddHours(webserverOffset - userOffset);
            offsetD = offsetD.ToUniversalTime();
            return offsetD;
        }
        /// <summary>
        /// parses out time set by client to return UTC
        /// </summary>
        /// <param name="tzi"></param>
        /// <param name="d"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        public static DateTime ClientToUTC(TimeZoneInfo tzi, DateTime d, string t)
        {
            var startdate = d.Date;
            var hour = double.Parse(t.Substring(0, 2));
            var min = double.Parse(t.Substring(2, 2));
            startdate = startdate.AddHours(hour);
            startdate = startdate.AddMinutes(min);
            var userOffset = tzi.GetUtcOffset(startdate).TotalHours;
            var webserverOffset = TimeZoneInfo.Local.GetUtcOffset(startdate).TotalHours;
            var offsetD = startdate.AddHours(webserverOffset - userOffset);
            offsetD = offsetD.ToUniversalTime();
            return offsetD;
        }
        public static DateTime UTCtoClient(TimeZoneInfo tzi, DateTime utc, IReadOnlyRepository repo)
        {
            //if (_repo == null)
                _repo = repo;
            if (tzi == null)
            {
                foreach (var dr in _repo.Get<Employee>())
                {
                    if (dr.GetTimeZoneInstance() != null)
                    {
                        tzi = dr.GetTimeZoneInstance();
                    }
                }
            }
            if (tzi == null || utc == DateTime.MinValue) return DateTime.MinValue;

            var userOffset = tzi.GetUtcOffset(utc).TotalHours;
            var client = utc.AddHours(userOffset);
            return client;
        }
        public static DateTime UTCtoClient(TimeZoneInfo tzi, DateTime utc)
        {

            if (tzi == null && _repo != null)
            {
                foreach (var dr in _repo.Get<Employee>())
                {
                    if (dr.GetTimeZoneInstance() != null)
                    {
                        tzi = dr.GetTimeZoneInstance();
                    }
                }
            }
            else
            {
                // use webserver offset
                tzi = TimeZoneInfo.Local;
            }
            if (tzi == null || utc == DateTime.MinValue) return DateTime.MinValue;

            var userOffset = tzi.GetUtcOffset(utc).TotalHours;
            var client = utc.AddHours(userOffset);
            return client;
        }
    }
}