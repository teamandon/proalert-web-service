﻿namespace ProAlert.Andon.Service.Common
{
    public class Enumerations
    {
        public enum CallType
        {
            Maintenance,
            QC,
            Materials
        }

        public enum DisplayLine
        {
            Maintenance = 1,
            QC = 2,
            Materials = 3,
            Advisory = 4
        }

        public enum AMInterval
        {
            Minutes,
            Hours,
            Days,
            Weeks
        }

        public enum DisplayColors
        {
            Green,
            Yellow,
            Red
        }

        public enum Shift
        {
            First,
            Second,
            Third
        }
       
    }
}