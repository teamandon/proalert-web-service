﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Vistech.Andon.Service.Common
{
    public interface IEntityService<T> : IService where T : class
    {
        void Insert(T entity);
        void Delete(T entity);
        
        T GetById(object id);
        void Delete(object id);
        void Update(T entity);
        //IEnumerable<T> GetAll();
        void Save();

        void Undo(T entity);

        //Task SaveAsync();
        T GetOne(
            Expression<Func<T, bool>> filter = null,
            string includeProperties = null);

        Task<T> GetOneAsync(
            Expression<Func<T, bool>> filter = null,
            string includeProperties = null);

        IEnumerable<T> GetAll(
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            string includeProperties = null,
            int? skip = null,
            int? take = null);

        T GetFirst(
            Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            string includeProperties = null);

        Task<T> GetFirstAsync(
            Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            string includeProperties = null);

        IEnumerable<T> Get(
            Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            string includeProperties = null,
            int? skip = null,
            int? take = null);

        Task<IEnumerable<T>> GetAsync(
            Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            string includeProperties = null,
            int? skip = null,
            int? take = null);
    }

}
