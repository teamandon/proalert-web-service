﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using ProAlert.Andon.Service.Interfaces;


namespace ProAlert.Andon.Service.Common
{
    public class BstBase
    {
        private IReadOnlyRepository _repository;

        public IReadOnlyRepository Repository
        {
            get { return _repository; }
            set { _repository = value; }
        }

        public DataTable ConvertToDataTable<T>(IList<T> data)
        {
            var properties =
               TypeDescriptor.GetProperties(typeof(T));
            var table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                var row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }

        public DataTable ConvertToDataTableWithTZ<T>(IList<T> data)
        {
            var properties =
               TypeDescriptor.GetProperties(typeof(T));
            var table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                var row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    row[prop.Name] = prop.GetValue(item) == null ? DBNull.Value : (prop.PropertyType == typeof(DateTime) || prop.PropertyType == typeof(DateTime?)) ? timeconversions.UTCtoClient(null, (DateTime)prop.GetValue(item), _repository) : prop.GetValue(item);
                }
                table.Rows.Add(row);
            }
            return table;
        }
    }
}
