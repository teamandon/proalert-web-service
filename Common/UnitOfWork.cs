﻿using System;
using System.Linq;
using Vistech.Andon.Model.Models;
using Vistech.Andon.Service.ViewModels;

namespace Vistech.Andon.Service.Common
{
    public class UnitOfWork : IDisposable
    {
        private AndonVistechContext context = new AndonVistechContext();
        private IAppErrorsService _appErrorEntityService;
        //private GenericRepository<AppError> _appErrorRepository;
        private GenericRepository<AudioFile> _audioFileRepository;
        private GenericRepository<AutoMessage> _autoMessageRepository;
        private GenericRepository<Call> _callRepository;
        private GenericRepository<CallLog> _callLogRepository;
        private GenericRepository<Cycle> _cycleRepository;
        private GenericRepository<CycleHistory> _cycleHistoryRepository;
        private GenericRepository<CycleSummary> _cycleSummaryRepository;
        private GenericRepository<Email> _emailRepository;
        private GenericRepository<Employee> _employeeRepository;
        private GenericRepository<Issue> _issueRepository;
        private GenericRepository<LastStage> _lastStageRepository;
        private GenericRepository<PlannedDt> _plannedDtRepository;
        private GenericRepository<PlannedDtLog> _plannedDtLogRepository;
        private GenericRepository<Position> _positionRepository;
        private GenericRepository<Product> _productRepository;
        private GenericRepository<ProductWCStatsHistory> _productWcStatsHistoryRepository;
        private GenericRepository<ProductWorkCenterStat> _productWcStatRepository;
        private GenericRepository<RejectCode> _rejectCodeRepository;
        private GenericRepository<Scrap> _scrapRepository;
        private GenericRepository<ScrapType> _scrapTypeRepository;
        private GenericRepository<SubCategory> _subCategoryRepository;
        private GenericRepository<Timesegment> _timesegmentRepository;
        private GenericRepository<TzConversion> _tzConversionRepository;
        private GenericRepository<WCProductTimeline> _tlRepository;
        private GenericRepository<WCProductTimelineHistory> _tlHistRepository;
        private GenericRepository<WorkCenter> _workCenterRepository;
        private GenericRepository<WorkCenterCall> _workCenterCallRepository;

        public EntityService<AppError> AppErrorService
        {
            get
            {
                if (this._appErrorEntityService == null)
                {
                    this._appErrorEntityService = new EntityService<AppError>(context);
                }
                return _appErrorEntityService;
            }
        }
        //public GenericRepository<AppError> AppErrorRepository
        //{
        //    get
        //    {
        //        if (this._appErrorRepository == null)
        //        {
        //            this._appErrorRepository = new GenericRepository<AppError>(context);
        //        }
        //        return _appErrorRepository;
        //    }
        //}

        public GenericRepository<AudioFile> AudioFileRepository
        {
            get
            {
                if (this._audioFileRepository == null)
                {
                    this._audioFileRepository = new GenericRepository<AudioFile>(context);
                }
                return _audioFileRepository;
            }
        }

        public GenericRepository<AutoMessage> AutoMessageRepository
        {
            get
            {
                if (this._autoMessageRepository == null)
                {
                    this._autoMessageRepository = new GenericRepository<AutoMessage>(context);
                }
                return _autoMessageRepository;
            }
        }

        public GenericRepository<Call> CallRepository
        {
            get
            {
                if (this._callRepository == null)
                {
                    this._callRepository = new GenericRepository<Call>(context);
                }
                return _callRepository;
            }
        }

        public GenericRepository<CallLog> CallLogRepository
        {
            get
            {
                if (this._callLogRepository == null)
                {
                    this._callLogRepository = new GenericRepository<CallLog>(context);
                }
                return _callLogRepository;
            }
        }

        public GenericRepository<Cycle> CycleRepository
        {
            get
            {
                if (this._cycleRepository == null)
                {
                    this._cycleRepository = new GenericRepository<Cycle>(context);
                }
                return _cycleRepository;
            }
        }

        public GenericRepository<CycleHistory> CycleHistoryRepository
        {
            get
            {
                if (this._cycleHistoryRepository == null)
                {
                    this._cycleHistoryRepository = new GenericRepository<CycleHistory>(context);
                }
                return _cycleHistoryRepository;
            }
        }

        public GenericRepository<CycleSummary> CycleSummaryRepository
        {
            get
            {
                if (this._cycleSummaryRepository == null)
                {
                    this._cycleSummaryRepository = new GenericRepository<CycleSummary>(context);
                }
                return _cycleSummaryRepository;
            }
        }

        public GenericRepository<Email> EmailRespository
        {
            get
            {
                if (this._emailRepository == null)
                {
                    this._emailRepository = new GenericRepository<Email>(context);
                }
                return _emailRepository;
            }
        }

        public GenericRepository<Employee> EmployeeRepository
        {
            get
            {
                if (this._employeeRepository == null)
                {
                    this._employeeRepository = new GenericRepository<Employee>(context);
                }
                return _employeeRepository;
            }
        }

        public GenericRepository<Issue> IssueRepository
        {
            get
            {
                if (this._issueRepository == null)
                {
                    this._issueRepository = new GenericRepository<Issue>(context);
                }
                return _issueRepository;
            }
        }

        public GenericRepository<LastStage> LastStageRepository
        {
            get
            {
                if (this._lastStageRepository == null)
                {
                    this._lastStageRepository = new GenericRepository<LastStage>(context);
                }
                return _lastStageRepository;
            }
        }

        public GenericRepository<PlannedDt> PlannedDtRepository
        {
            get
            {
                if (this._plannedDtRepository == null)
                {
                    this._plannedDtRepository = new GenericRepository<PlannedDt>(context);
                }
                return _plannedDtRepository;
            }
        }

        public GenericRepository<PlannedDtLog> PlannedDtLogRepository
        {
            get
            {
                if (this._plannedDtLogRepository == null)
                {
                    this._plannedDtLogRepository = new GenericRepository<PlannedDtLog>(context);
                }
                return _plannedDtLogRepository;
            }
        }

        public GenericRepository<Position> PositionRepository
        {
            get
            {
                if (this._positionRepository == null)
                {
                    this._positionRepository = new GenericRepository<Position>(context);
                }
                return _positionRepository;
            }
        }

        public GenericRepository<Product> ProductRepository
        {
            get
            {
                if (this._productRepository == null)
                {
                    this._productRepository = new GenericRepository<Product>(context);
                }
                return _productRepository;
            }
        }

        public GenericRepository<ProductWCStatsHistory> ProductWcStatHistoryRepository
        {
            get
            {
                if (this._productWcStatsHistoryRepository == null)
                {
                    this._productWcStatsHistoryRepository = new GenericRepository<ProductWCStatsHistory>(context);
                }
                return _productWcStatsHistoryRepository;
            }
        }

        public GenericRepository<ProductWorkCenterStat> ProductWcStatRepository
        {
            get
            {
                if (this._productWcStatRepository == null)
                {
                    this._productWcStatRepository = new GenericRepository<ProductWorkCenterStat>(context);
                }
                return _productWcStatRepository;
            }
        }

        public GenericRepository<RejectCode> RejectCodeRepository
        {
            get
            {
                if (this._rejectCodeRepository == null)
                {
                    this._rejectCodeRepository = new GenericRepository<RejectCode>(context);
                }
                return _rejectCodeRepository;
            }
        }

        public GenericRepository<Scrap> ScrapRepository
        {
            get
            {
                if (this._scrapRepository == null)
                {
                    this._scrapRepository = new GenericRepository<Scrap>(context);
                }
                return _scrapRepository;
            }
        }

        public GenericRepository<ScrapType> ScrapTypeRepository
        {
            get
            {
                if (this._scrapTypeRepository == null)
                {
                    this._scrapTypeRepository = new GenericRepository<ScrapType>(context);
                }
                return _scrapTypeRepository;
            }
        }

        public GenericRepository<SubCategory> SubCategoryRepository
        {
            get
            {
                if (this._subCategoryRepository == null)
                {
                    this._subCategoryRepository = new GenericRepository<SubCategory>(context);
                }
                return _subCategoryRepository;
            }
        }

        public GenericRepository<Timesegment> TimesegmentRepository
        {
            get
            {
                if (this._timesegmentRepository == null)
                {
                    this._timesegmentRepository = new GenericRepository<Timesegment>(context);
                }
                return _timesegmentRepository;
            }
        }

        public GenericRepository<TzConversion> TzConversionRepository
        {
            get
            {
                if (this._tzConversionRepository == null)
                {
                    this._tzConversionRepository = new GenericRepository<TzConversion>(context);
                }
                return _tzConversionRepository;
            }
        }

        public GenericRepository<WCProductTimeline> TlRepository
        {
            get
            {

                if (this._tlRepository == null)
                {
                    this._tlRepository = new GenericRepository<WCProductTimeline>(context);
                }

                return _tlRepository;
            }
        }

        public GenericRepository<WCProductTimelineHistory> TlHistRepository
        {
            get
            {

                if (this._tlHistRepository == null)
                {
                    this._tlHistRepository = new GenericRepository<WCProductTimelineHistory>(context);
                }

                return _tlHistRepository;
            }
        }

        public GenericRepository<WorkCenter> WorkCenterRepository
        {
            get
            {
                if (this._workCenterRepository == null)
                {
                    this._workCenterRepository = new GenericRepository<WorkCenter>(context);
                }
                return _workCenterRepository;
            }
        }

        public GenericRepository<WorkCenterCall> WorkCenterCallRepository
        {
            get
            {
                if (this._workCenterCallRepository == null)
                {
                    this._workCenterCallRepository = new GenericRepository<WorkCenterCall>(context);
                }
                return _workCenterCallRepository;
            }
        }

        public bool Proxies
        {
            get { return context.Configuration.ProxyCreationEnabled; } 
            
            set
            {
                context.Configuration.ProxyCreationEnabled = value;
                //context.Configuration.AutoDetectChangesEnabled = value;
            }
        }

        public void ExecuteSQL(string sql)
        {
            context.Database.ExecuteSqlCommand(sql);
        }

        public DtRpts GetDtRpt()
        {
            var rpt = new DtRpts {List = context.Database.SqlQuery<DtRpt>("spGetCallLogRpt").ToList()};
            return rpt;
        }

        public OeeRpts GetOeeRpts()
        {
            var rpt = new OeeRpts { List = context.Database.SqlQuery<OeeRpt>("spGetOeeRpt").ToList() };
            return rpt;
        }

        public void Save()
        {
            //var objectContext = ((IObjectContextAdapter)context).ObjectContext;
            //objectContext.Refresh(RefreshMode.ClientWins, TlRepository);
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}