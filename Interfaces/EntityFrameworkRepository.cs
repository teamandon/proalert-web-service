﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ProAlert.Andon.Service.Models;
using ProAlert.Andon.Service.ViewModels;

namespace ProAlert.Andon.Service.Interfaces
{
    public class EntityFrameworkRepository<TContext> : EfReadOnlyRepository.EntityFrameworkReadOnlyRepository<TContext>, IRepository where TContext : DbContext
    {
        public EntityFrameworkRepository(TContext context)
            : base(context)
        {
            //context.Configuration.ProxyCreationEnabled = false;
            //context.Configuration.LazyLoadingEnabled = false;
        }

        public virtual void InsertError(AppError entity)
        {
            context.Set<AppError>().Add(entity);
        }

        public virtual Mod2SqlStatus GetMod2SqlStatus(Mod2SqlStatus entity)
        {
            return context.Set<Mod2SqlStatus>().Find(entity.WorkCenterId);
        }
        public virtual void InsertMod2SqlStatus(Mod2SqlStatus entity)
        { 
            context.Set<Mod2SqlStatus>().Add(entity);
        }

        public virtual void UpdateMod2SqlStatus(Mod2SqlStatus entity)
        {
            context.Set<Mod2SqlStatus>().Attach(entity);
            context.Entry(entity).State = EntityState.Modified;
        }

        public virtual void InsertDdVmJson(DdVmJson entity)
        {
            context.Set<DdVmJson>().Add(entity);
        }

        public virtual void UpdateDdVmJson(DdVmJson entity)
        {
            context.Set<DdVmJson>().Attach(entity);
            context.Entry(entity).State = EntityState.Modified;
        }

        /// <summary>
        /// Get by TLID
        /// </summary>
        /// <param name="tlId"></param>
        /// <returns></returns>
        public virtual DdVmJson GetDdVmJson(int tlId)
        {
            //DdVmJson entity = null;
            try
            {
                var entity = context.Set<DdVmJson>().Single(x => x.TlId == tlId);
                return entity;
            }
            catch (DbEntityValidationException e)
            {
                ThrowEnhancedValidationException(e);
            }
            return null;
        }
        public virtual void Create<TEntity>(TEntity entity, string createdBy = null)
            where TEntity : class, IEntity
        {
            entity.CreatedDate = DateTime.UtcNow;
            entity.CreatedBy = createdBy;
            context.Set<TEntity>().Add(entity);
        }

        public virtual void Update<TEntity>(TEntity entity, string modifiedBy = null)
            where TEntity : class, IEntity
        {
            entity.ModifiedDate = DateTime.UtcNow;
            entity.ModifiedBy = modifiedBy;
            context.Set<TEntity>().Attach(entity);
            context.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Delete<TEntity>(object id)
            where TEntity : class, IEntity
        {
            var entity = context.Set<TEntity>().Find(id);
            Delete(entity);
        }

        public virtual void Delete<TEntity>(TEntity entity)
            where TEntity : class, IEntity
        {
            var dbSet = context.Set<TEntity>();
            if (context.Entry(entity).State == EntityState.Detached)
            {
                dbSet.Attach(entity);
            }
            dbSet.Remove(entity);
        }

        public void ExecuteSQL(string sql)
        {
            context.Database.ExecuteSqlCommand(sql);
        }

        public DtRpts GetDtRpt()
        {
            var rpt = new DtRpts { List = context.Database.SqlQuery<DtRpt>("spGetCallLogRpt").ToList() };
            return rpt;
        }

        public OeeRpts GetOeeRpts()
        {
            var rpt = new OeeRpts { List = context.Database.SqlQuery<OeeRpt>("spGetOeeRpt").ToList() };
            return rpt;
        }

        public ScrapRpts GetScrapRpts(List<string> tsList)
        {
            var p = new SqlParameter("@tsid", string.Join(",", tsList.ToArray()));
            var rpt = new ScrapRpts {List = context.Database.SqlQuery<ScrapRpt>("exec spGetScrapByTsId @tsid", p).ToList()};
            return rpt;
        }
        public virtual void Save()
        {
            try
            {
                context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                ThrowEnhancedValidationException(e);
            }
        }

        public virtual Task SaveAsync()
        {
            try
            {
                return context.SaveChangesAsync();
            }
            catch (DbEntityValidationException e)
            {
                ThrowEnhancedValidationException(e);
            }

            return Task.FromResult(0);
        }

        protected virtual void ThrowEnhancedValidationException(DbEntityValidationException e)
        {
            var errorMessages = e.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

            var fullErrorMessage = string.Join("; ", errorMessages);
            var exceptionMessage = string.Concat(e.Message, " The validation errors are: ", fullErrorMessage);
            throw new DbEntityValidationException(exceptionMessage, e.EntityValidationErrors);
        }


    }

}
