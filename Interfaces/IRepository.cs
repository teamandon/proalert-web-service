﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ProAlert.Andon.Service.Models;
using ProAlert.Andon.Service.ViewModels;

namespace ProAlert.Andon.Service.Interfaces
{
    public interface IRepository : IReadOnlyRepository
    {
        void InsertError(AppError entity);
        void InsertMod2SqlStatus(Mod2SqlStatus entity);

        Mod2SqlStatus GetMod2SqlStatus(Mod2SqlStatus entity);
        void UpdateMod2SqlStatus(Mod2SqlStatus entity);

        void InsertDdVmJson(DdVmJson entity);
        void UpdateDdVmJson(DdVmJson entity);
        /// <summary>
        /// Get by WcProductTimeLine ID
        /// </summary>
        /// <param name="tlId"></param>
        /// <returns></returns>
        DdVmJson GetDdVmJson(int tlId);
        /// <summary>
        /// Create by entity: auto fill Create Date
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entity"></param>
        /// <param name="createdBy"></param>
        void Create<TEntity>(TEntity entity, string createdBy = null)
            where TEntity : class, IEntity;
        /// <summary>
        /// update by entity: auto fill Modified Date
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entity"></param>
        /// <param name="modifiedBy"></param>
        void Update<TEntity>(TEntity entity, string modifiedBy = null)
            where TEntity : class, IEntity;

        void Delete<TEntity>(object id)
            where TEntity : class, IEntity;

        void Delete<TEntity>(TEntity entity)
            where TEntity : class, IEntity;

        DtRpts GetDtRpt();

        OeeRpts GetOeeRpts();
        /// <summary>
        /// returns list of scrap based on passed timesegent ids
        /// </summary>
        /// <param name="tsList"></param>
        /// <returns></returns>
        ScrapRpts GetScrapRpts(List<string> tsList);

        void ExecuteSQL(string sql);

        void Save();

        Task SaveAsync();
    }
}
