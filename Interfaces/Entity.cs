﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ProAlert.Andon.Service.Common;

namespace ProAlert.Andon.Service.Interfaces
{
    //public interface IModifiableEntity
    //{
    //    string Name { get; set; }
    //}

    public interface IEntity<T> : IEntity
    {
        new T Id { get; set; }
    }

    public interface IEntity //: IModifiableEntity
    {
        object Id { get; set; }
        DateTime CreatedDate { get; set; }
        DateTime? ModifiedDate { get; set; }
        string CreatedBy { get; set; }
        string ModifiedBy { get; set; }
        string MacId { get; set; }
        byte[] RowVersion { get; set; }
    }



    public abstract class Entity<T> : IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public T Id { get; set; }
        object IEntity.Id
        {
            get { return this.Id; }
            set { this.Id = (T)value; }
        }
        //public string Name { get; set; }

        private DateTime? _createdDate;
        [DataType(DataType.DateTime)]
        public DateTime CreatedDate
        {
            get { return _createdDate ?? DateTime.UtcNow; }
            set { _createdDate = value; }
        }

        [DataType(DataType.DateTime)]
        public DateTime? ModifiedDate { get; set; }

        [Display(Name="Created By"), MaxLength(50)]
        public string CreatedBy { get; set; }
        [Display(Name="Modified By"), MaxLength(50)]
        public string ModifiedBy { get; set; }
        [Display(Name="MAC Address"), MaxLength(50)]
        public string MacId { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }

        public DateTime? DispModifiedDate(TimeZoneInfo tzi)
        {
            if (ModifiedDate == null) return null;
            return timeconversions.UTCtoClient(tzi, ModifiedDate ?? DateTime.Now);
        }
    }
}