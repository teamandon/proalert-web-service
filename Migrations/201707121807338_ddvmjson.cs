namespace ProAlert.Andon.Service.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ddvmjson : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DdVmJson",
                c => new
                    {
                        TlId = c.Int(nullable: false),
                        Json = c.String(),
                    })
                .PrimaryKey(t => t.TlId)
                .ForeignKey("dbo.WCProductTimeline", t => t.TlId)
                .Index(t => t.TlId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DdVmJson", "TlId", "dbo.WCProductTimeline");
            DropIndex("dbo.DdVmJson", new[] { "TlId" });
            DropTable("dbo.DdVmJson");
        }
    }
}
