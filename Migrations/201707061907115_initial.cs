namespace ProAlert.Andon.Service.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AppError",
                c => new
                    {
                        AppErrorID = c.Int(nullable: false, identity: true),
                        ErrorMsg = c.String(),
                        ErrorDT = c.DateTime(nullable: false),
                        UserID = c.Int(),
                        Source = c.String(),
                    })
                .PrimaryKey(t => t.AppErrorID);
            
            CreateTable(
                "dbo.AudioFile",
                c => new
                    {
                        AudioFileID = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 50),
                        Path = c.String(maxLength: 256),
                        Sound = c.Binary(),
                        RepeatCnt = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 50),
                        ModifiedBy = c.String(maxLength: 50),
                        MacId = c.String(maxLength: 50),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.AudioFileID);
            
            CreateTable(
                "dbo.AutoMessage",
                c => new
                    {
                        AutoMessageID = c.Int(nullable: false, identity: true),
                        Message = c.String(maxLength: 256),
                        AudioFileID = c.Int(nullable: false),
                        Duration = c.Int(nullable: false),
                        DisplayLine = c.Int(),
                        DisplayColors = c.Int(),
                        Start = c.DateTime(nullable: false),
                        Reoccurring = c.Boolean(nullable: false),
                        Interval = c.Int(),
                        IntervalQty = c.Int(),
                        EmployeeID = c.Int(),
                        Image = c.Binary(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 50),
                        ModifiedBy = c.String(maxLength: 50),
                        MacId = c.String(maxLength: 50),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.AutoMessageID)
                .ForeignKey("dbo.AudioFile", t => t.AudioFileID, cascadeDelete: true)
                .ForeignKey("dbo.Employee", t => t.EmployeeID)
                .Index(t => t.AudioFileID)
                .Index(t => t.EmployeeID);
            
            CreateTable(
                "dbo.Employee",
                c => new
                    {
                        EmployeeID = c.Int(nullable: false, identity: true),
                        PositionID = c.Int(),
                        HireDate = c.DateTime(nullable: false),
                        TerminationDate = c.DateTime(),
                        LogIn = c.String(maxLength: 50),
                        Pin = c.Int(nullable: false),
                        TimeZone = c.String(maxLength: 50),
                        FirstName = c.String(maxLength: 50),
                        LastName = c.String(maxLength: 50),
                        Cell = c.String(),
                        Email = c.String(),
                        Picture = c.Binary(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 50),
                        ModifiedBy = c.String(maxLength: 50),
                        MacId = c.String(maxLength: 50),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.EmployeeID)
                .ForeignKey("dbo.Position", t => t.PositionID)
                .Index(t => t.PositionID);
            
            CreateTable(
                "dbo.Position",
                c => new
                    {
                        PositionID = c.Int(nullable: false, identity: true),
                        Title = c.String(maxLength: 50),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 50),
                        ModifiedBy = c.String(maxLength: 50),
                        MacId = c.String(maxLength: 50),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.PositionID);
            
            CreateTable(
                "dbo.BOM",
                c => new
                    {
                        ProductID = c.Int(nullable: false),
                        ComponentID = c.Int(nullable: false),
                        Quantity = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 50),
                        ModifiedBy = c.String(maxLength: 50),
                        MacId = c.String(maxLength: 50),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Product_Id = c.Int(),
                        Product1_Id = c.Int(),
                    })
                .PrimaryKey(t => new { t.ProductID, t.ComponentID })
                .ForeignKey("dbo.Product", t => t.Product_Id)
                .ForeignKey("dbo.Product", t => t.Product1_Id)
                .Index(t => t.Product_Id)
                .Index(t => t.Product1_Id);
            
            CreateTable(
                "dbo.Product",
                c => new
                    {
                        ProductID = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 50),
                        Description = c.String(maxLength: 256),
                        ImagePath = c.String(maxLength: 256),
                        Material = c.String(maxLength: 256),
                        StandardPack = c.Int(nullable: false),
                        Image = c.Binary(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 50),
                        ModifiedBy = c.String(maxLength: 50),
                        MacId = c.String(maxLength: 50),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.ProductID);
            
            CreateTable(
                "dbo.ProductWorkCenterStat",
                c => new
                    {
                        ProductWorkCenterStatID = c.Int(nullable: false, identity: true),
                        WorkCenterID = c.Int(nullable: false),
                        ProductID = c.Int(nullable: false),
                        CyclesPerHour = c.Decimal(nullable: false, precision: 18, scale: 5),
                        UnitsPerCycle = c.Decimal(nullable: false, precision: 18, scale: 5),
                        Yield = c.Decimal(nullable: false, precision: 18, scale: 5),
                        SetUpTime = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Available = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Performance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Quality = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AvailableCap = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PerformanceCap = c.Decimal(nullable: false, precision: 18, scale: 2),
                        QualityCap = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 50),
                        ModifiedBy = c.String(maxLength: 50),
                        MacId = c.String(maxLength: 50),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.ProductWorkCenterStatID)
                .ForeignKey("dbo.Product", t => t.ProductID, cascadeDelete: true)
                .ForeignKey("dbo.WorkCenter", t => t.WorkCenterID, cascadeDelete: true)
                .Index(t => t.WorkCenterID)
                .Index(t => t.ProductID);
            
            CreateTable(
                "dbo.WorkCenter",
                c => new
                    {
                        WorkCenterID = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 50),
                        Description = c.String(maxLength: 256),
                        Down = c.Boolean(),
                        Planned = c.Boolean(),
                        OEEStartTime = c.DateTime(nullable: false),
                        ProductID = c.Int(),
                        WorkCenterGUIOptionsID = c.Int(),
                        GuiRunning = c.Boolean(nullable: false),
                        MachineRunning = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 50),
                        ModifiedBy = c.String(maxLength: 50),
                        MacId = c.String(maxLength: 50),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.WorkCenterID)
                .ForeignKey("dbo.Product", t => t.ProductID)
                .ForeignKey("dbo.WorkCenterGUIOptions", t => t.WorkCenterGUIOptionsID)
                .Index(t => t.ProductID)
                .Index(t => t.WorkCenterGUIOptionsID);
            
            CreateTable(
                "dbo.WorkCenterCall",
                c => new
                    {
                        WorkCenterCallID = c.Int(nullable: false, identity: true),
                        WorkCenterID = c.Int(nullable: false),
                        CallID = c.Int(nullable: false),
                        OrderID = c.Int(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 50),
                        ModifiedBy = c.String(maxLength: 50),
                        MacId = c.String(maxLength: 50),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.WorkCenterCallID)
                .ForeignKey("dbo.Call", t => t.CallID, cascadeDelete: true)
                .ForeignKey("dbo.WorkCenter", t => t.WorkCenterID, cascadeDelete: true)
                .Index(t => t.WorkCenterID)
                .Index(t => t.CallID);
            
            CreateTable(
                "dbo.Call",
                c => new
                    {
                        CallID = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 50),
                        Type = c.Int(),
                        DT = c.Boolean(nullable: false),
                        Description = c.String(maxLength: 256),
                        AudioFileID = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 50),
                        ModifiedBy = c.String(maxLength: 50),
                        MacId = c.String(maxLength: 50),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.CallID)
                .ForeignKey("dbo.AudioFile", t => t.AudioFileID, cascadeDelete: true)
                .Index(t => t.AudioFileID);
            
            CreateTable(
                "dbo.WorkCenterGUIOptions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ManualCycles = c.Boolean(),
                        SingleScrap = c.Boolean(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 50),
                        ModifiedBy = c.String(maxLength: 50),
                        MacId = c.String(maxLength: 50),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CallLogHistory",
                c => new
                    {
                        CallLogHistoryID = c.Int(nullable: false, identity: true),
                        CallLogID = c.Int(nullable: false),
                        CallID = c.Int(nullable: false),
                        InitiateDt = c.DateTime(nullable: false),
                        ResponseDt = c.DateTime(),
                        ResolveDt = c.DateTime(),
                        WorkCenterId = c.Int(nullable: false),
                        EmployeeID = c.Int(),
                        Planned = c.Boolean(nullable: false),
                        IssueID = c.Int(),
                        SubCategoryID = c.Int(),
                        Resolution = c.String(),
                        OperatorNotes = c.String(),
                        ProductID = c.Int(),
                        SignOffDT = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 50),
                        ModifiedBy = c.String(maxLength: 50),
                        MacId = c.String(maxLength: 50),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.CallLogHistoryID);
            
            CreateTable(
                "dbo.CallLog",
                c => new
                    {
                        CallLogID = c.Int(nullable: false, identity: true),
                        CallID = c.Int(nullable: false),
                        InitiateDt = c.DateTime(nullable: false),
                        ResponseDt = c.DateTime(),
                        ResolveDt = c.DateTime(),
                        WorkCenterId = c.Int(nullable: false),
                        EmployeeID = c.Int(),
                        Planned = c.Boolean(nullable: false),
                        IssueID = c.Int(),
                        SubCategoryID = c.Int(),
                        Resolution = c.String(),
                        OperatorNotes = c.String(),
                        ProductID = c.Int(),
                        SignOffDT = c.DateTime(),
                        TimesegmentID = c.Int(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 50),
                        ModifiedBy = c.String(maxLength: 50),
                        MacId = c.String(maxLength: 50),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.CallLogID)
                .ForeignKey("dbo.Call", t => t.CallID, cascadeDelete: true)
                .ForeignKey("dbo.Employee", t => t.EmployeeID)
                .ForeignKey("dbo.Issue", t => t.IssueID)
                .ForeignKey("dbo.Product", t => t.ProductID)
                .ForeignKey("dbo.SubCategory", t => t.SubCategoryID)
                .ForeignKey("dbo.Timesegment", t => t.TimesegmentID)
                .ForeignKey("dbo.WorkCenter", t => t.WorkCenterId, cascadeDelete: true)
                .Index(t => t.CallID)
                .Index(t => t.WorkCenterId)
                .Index(t => t.EmployeeID)
                .Index(t => t.IssueID)
                .Index(t => t.SubCategoryID)
                .Index(t => t.ProductID)
                .Index(t => t.TimesegmentID);
            
            CreateTable(
                "dbo.Issue",
                c => new
                    {
                        IssueID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        Description = c.String(maxLength: 256),
                        Material = c.Boolean(nullable: false),
                        Manufacturing = c.Boolean(nullable: false),
                        Rework = c.Boolean(nullable: false),
                        DT = c.Boolean(nullable: false),
                        SubCategoryID = c.Int(),
                        DispOrder = c.Int(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 50),
                        ModifiedBy = c.String(maxLength: 50),
                        MacId = c.String(maxLength: 50),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.IssueID)
                .ForeignKey("dbo.SubCategory", t => t.SubCategoryID)
                .Index(t => t.SubCategoryID);
            
            CreateTable(
                "dbo.SubCategory",
                c => new
                    {
                        SubCategoryID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        Description = c.String(maxLength: 256),
                        DispOrder = c.Int(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 50),
                        ModifiedBy = c.String(maxLength: 50),
                        MacId = c.String(maxLength: 50),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.SubCategoryID);
            
            CreateTable(
                "dbo.Timesegment",
                c => new
                    {
                        TimesegmentID = c.Int(nullable: false, identity: true),
                        WCProductTimelineID = c.Int(nullable: false),
                        CycleSummaryID = c.Int(),
                        Start = c.DateTime(nullable: false),
                        Stop = c.DateTime(),
                        DT = c.Boolean(),
                        plannedDT = c.Boolean(),
                        ProductChangeOver = c.Boolean(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 50),
                        ModifiedBy = c.String(maxLength: 50),
                        MacId = c.String(maxLength: 50),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.TimesegmentID)
                .ForeignKey("dbo.CycleSummary", t => t.CycleSummaryID)
                .ForeignKey("dbo.WCProductTimeline", t => t.WCProductTimelineID, cascadeDelete: true)
                .Index(t => t.WCProductTimelineID)
                .Index(t => t.CycleSummaryID);
            
            CreateTable(
                "dbo.CycleSummary",
                c => new
                    {
                        CycleSummaryID = c.Int(nullable: false, identity: true),
                        Count = c.Int(nullable: false),
                        LastCycleHistoryID = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 50),
                        ModifiedBy = c.String(maxLength: 50),
                        MacId = c.String(maxLength: 50),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.CycleSummaryID);
            
            CreateTable(
                "dbo.PlannedDtLog",
                c => new
                    {
                        PlannedDtLogID = c.Int(nullable: false, identity: true),
                        PlannedDtID = c.Int(nullable: false),
                        Start = c.DateTime(nullable: false),
                        Stop = c.DateTime(),
                        Notes = c.String(maxLength: 256),
                        EmployeeID = c.Int(),
                        WorkCenterID = c.Int(nullable: false),
                        ProductID = c.Int(),
                        TimesegmentID = c.Int(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 50),
                        ModifiedBy = c.String(maxLength: 50),
                        MacId = c.String(maxLength: 50),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.PlannedDtLogID)
                .ForeignKey("dbo.Employee", t => t.EmployeeID)
                .ForeignKey("dbo.PlannedDt", t => t.PlannedDtID, cascadeDelete: true)
                .ForeignKey("dbo.Product", t => t.ProductID)
                .ForeignKey("dbo.Timesegment", t => t.TimesegmentID)
                .ForeignKey("dbo.WorkCenter", t => t.WorkCenterID, cascadeDelete: true)
                .Index(t => t.PlannedDtID)
                .Index(t => t.EmployeeID)
                .Index(t => t.WorkCenterID)
                .Index(t => t.ProductID)
                .Index(t => t.TimesegmentID);
            
            CreateTable(
                "dbo.PlannedDt",
                c => new
                    {
                        PlannedDtID = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 50),
                        Description = c.String(maxLength: 256),
                        DisplayOrder = c.Int(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 50),
                        ModifiedBy = c.String(maxLength: 50),
                        MacId = c.String(maxLength: 50),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.PlannedDtID);
            
            CreateTable(
                "dbo.Scrap",
                c => new
                    {
                        ScrapID = c.Int(nullable: false, identity: true),
                        WorkCenterID = c.Int(),
                        ProductID = c.Int(),
                        LastStageID = c.Int(),
                        RejectCodeID = c.Int(),
                        ScrapTypeID = c.Int(),
                        ScrapDT = c.DateTime(nullable: false),
                        ScrapCount = c.Int(nullable: false),
                        Notes = c.String(maxLength: 500),
                        DefectLocation = c.String(maxLength: 256),
                        SignoffDT = c.DateTime(),
                        EmployeeID = c.Int(),
                        TimesegmentID = c.Int(),
                        ScrapParentID = c.Int(),
                        Allocated = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 50),
                        ModifiedBy = c.String(maxLength: 50),
                        MacId = c.String(maxLength: 50),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.ScrapID)
                .ForeignKey("dbo.Employee", t => t.EmployeeID)
                .ForeignKey("dbo.LastStage", t => t.LastStageID)
                .ForeignKey("dbo.Product", t => t.ProductID)
                .ForeignKey("dbo.RejectCode", t => t.RejectCodeID)
                .ForeignKey("dbo.Scrap", t => t.ScrapParentID)
                .ForeignKey("dbo.ScrapType", t => t.ScrapTypeID)
                .ForeignKey("dbo.WorkCenter", t => t.WorkCenterID)
                .ForeignKey("dbo.Timesegment", t => t.TimesegmentID)
                .Index(t => t.WorkCenterID)
                .Index(t => t.ProductID)
                .Index(t => t.LastStageID)
                .Index(t => t.RejectCodeID)
                .Index(t => t.ScrapTypeID)
                .Index(t => t.EmployeeID)
                .Index(t => t.TimesegmentID)
                .Index(t => t.ScrapParentID);
            
            CreateTable(
                "dbo.LastStage",
                c => new
                    {
                        LastStageID = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 50),
                        Description = c.String(maxLength: 256),
                        DispOrder = c.Int(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 50),
                        ModifiedBy = c.String(maxLength: 50),
                        MacId = c.String(maxLength: 50),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.LastStageID);
            
            CreateTable(
                "dbo.RejectCode",
                c => new
                    {
                        RejectCodeID = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 50),
                        Description = c.String(maxLength: 256),
                        DispOrder = c.Int(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 50),
                        ModifiedBy = c.String(maxLength: 50),
                        MacId = c.String(maxLength: 50),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.RejectCodeID);
            
            CreateTable(
                "dbo.ScrapType",
                c => new
                    {
                        ScrapTypeID = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 50),
                        Description = c.String(maxLength: 256),
                        Default = c.Boolean(nullable: false),
                        DispOrder = c.Int(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 50),
                        ModifiedBy = c.String(maxLength: 50),
                        MacId = c.String(maxLength: 50),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.ScrapTypeID);
            
            CreateTable(
                "dbo.WCProductTimeline",
                c => new
                    {
                        WCProductTimelineID = c.Int(nullable: false, identity: true),
                        ShiftLogId = c.Int(nullable: false),
                        WorkCenterID = c.Int(),
                        ProductID = c.Int(),
                        Start = c.DateTime(nullable: false),
                        Stop = c.DateTime(),
                        TotalDT = c.Decimal(nullable: false, precision: 18, scale: 5),
                        TotalPlannedDT = c.Decimal(nullable: false, precision: 18, scale: 5),
                        PlannedProduction = c.Decimal(nullable: false, precision: 18, scale: 5),
                        OperationTime = c.Decimal(nullable: false, precision: 18, scale: 5),
                        ProductWcStatsHistoryID = c.Int(),
                        Current = c.Boolean(nullable: false),
                        Availability = c.Decimal(nullable: false, precision: 18, scale: 5),
                        Performance = c.Decimal(nullable: false, precision: 18, scale: 5),
                        Quality = c.Decimal(nullable: false, precision: 18, scale: 5),
                        OEE = c.Decimal(nullable: false, precision: 18, scale: 5),
                        LastDTReason = c.String(),
                        WCProduct = c.String(),
                        Down = c.Boolean(nullable: false),
                        WCPlanned = c.Boolean(nullable: false),
                        Mfg = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Matl = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Rework = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Other = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ActualPieces = c.Decimal(nullable: false, precision: 18, scale: 5),
                        ActualCycles = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 50),
                        ModifiedBy = c.String(maxLength: 50),
                        MacId = c.String(maxLength: 50),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.WCProductTimelineID)
                .ForeignKey("dbo.Product", t => t.ProductID)
                .ForeignKey("dbo.ProductWCStatsHistory", t => t.ProductWcStatsHistoryID)
                .ForeignKey("dbo.ShiftLogs", t => t.ShiftLogId, cascadeDelete: true)
                .ForeignKey("dbo.WorkCenter", t => t.WorkCenterID)
                .Index(t => t.ShiftLogId)
                .Index(t => t.WorkCenterID)
                .Index(t => t.ProductID)
                .Index(t => t.ProductWcStatsHistoryID);
            
            CreateTable(
                "dbo.ProductWCStatsHistory",
                c => new
                    {
                        ProductWCStatsHistoryID = c.Int(nullable: false, identity: true),
                        ProductWorkCenterStatID = c.Int(nullable: false),
                        WorkCenterID = c.Int(nullable: false),
                        ProductID = c.Int(nullable: false),
                        CyclesPerHour = c.Decimal(nullable: false, precision: 18, scale: 5),
                        UnitsPerCycle = c.Decimal(nullable: false, precision: 18, scale: 5),
                        Yield = c.Decimal(nullable: false, precision: 18, scale: 5),
                        SetUpTime = c.Decimal(nullable: false, precision: 18, scale: 5),
                        Available = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Performance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Quality = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AvailableCap = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PerformanceCap = c.Decimal(nullable: false, precision: 18, scale: 2),
                        QualityCap = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 50),
                        ModifiedBy = c.String(maxLength: 50),
                        MacId = c.String(maxLength: 50),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.ProductWCStatsHistoryID);
            
            CreateTable(
                "dbo.ShiftLogs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Shift = c.Int(nullable: false),
                        Start = c.DateTime(nullable: false),
                        Stop = c.DateTime(nullable: false),
                        WorkCenterId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 50),
                        ModifiedBy = c.String(maxLength: 50),
                        MacId = c.String(maxLength: 50),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CycleHistory",
                c => new
                    {
                        CycleHistoryID = c.Int(nullable: false, identity: true),
                        WorkCenterID = c.Int(nullable: false),
                        Count = c.Int(nullable: false),
                        UpdateDT = c.DateTime(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 50),
                        ModifiedBy = c.String(maxLength: 50),
                        MacId = c.String(maxLength: 50),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.CycleHistoryID)
                .ForeignKey("dbo.WorkCenter", t => t.WorkCenterID, cascadeDelete: true)
                .Index(t => t.WorkCenterID);
            
            CreateTable(
                "dbo.Cycle",
                c => new
                    {
                        CycleID = c.Int(nullable: false, identity: true),
                        WorkCenterID = c.Int(nullable: false),
                        Count = c.Int(nullable: false),
                        UpdateDT = c.DateTime(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 50),
                        ModifiedBy = c.String(maxLength: 50),
                        MacId = c.String(maxLength: 50),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.CycleID)
                .ForeignKey("dbo.WorkCenter", t => t.WorkCenterID, cascadeDelete: true)
                .Index(t => t.WorkCenterID);
            
            CreateTable(
                "dbo.Email",
                c => new
                    {
                        EmailID = c.Int(nullable: false, identity: true),
                        WorkCenterID = c.Int(),
                        Recipient = c.String(maxLength: 100),
                        RecipientEmail = c.String(maxLength: 256),
                        Delay = c.Int(nullable: false),
                        Interval = c.Int(nullable: false),
                        Down = c.Boolean(nullable: false),
                        Availablilty = c.Decimal(precision: 18, scale: 2),
                        Performance = c.Decimal(precision: 18, scale: 2),
                        Quality = c.Decimal(precision: 18, scale: 2),
                        OEE = c.Decimal(precision: 18, scale: 2),
                        Scrap = c.String(),
                        SetupExceeded = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 50),
                        ModifiedBy = c.String(maxLength: 50),
                        MacId = c.String(maxLength: 50),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.EmailID)
                .ForeignKey("dbo.WorkCenter", t => t.WorkCenterID)
                .Index(t => t.WorkCenterID);
            
            CreateTable(
                "dbo.Mod2SqlCount",
                c => new
                    {
                        WorkCenterId = c.String(nullable: false, maxLength: 128),
                        Count = c.Int(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.WorkCenterId);
            
            CreateTable(
                "dbo.Mod2SqlStatus",
                c => new
                    {
                        WorkCenterId = c.String(nullable: false, maxLength: 128),
                        OeeStatus = c.Int(nullable: false),
                        UpdateDt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.WorkCenterId);
            
            CreateTable(
                "dbo.ShiftStartTimes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstStart = c.DateTime(nullable: false),
                        SecondStart = c.DateTime(nullable: false),
                        ThirdStart = c.DateTime(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 50),
                        ModifiedBy = c.String(maxLength: 50),
                        MacId = c.String(maxLength: 50),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TzConversion",
                c => new
                    {
                        Client = c.String(nullable: false, maxLength: 100),
                        System = c.String(maxLength: 100),
                        Id = c.Int(nullable: false, identity: true),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 50),
                        ModifiedBy = c.String(maxLength: 50),
                        MacId = c.String(maxLength: 50),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Client);
            
            CreateTable(
                "dbo.WCProductTimelineHistory",
                c => new
                    {
                        WCProductTimelineHistoryID = c.Int(nullable: false, identity: true),
                        WCProductTimelineID = c.Int(nullable: false),
                        WorkCenterID = c.Int(),
                        ProductID = c.Int(),
                        Start = c.DateTime(nullable: false),
                        Stop = c.DateTime(),
                        TotalDT = c.Decimal(nullable: false, precision: 18, scale: 5),
                        TotalPlannedDT = c.Decimal(nullable: false, precision: 18, scale: 5),
                        PlannedProduction = c.Decimal(nullable: false, precision: 18, scale: 5),
                        OperationTime = c.Decimal(nullable: false, precision: 18, scale: 5),
                        ProductWcStatsHistoryID = c.Int(),
                        Current = c.Boolean(nullable: false),
                        Availability = c.Decimal(nullable: false, precision: 18, scale: 5),
                        Performance = c.Decimal(nullable: false, precision: 18, scale: 5),
                        Quality = c.Decimal(nullable: false, precision: 18, scale: 5),
                        OEE = c.Decimal(nullable: false, precision: 18, scale: 5),
                        LastDTReason = c.String(),
                        WCProduct = c.String(),
                        Down = c.Boolean(nullable: false),
                        WCPlanned = c.Boolean(nullable: false),
                        Mfg = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Matl = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Rework = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Other = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ActualPieces = c.Decimal(nullable: false, precision: 18, scale: 5),
                        ActualCycles = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 50),
                        ModifiedBy = c.String(maxLength: 50),
                        MacId = c.String(maxLength: 50),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.WCProductTimelineHistoryID)
                .ForeignKey("dbo.ProductWCStatsHistory", t => t.ProductWcStatsHistoryID)
                .Index(t => t.ProductWcStatsHistoryID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WCProductTimelineHistory", "ProductWcStatsHistoryID", "dbo.ProductWCStatsHistory");
            DropForeignKey("dbo.Email", "WorkCenterID", "dbo.WorkCenter");
            DropForeignKey("dbo.Cycle", "WorkCenterID", "dbo.WorkCenter");
            DropForeignKey("dbo.CycleHistory", "WorkCenterID", "dbo.WorkCenter");
            DropForeignKey("dbo.CallLog", "WorkCenterId", "dbo.WorkCenter");
            DropForeignKey("dbo.WCProductTimeline", "WorkCenterID", "dbo.WorkCenter");
            DropForeignKey("dbo.Timesegment", "WCProductTimelineID", "dbo.WCProductTimeline");
            DropForeignKey("dbo.WCProductTimeline", "ShiftLogId", "dbo.ShiftLogs");
            DropForeignKey("dbo.WCProductTimeline", "ProductWcStatsHistoryID", "dbo.ProductWCStatsHistory");
            DropForeignKey("dbo.WCProductTimeline", "ProductID", "dbo.Product");
            DropForeignKey("dbo.Scrap", "TimesegmentID", "dbo.Timesegment");
            DropForeignKey("dbo.Scrap", "WorkCenterID", "dbo.WorkCenter");
            DropForeignKey("dbo.Scrap", "ScrapTypeID", "dbo.ScrapType");
            DropForeignKey("dbo.Scrap", "ScrapParentID", "dbo.Scrap");
            DropForeignKey("dbo.Scrap", "RejectCodeID", "dbo.RejectCode");
            DropForeignKey("dbo.Scrap", "ProductID", "dbo.Product");
            DropForeignKey("dbo.Scrap", "LastStageID", "dbo.LastStage");
            DropForeignKey("dbo.Scrap", "EmployeeID", "dbo.Employee");
            DropForeignKey("dbo.PlannedDtLog", "WorkCenterID", "dbo.WorkCenter");
            DropForeignKey("dbo.PlannedDtLog", "TimesegmentID", "dbo.Timesegment");
            DropForeignKey("dbo.PlannedDtLog", "ProductID", "dbo.Product");
            DropForeignKey("dbo.PlannedDtLog", "PlannedDtID", "dbo.PlannedDt");
            DropForeignKey("dbo.PlannedDtLog", "EmployeeID", "dbo.Employee");
            DropForeignKey("dbo.Timesegment", "CycleSummaryID", "dbo.CycleSummary");
            DropForeignKey("dbo.CallLog", "TimesegmentID", "dbo.Timesegment");
            DropForeignKey("dbo.CallLog", "SubCategoryID", "dbo.SubCategory");
            DropForeignKey("dbo.CallLog", "ProductID", "dbo.Product");
            DropForeignKey("dbo.CallLog", "IssueID", "dbo.Issue");
            DropForeignKey("dbo.Issue", "SubCategoryID", "dbo.SubCategory");
            DropForeignKey("dbo.CallLog", "EmployeeID", "dbo.Employee");
            DropForeignKey("dbo.CallLog", "CallID", "dbo.Call");
            DropForeignKey("dbo.BOM", "Product1_Id", "dbo.Product");
            DropForeignKey("dbo.BOM", "Product_Id", "dbo.Product");
            DropForeignKey("dbo.ProductWorkCenterStat", "WorkCenterID", "dbo.WorkCenter");
            DropForeignKey("dbo.WorkCenter", "WorkCenterGUIOptionsID", "dbo.WorkCenterGUIOptions");
            DropForeignKey("dbo.WorkCenterCall", "WorkCenterID", "dbo.WorkCenter");
            DropForeignKey("dbo.WorkCenterCall", "CallID", "dbo.Call");
            DropForeignKey("dbo.Call", "AudioFileID", "dbo.AudioFile");
            DropForeignKey("dbo.WorkCenter", "ProductID", "dbo.Product");
            DropForeignKey("dbo.ProductWorkCenterStat", "ProductID", "dbo.Product");
            DropForeignKey("dbo.AutoMessage", "EmployeeID", "dbo.Employee");
            DropForeignKey("dbo.Employee", "PositionID", "dbo.Position");
            DropForeignKey("dbo.AutoMessage", "AudioFileID", "dbo.AudioFile");
            DropIndex("dbo.WCProductTimelineHistory", new[] { "ProductWcStatsHistoryID" });
            DropIndex("dbo.Email", new[] { "WorkCenterID" });
            DropIndex("dbo.Cycle", new[] { "WorkCenterID" });
            DropIndex("dbo.CycleHistory", new[] { "WorkCenterID" });
            DropIndex("dbo.WCProductTimeline", new[] { "ProductWcStatsHistoryID" });
            DropIndex("dbo.WCProductTimeline", new[] { "ProductID" });
            DropIndex("dbo.WCProductTimeline", new[] { "WorkCenterID" });
            DropIndex("dbo.WCProductTimeline", new[] { "ShiftLogId" });
            DropIndex("dbo.Scrap", new[] { "ScrapParentID" });
            DropIndex("dbo.Scrap", new[] { "TimesegmentID" });
            DropIndex("dbo.Scrap", new[] { "EmployeeID" });
            DropIndex("dbo.Scrap", new[] { "ScrapTypeID" });
            DropIndex("dbo.Scrap", new[] { "RejectCodeID" });
            DropIndex("dbo.Scrap", new[] { "LastStageID" });
            DropIndex("dbo.Scrap", new[] { "ProductID" });
            DropIndex("dbo.Scrap", new[] { "WorkCenterID" });
            DropIndex("dbo.PlannedDtLog", new[] { "TimesegmentID" });
            DropIndex("dbo.PlannedDtLog", new[] { "ProductID" });
            DropIndex("dbo.PlannedDtLog", new[] { "WorkCenterID" });
            DropIndex("dbo.PlannedDtLog", new[] { "EmployeeID" });
            DropIndex("dbo.PlannedDtLog", new[] { "PlannedDtID" });
            DropIndex("dbo.Timesegment", new[] { "CycleSummaryID" });
            DropIndex("dbo.Timesegment", new[] { "WCProductTimelineID" });
            DropIndex("dbo.Issue", new[] { "SubCategoryID" });
            DropIndex("dbo.CallLog", new[] { "TimesegmentID" });
            DropIndex("dbo.CallLog", new[] { "ProductID" });
            DropIndex("dbo.CallLog", new[] { "SubCategoryID" });
            DropIndex("dbo.CallLog", new[] { "IssueID" });
            DropIndex("dbo.CallLog", new[] { "EmployeeID" });
            DropIndex("dbo.CallLog", new[] { "WorkCenterId" });
            DropIndex("dbo.CallLog", new[] { "CallID" });
            DropIndex("dbo.Call", new[] { "AudioFileID" });
            DropIndex("dbo.WorkCenterCall", new[] { "CallID" });
            DropIndex("dbo.WorkCenterCall", new[] { "WorkCenterID" });
            DropIndex("dbo.WorkCenter", new[] { "WorkCenterGUIOptionsID" });
            DropIndex("dbo.WorkCenter", new[] { "ProductID" });
            DropIndex("dbo.ProductWorkCenterStat", new[] { "ProductID" });
            DropIndex("dbo.ProductWorkCenterStat", new[] { "WorkCenterID" });
            DropIndex("dbo.BOM", new[] { "Product1_Id" });
            DropIndex("dbo.BOM", new[] { "Product_Id" });
            DropIndex("dbo.Employee", new[] { "PositionID" });
            DropIndex("dbo.AutoMessage", new[] { "EmployeeID" });
            DropIndex("dbo.AutoMessage", new[] { "AudioFileID" });
            DropTable("dbo.WCProductTimelineHistory");
            DropTable("dbo.TzConversion");
            DropTable("dbo.ShiftStartTimes");
            DropTable("dbo.Mod2SqlStatus");
            DropTable("dbo.Mod2SqlCount");
            DropTable("dbo.Email");
            DropTable("dbo.Cycle");
            DropTable("dbo.CycleHistory");
            DropTable("dbo.ShiftLogs");
            DropTable("dbo.ProductWCStatsHistory");
            DropTable("dbo.WCProductTimeline");
            DropTable("dbo.ScrapType");
            DropTable("dbo.RejectCode");
            DropTable("dbo.LastStage");
            DropTable("dbo.Scrap");
            DropTable("dbo.PlannedDt");
            DropTable("dbo.PlannedDtLog");
            DropTable("dbo.CycleSummary");
            DropTable("dbo.Timesegment");
            DropTable("dbo.SubCategory");
            DropTable("dbo.Issue");
            DropTable("dbo.CallLog");
            DropTable("dbo.CallLogHistory");
            DropTable("dbo.WorkCenterGUIOptions");
            DropTable("dbo.Call");
            DropTable("dbo.WorkCenterCall");
            DropTable("dbo.WorkCenter");
            DropTable("dbo.ProductWorkCenterStat");
            DropTable("dbo.Product");
            DropTable("dbo.BOM");
            DropTable("dbo.Position");
            DropTable("dbo.Employee");
            DropTable("dbo.AutoMessage");
            DropTable("dbo.AudioFile");
            DropTable("dbo.AppError");
        }
    }
}
