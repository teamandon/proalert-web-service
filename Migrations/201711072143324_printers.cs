namespace ProAlert.Andon.Service.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class printers : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.WorkCenterPrintingOptions", "ScrapPrinter", c => c.String(maxLength: 100));
            AddColumn("dbo.WorkCenterPrintingOptions", "ShippingPrinter", c => c.String(maxLength: 100));
            AlterColumn("dbo.Product", "BlanketPo", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Product", "BlanketPo", c => c.String());
            DropColumn("dbo.WorkCenterPrintingOptions", "ShippingPrinter");
            DropColumn("dbo.WorkCenterPrintingOptions", "ScrapPrinter");
        }
    }
}
