namespace ProAlert.Andon.Service.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class blanketpo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Product", "BlanketPo", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Product", "BlanketPo");
        }
    }
}
