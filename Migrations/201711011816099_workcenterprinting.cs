namespace ProAlert.Andon.Service.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class workcenterprinting : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.WorkCenterPrintingOptions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ScrapLabel = c.Boolean(),
                        ShippingLabel = c.Boolean(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 50),
                        ModifiedBy = c.String(maxLength: 50),
                        MacId = c.String(maxLength: 50),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.WorkCenter", "WorkCenterPrintingOptionsID", c => c.Int());
            CreateIndex("dbo.WorkCenter", "WorkCenterPrintingOptionsID");
            AddForeignKey("dbo.WorkCenter", "WorkCenterPrintingOptionsID", "dbo.WorkCenterPrintingOptions", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WorkCenter", "WorkCenterPrintingOptionsID", "dbo.WorkCenterPrintingOptions");
            DropIndex("dbo.WorkCenter", new[] { "WorkCenterPrintingOptionsID" });
            DropColumn("dbo.WorkCenter", "WorkCenterPrintingOptionsID");
            DropTable("dbo.WorkCenterPrintingOptions");
        }
    }
}
