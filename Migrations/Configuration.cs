using System.Collections.Generic;
using System.IO;
using System.Web.Hosting;
using ProAlert.Andon.Service.Common;
using ProAlert.Andon.Service.Models;

namespace ProAlert.Andon.Service.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<ProAlert.Andon.Service.Models.ProAlertContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(ProAlert.Andon.Service.Models.ProAlertContext context)
        {
            //    var pAdmin = new Position { Title = "Admin" };
            //    var pSuper = new Position { Title = "Super" };
            //    var pOp = new Position { Title = "Operator" };
            //    var pAd = new Position { Title = "AndonDisplay" };
            //    var empList = new List<Employee>
            //                {
            //                    new Employee
            //                    {
            //                        Position = pAdmin,
            //                        LogIn = "Admin",
            //                        FirstName = "Admin",
            //                        LastName = "Admin",
            //                        Pin = 777,
            //                        HireDate = DateTime.UtcNow,
            //                        TimeZone = "Eastern Standard Time"
            //                    },
            //                    new Employee
            //                    {
            //                        Position = pAdmin,
            //                        LogIn = "tzi",
            //                        FirstName = "tzi",
            //                        LastName = "tzi",
            //                        Pin = 999,
            //                        HireDate = DateTime.UtcNow,
            //                        TimeZone = "Eastern Standard Time"
            //                    },

            //                    new Employee
            //                    {
            //                        Position = pSuper,
            //                        LogIn = "super",
            //                        FirstName = "super",
            //                        LastName = "super",
            //                        Pin = 888,
            //                        HireDate = DateTime.UtcNow,
            //                        TimeZone = "Eastern Standard Time"
            //                    },
            //                    new Employee
            //                    {
            //                        Position = pOp,
            //                        LogIn = "operator",
            //                        FirstName = "operator",
            //                        LastName = "operator",
            //                        Pin = 123,
            //                        HireDate = DateTime.UtcNow,
            //                        TimeZone = "Eastern Standard Time"
            //                    },
            //                    new Employee
            //                    {
            //                        Position = pAd,
            //                        LogIn = "ad",
            //                        FirstName = "ad",
            //                        LastName = "display",
            //                        Pin = 987,
            //                        HireDate = DateTime.UtcNow,
            //                        TimeZone = "Eastern Standard Time"
            //                    }
            //                };

            //    var pdtList = new List<PlannedDt>
            //                {
            //                    new PlannedDt
            //                    {
            //                        Name = "Product Change Over",
            //                        Description = "product change over",
            //                        CreatedDate = DateTime.UtcNow
            //                    },
            //                    new PlannedDt
            //                    {
            //                        Name = "Lunch",
            //                        Description = "lunch",
            //                        CreatedDate = DateTime.UtcNow
            //                    },
            //                    new PlannedDt
            //                    {
            //                        Name = "Break",
            //                        Description = "break",
            //                        CreatedDate = DateTime.UtcNow
            //                    },
            //                    new PlannedDt
            //                    {
            //                        Name = "End of Shift",
            //                        Description = "end of shift",
            //                        CreatedDate = DateTime.UtcNow
            //                    }
            //                };

            //    var stList = new List<ScrapType>
            //                {
            //                    new ScrapType
            //                    {
            //                        Name = "Material",
            //                        Description = "materials",
            //                        Default = true
            //                    },
            //                    new ScrapType
            //                    {
            //                        Name = "Manufacturing",
            //                        Description = "manufacturing",
            //                        Default = false
            //                    },
            //                    new ScrapType
            //                    {
            //                        Name = "Rework",
            //                        Description = "rework",
            //                        Default = false
            //                    },
            //                    new ScrapType
            //                    {
            //                        Name = "Other",
            //                        Description = "other",
            //                        Default = false
            //                    }
            //                };

            //    context.RejectCodes.AddOrUpdate(new RejectCode { Name = "200", Description = "Heat" }, new RejectCode { Name = "300", Description = "Mold" });
            //    context.LastStages.AddOrUpdate(new LastStage { Name = "999", Description = "Pack Out" });
            //    context.Issues.AddOrUpdate(new Issue { Name = "Short", Description = "short" });
            //    context.SubCategories.AddOrUpdate(new SubCategory { Name = "Cord", Description = "Cord cut" });
            //    byte[] sd1;
            //    using (Stream fs = File.OpenRead(HostingEnvironment.MapPath(@"~\sounds\Maintenance.wav")))
            //    {
            //        sd1 = new byte[fs.Length];
            //        fs.Read(sd1, 0, sd1.Length);
            //    }
            //    var afMaint = new AudioFile { Name = "Maintenance", RepeatCnt = 2, Sound = sd1 };
            //    byte[] sd2;
            //    using (Stream fs = File.OpenRead(HostingEnvironment.MapPath(@"~\sounds\Quality.wav")))
            //    {
            //        sd2 = new byte[fs.Length];
            //        fs.Read(sd2, 0, sd2.Length);
            //    }
            //    var afQuality = new AudioFile { Name = "Quality", RepeatCnt = 2, Sound = sd2 };
            //    byte[] sd3;
            //    using (Stream fs = File.OpenRead(HostingEnvironment.MapPath(@"~\sounds\Materials.wav")))
            //    {
            //        sd3 = new byte[fs.Length];
            //        fs.Read(sd3, 0, sd3.Length);
            //    }
            //    var afMatl = new AudioFile { Name = "Materials", RepeatCnt = 2, Sound = sd3 };
            //    byte[] soundData;
            //    using (Stream fs = File.OpenRead(HostingEnvironment.MapPath(@"~\sounds\Supervisors.wav")))
            //    {
            //        soundData = new byte[fs.Length];
            //        fs.Read(soundData, 0, soundData.Length);
            //    }
            //    var afSups = new AudioFile { Name = "Supervisors", RepeatCnt = 2, Sound = soundData };
            //    //context.AudioFiles.AddOrUpdate(af);
            //    var products = new List<Product>();
            //    var jpgs = new List<string> { "221.jpg", "1.jpg", "10.jpg", "13.jpg", "9.jpg" };
            //    foreach (var jpg in jpgs)
            //    {
            //        byte[] imgData;
            //        using (Stream fs = File.OpenRead(HostingEnvironment.MapPath(@"~\Images\" + jpg)))
            //        {
            //            imgData = new byte[fs.Length];
            //            fs.Read(imgData, 0, imgData.Length);
            //        }
            //        products.Add(new Product
            //        {
            //            Name = jpg.Split('.')[0],
            //            Description = jpg.Split('.')[0],
            //            Image = imgData,
            //            Material = "Plastic",
            //            StandardPack = 20
            //        });
            //    }

            //    //context.Products.AddOrUpdate(pf);
            //    var listCalls = new List<Call>
            //                {
            //                    new Call
            //                    {
            //                        Name = "Maintenance",
            //                        Description = "maintenance",
            //                        AudioFile = afMaint,
            //                        DT = true,
            //                        Type = Enumerations.CallType.Maintenance
            //                    },
            //                    new Call
            //                    {
            //                        Name = "Quality",
            //                        Description = "Quality",
            //                        AudioFile = afQuality,
            //                        DT = true,
            //                        Type = Enumerations.CallType.QC
            //                    },
            //                    new Call
            //                    {
            //                        Name = "Materials",
            //                        Description = "materials",
            //                        AudioFile = afMatl,
            //                        DT = true,
            //                        Type = Enumerations.CallType.Materials
            //                    },
            //                    new Call
            //                    {
            //                        Name = "Maintenance",
            //                        Description = "maintenance",
            //                        AudioFile = afMaint,
            //                        DT = false,
            //                        Type = Enumerations.CallType.Maintenance
            //                    },
            //                    new Call
            //                    {
            //                        Name = "Quality",
            //                        Description = "Quality",
            //                        AudioFile = afQuality,
            //                        DT = false,
            //                        Type = Enumerations.CallType.QC
            //                    },
            //                    new Call
            //                    {
            //                        Name = "Materials",
            //                        Description = "materials",
            //                        AudioFile = afMatl,
            //                        DT = false,
            //                        Type = Enumerations.CallType.Materials
            //                    },
            //                    new Call
            //                    {
            //                        Name = "Dumpster",
            //                        Description = "dumpster",
            //                        AudioFile = afMatl,
            //                        DT = false,
            //                        Type = Enumerations.CallType.Materials
            //                    }
            //                };

            //    var listWc = new List<WorkCenter>();
            //    for (var i = 1; i < 6; i++)
            //    {
            //        var nwc = new WorkCenter
            //        {
            //            Name = "WC " + i,
            //            Description = "work center " + i,
            //            WorkCenterGUIOptions = new WorkCenterGUIOptions { ManualCycles = true, SingleScrap = false },
            //            OEEStartTime = DateTime.UtcNow
            //        };
            //        var ProdWcStats = new List<ProductWorkCenterStat>();
            //        foreach (var pf in products)
            //        {
            //            ProdWcStats.Add(new ProductWorkCenterStat
            //            {
            //                Product = pf,
            //                CyclesPerHour = 120,
            //                UnitsPerCycle = 2,
            //                Yield = (decimal).99,
            //                SetUpTime = 10,
            //                Available = (decimal).94,
            //                Performance = (decimal)0.92,
            //                Quality = (decimal).98,
            //                PerformanceCap = (decimal)1.5,
            //                AvailableCap = 1,
            //                QualityCap = 1
            //            });
            //        }
            //        nwc.ProductWorkCenterStats = ProdWcStats;
            //        listWc.Add(nwc);
            //    }
            //    var shiftTimes = new ShiftStartTimes
            //    {

            //        FirstStart = DateTime.UtcNow.Date.AddHours(9),
            //        SecondStart = DateTime.UtcNow.Date.AddHours(17),
            //        ThirdStart = DateTime.UtcNow.Date.AddHours(25),
            //        CreatedDate = DateTime.UtcNow
            //    };
            //    context.ShiftStartTimes.AddOrUpdate(shiftTimes);
            //    // -------------------------------------------
            //    // do not know how to get this to work.  Moving to a hidden link on the contact page.
            //    //var listWcCalls = new List<WorkCenterCall>();
            //    //foreach (var workCenter in listWc)
            //    //{
            //    //    foreach (var c in listCalls)
            //    //    {
            //    //        listWcCalls.Add(new WorkCenterCall
            //    //        {
            //    //            Pair = workCenter.Name + c.Name,
            //    //            Call = c,
            //    //            WorkCenter = workCenter,
            //    //            OrderID = c.Name.Equals("Dumpster") ? 1 : 0
            //    //        });
            //    //    }
            //    //}

            //    empList.ForEach(s => context.Employees.AddOrUpdate(p => p.LogIn, s));
            //    pdtList.ForEach(s => context.PlannedDts.AddOrUpdate(p => p.Name, s));
            //    stList.ForEach(s => context.ScrapTypes.AddOrUpdate(p => p.Name, s));
            //    listWc.ForEach(s => context.WorkCenters.AddOrUpdate(p => p.Name, s));
            //    listCalls.ForEach(s => context.Calls.AddOrUpdate(p => new { p.Name, p.DT }, s));
            //    //listWcCalls.ForEach(s => context.WorkCenterCalls.AddOrUpdate(p => p.Pair, s));

        }
    }
}
