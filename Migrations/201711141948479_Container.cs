namespace ProAlert.Andon.Service.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Container : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Container",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        WorkCenterId = c.Int(nullable: false),
                        ProductId = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                        SerialNumber = c.Int(nullable: false),
                        PO = c.String(maxLength: 50),
                        Location = c.String(maxLength: 50),
                        PrintDate = c.DateTime(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 50),
                        ModifiedBy = c.String(maxLength: 50),
                        MacId = c.String(maxLength: 50),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            //AddColumn("dbo.WorkCenter", "Serial", c => c.Int());
        }
        
        public override void Down()
        {
            //DropColumn("dbo.WorkCenter", "Serial");
            DropTable("dbo.Container");
        }
    }
}
