namespace ProAlert.Andon.Service.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class rolleight : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.GlobalSetting", "RollEight", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.GlobalSetting", "RollEight");
        }
    }
}
