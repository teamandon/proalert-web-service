namespace ProAlert.Andon.Service.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class globalsetting : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.GlobalSettings", newName: "GlobalSetting");
            RenameColumn(table: "dbo.GlobalSetting", name: "Id", newName: "GlobalSettingID");
            RenameColumn(table: "dbo.GlobalSetting", name: "OeeSummaryStartDt", newName: "OeeSumStartDt");
        }
        
        public override void Down()
        {
            RenameColumn(table: "dbo.GlobalSetting", name: "OeeSumStartDt", newName: "OeeSummaryStartDt");
            RenameColumn(table: "dbo.GlobalSetting", name: "GlobalSettingID", newName: "Id");
            RenameTable(name: "dbo.GlobalSetting", newName: "GlobalSettings");
        }
    }
}
